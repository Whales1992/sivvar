package com.example.sivvar;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void dateChecker(){
        println(new SimpleDateFormat("D MMM YYYY").format(new Date()));
    }

    //@Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    //@Test
    public void time(){
        System.out.println("DIVIDER"+ 120/60);
        System.out.println("MODULUS"+ 121%60);
    }

    //@Test
    public void floatTest(){
        float x = 0.1f;
        println(Float.valueOf(x)+"");
    }

    public void println(String s){
        System.out.println(s);
    }
}