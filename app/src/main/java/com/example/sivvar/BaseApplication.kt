package com.example.sivvar

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.orm.SugarApp
import android.os.Build
import android.util.Log
import com.example.sivvar.activities.linphone.MainActivity


class BaseApplication : SugarApp() {
    private val CHANNEL_ID = "NOTIFICATION CHANNEL"
    private val notificationId = 1
    var builder : NotificationCompat.Builder? = null

    override fun onCreate() {
        super.onCreate()

        try{
            initNotification()
        }catch(ex: Exception) {
            Log.e("Exception", "${ex.message}")
        }
    }

    companion object{

        fun updateNotification(state: String, context: Context){
            val notifyIntent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            val notifyPendingIntent = PendingIntent.getActivity(
                    context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
            )

            val ready: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                context.resources.getColor(R.color.green)
            else
                context.resources.getColor(R.color.green)

            val notReady: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                context.resources.getColor(R.color.red)
            else
                context.resources.getColor(R.color.red)

            if(state.equals("CONNECTED")){
                val builder = NotificationCompat.Builder(context, "NOTIFICATION CHANNEL")
                        .setSmallIcon(R.drawable.ready)
//                        .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ready))
                        .setColor(ready)
                        .setContentTitle("SIVVAR App")
                        .setContentText("SIVVAR Is Ready")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(notifyPendingIntent)

                with(NotificationManagerCompat.from(context)) {
                    // notificationId is a unique int for each notification that you must define
                    notify(1, builder!!.build())
                }
            }else{
                val builder = NotificationCompat.Builder(context, "NOTIFICATION CHANNEL")
                        .setSmallIcon(R.drawable.ready)
//                        .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.not_ready))
                        .setColor(notReady)
                        .setContentTitle("SIVVAR App")
                        .setContentText("SIVVAR Not Ready")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(notifyPendingIntent)

                with(NotificationManagerCompat.from(context)) {
                    // notificationId is a unique int for each notification that you must define
                    notify(1, builder!!.build())
                }
            }
        }

    }

    private fun initNotification(){
        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        notifyIntent.putExtra("notifications", 777)

        val notifyPendingIntent =
                PendingIntent.getActivity(this, 0, notifyIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT)

        val color: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            getColor(R.color.red)
        else
            resources.getColor(R.color.red)

        builder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.not_ready)
//                .setLargeIcon(logo)
                .setColor(color)
                .setContentTitle("SIVVAR App")
                .setContentText("Sivvar Not Ready")
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(notifyPendingIntent)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId, builder!!.build())
        }
    }

}