package com.example.sivvar.network;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.example.sivvar.helpers.PhoneRegisterParams;
import com.example.sivvar.helpers.VerifyPhoneRegisterParams;
import com.example.sivvar.network.Interfaces.APIInterface;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCalls{
//    private final String SERVICE = "http://162.243.12.190:9496/sivvarui/sivvarservicesapi/v3/sivvarapp/";
    private final String FAV = "http://162.243.12.190:9496/sivvarui/sivvarservicesapi/v3/sivvarapp/";
    private final String SERVICE = "http://sivvar-services-prod.sivvar.services:8896/sivvarui/sivvarservicesapi/v3/sivvarapp/";
//    private final String INITIALIZE = "https://sivvar-initialization.sivvar.services:8829/sivvarlogics/sivvarapi/v2/sivvarapp/";
    private final String INITIALIZE = "http://sivvar-initialization-prod.sivvar.services:8892/sivvarlogics/sivvarapi/v3/sivvarapp/";
//    private final String INITIALIZE_NEW = "http://162.243.12.190:9494/sivvarlogics/sivvarapi/v3/sivvarapp/";
    APIInterface apiInterface;

    public ApiCalls(String init, Context context, int readTimeOut, int writeTimeOut, int connectionTimeOut){
        String base = "";

        switch (init) {
            case "init": case "init_new":
                base = INITIALIZE;
                break;
//            case "init_new":
//                base = INITIALIZE_NEW;
//                break;
            case "services":
                base = SERVICE;
                break;

            case "favourites":
                base = FAV;
                break;
        }

            apiInterface = APIClient.getClient(base, context, readTimeOut, writeTimeOut, connectionTimeOut).create(APIInterface.class);
    }


    /**
    * Function to login/register using phone call as the method of verification
    * Once this api is called successfully, the call kicks in, note this call should not be picked
    * hence should be ignore but before then it's been observed and used as confirmation.
    */
    public void initByPhoneCall(PhoneRegisterParams param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.authByPhone(param);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    /**
     * Function to login/register using phone call as the method of verification
     * Once this api is called successfully, the call kicks in and should be picked and an OTP will be
     * called to the user which they will then use to confirm their authentication process.
     */
    public void initConcludeCallVerification(VerifyPhoneRegisterParams param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.concludeVerifications(param);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    public void initBySms(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.authBySms(param.get("msISDN").getAsString(), param.get("Imei").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    public void ivrphonecallverification(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.ivrphonecallverification(param.get("msISDN").getAsString(), param.get("Imei").getAsString(), param.get("retry").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS IVR" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                   //Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    public void completeivrphonecallverification(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.completeivrphonecallverification(param.get("msISDN").getAsString(), param.get("Imei").getAsString(), param.get("regCode").getAsString(), param.get("Channel").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    //Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    /**
    * This method handles the calling and response of sivvar categories API
    * The api is to get the list of categories from sivvar.
    */
    public void getCategories(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.categories(param.get("msISDN").getAsString(), param.get("Identity").getAsString(), param.get("Categories").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                   // Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    /**
    * This method handles the calling and response of sivvar favourites API
    * The api is to get the list of favourites from sivvar.
    */
    public void getFavourites(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.favourites(param.get("msISDN").getAsString(), param.get("Identity").getAsString(), param.get("FavouriteServices").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    //Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            //Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    /**
     * This method handles the calling and response of sivvar category services API
     * The api is to get the list of services inside of a specific categories from sivvar
     * by sending the @param categoryID along.
     */
    public void getServiceCategories(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.serviceCategories(param.get("msISDN").getAsString(), param.get("Identity").getAsString(), param.get("categoryID").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                   // Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.body()!=null)
                        responseCallback.onSuccess(response.body());
                    else
                        responseCallback.onFailure("An Error Occurred, Try again.");
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    //Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }


    /**
     * This method handles the calling and response of sivvar sub-services API
     * The api is to get the list of sub-services inside of a specific service or sub-service from sivvar
     * by sending the @param serviceId along.
     */
    public void getSubServices(JsonObject param, ResponseCallback responseCallback){
        try{
            Call<String> call;
            call = apiInterface.subServices(param.get("msISDN").getAsString(), param.get("Identity").getAsString(), param.get("serviceID").getAsString(), param.get("sivvarStamp").getAsString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                   // Log.e("SUCCESS RES" , "success"+response.body());

                    if(response.isSuccessful() || response.body()!=null){
                        responseCallback.onSuccess(response.body());
                    }else{
                        responseCallback.onFailure("An Error Occurred, Try again.");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    //Log.e("ERROR RES" , "err"+t.getMessage());

                    responseCallback.onFailure(t.getMessage()+"");
                }
            });
        }catch (Exception ex){
            Log.e("Exception!!!", ex.getMessage()+"");
            responseCallback.onFailure("An error occurred, try again.");
        }
    }
}