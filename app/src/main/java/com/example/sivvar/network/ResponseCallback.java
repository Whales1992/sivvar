package com.example.sivvar.network;

public interface ResponseCallback {
    String onSuccess(String res);
    String onFailure(String res);
}
