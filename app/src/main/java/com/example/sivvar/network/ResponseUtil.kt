package com.example.sivvar.network

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

class ResponseUtil {

    companion object{
        val GENERIC_ERROR_MESSAGE = "Opps, An error occurred, please try again."
        val GENERIC_SUCCESS_MESSAGE = "Operation was Successful"
        val INVALIDREQUEST = "E9900"
        val INVALIDMSISDN = "E9902" //MSISDN means PHONE NUMBER
        val NOTFROMSIVVAR = "E9999" //Forbidden request
        val OK = "S0000" //Success reponse code from hitting sivvar APIs
        val EXISTING = "S0001" //EXISTING USER RETURNING

        /*
        * This method simply handles the checking of success code
         * from the @param res (response) of SIVVAR API Version 3
         * and return a boolean of true if successfull else returns false
        * */
        fun isSuccessfullV3(res : String) : Boolean{
            try{
                val obj = JSONObject(res)
                val response = obj.getJSONObject("response")
                return response.getString("responseCode").equals(OK)
            }catch (ex : Exception){
                Log.e("EXCEPTION", ""+ex)
            }

            return false
        }

        /*
         * This method simply handles the checking of success or error messages
          * from the @param res (response) of SIVVAR API Version 3
          * and return the String value of the message or a generic error or success message
          * if no message come back from hitting the API
          * Note: this method also calls @method isSuccessfullV3 for additional checking
          * not entirly necessary but it's just cool any way.
         * */
        fun getMessageV3(res : String) : String{
             try{
                 val obj = JSONObject(res)
                 val response = obj.getJSONObject("response")

                 return if(response==null || response.getString("responseMessage").trim().isEmpty()){
                     if(isSuccessfullV3(res))
                         GENERIC_SUCCESS_MESSAGE
                     else
                         GENERIC_ERROR_MESSAGE
                 }else{
                     response.getString("responseMessage")
                 }
             }catch (ex : Exception){
                 Log.e("EXCEPTION", ""+ex)
             }

             return GENERIC_ERROR_MESSAGE
         }

        /*
        * This method simply handles the grabbing of responseData
        * from the @param res (response) of SIVVAR API Version 3
        * and return the Stringify value of the responseData or a generic error message
        * if no responseData come back from hitting the API
        * */
        fun getResponseDataV3(res : String) : String{
            try{
                val obj = JSONObject(res)
                val response = obj.getJSONObject("response")
                return response.getString("responseData")
            }catch (ex : Exception){
                Log.e("EXCEPTION", ""+ex)
            }

            return GENERIC_ERROR_MESSAGE
        }


        fun isSuccessfull(response : String) : Boolean{
            try {
                var json = JSONObject(response)
                if(json.has("response_code"))
                    return json.get("response_code").equals(OK)
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
            }
            return false
        }

        fun isSuccessfullIVR(res : String) : Boolean{
            try{
                var obj = JSONObject(res)
                var response = obj.getJSONObject("response")
                return response.getString("responseCode").equals(OK)
            }catch (ex : Exception){
                Log.e("EXCEPTION", ""+ex)
            }

            return false
        }

        fun isSuccessfullIVRretuning(res : String) : Boolean{
            try{
                val obj = JSONObject(res)
                val response = obj.getJSONObject("response")
                return response.getString("responseCode").equals(EXISTING)
            }catch (ex : Exception){
                Log.e("EXCEPTION", ""+ex)
            }

            return false
        }

        fun isInvalidRequest(response : String) : Boolean{
            try {
                var json = JSONObject(response)
                if(json.has("response_code"))
                    return json.get("response_code").equals(INVALIDREQUEST)
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return false
            }

            return false
        }


        fun isInvalidMSISDN(response : String) : Boolean{
            try {
                var json = JSONObject(response)
                if(json.has("response_code"))
                    return json.get("response_code").equals(INVALIDMSISDN)
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return false
            }

            return false
        }

        fun isNotFromSivvar(response : String) : Boolean{
            try {
                var json = JSONObject(response)
                if(json.has("response_code"))
                    return json.get("response_code").equals(NOTFROMSIVVAR)
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return false
            }

            return false
        }


        fun getMessage(response : String) : String{
            try {
                var json = JSONObject(response)
                if(json.has("response_message"))
                    return json.get("response_message") as String
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return ex.message+""
            }

            return "Something went wrong and could not get response message, please try again."
        }


        fun getData(response : String) : String{
            try {
                var json = JSONObject(response)
                if(json.has("response_data"))
                    return json.get("response_data") as String
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return ex.message+""
            }

            return "Something went wrong and could not get response data, please try again."
        }


        fun getMessageIVR(response : String) : String{
            try {
                var json = JSONObject(response)
                var res = json.getJSONObject("response")
                return res.getString("responseMessage")
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return ex.message+""
            }
        }

        fun getDataIVR(response : String) : String{
            try {
                var json = JSONObject(response)
                var res = json.getJSONObject("response")
                return res.getString("responseData")
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return ex.message+""
            }
        }


        fun getResponseTime(response : String) : String{
            try {
                var json = JSONObject(response)
                if(json.has("response_time"))
                    return json.get("response_time") as String
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
                return ex.message+""
            }

            return "Something went wrong and could not get response time, please try again."
        }

        fun existing(res: String): Boolean {
            try {
                var json = JSONObject(res)
                if(json.has("response_code"))
                    return json.get("response_code").equals(EXISTING)
            }catch (ex : JSONException){
                Log.e("Exception", ex.message+"")
            }

            return false
        }

    }
}