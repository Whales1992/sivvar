package com.example.sivvar.network.Interfaces;

import com.example.sivvar.helpers.PhoneRegisterParams;
import com.example.sivvar.helpers.SmsRegisterParams;
import com.example.sivvar.helpers.VerifyPhoneRegisterParams;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {
    @POST("phonecallinitialize/androiduser")
    Call<String> authByPhone(@Body PhoneRegisterParams param);

    @POST("smsinitialize/androiduser")
    Call<String> authBySms(@Body SmsRegisterParams param);

    @POST("smsinitialize/androiduser")
    Call<String> authBySms(@Query("msISDN") String msISDN,
                           @Query("Imei") String Imei,
                           @Query("sivvarStamp") String sivvarStamp);

    @POST("concludeinitialize/androiduser")
    Call<String> concludeVerifications(@Body VerifyPhoneRegisterParams param);

//    @POST("ivrphonecallverification/androiduser")
//    Call<String> ivrphonecallverification(@Body JsonObject param);

    @POST("ivrphonecallverification/androiduser")
    Call<String> ivrphonecallverification(
                        @Query("msISDN") String msISDN,
                        @Query("Imei") String Imei,
                        @Query("retry") String retry,
                        @Query("sivvarStamp") String sivvarStamp);


    @POST("completesivvarinitialization/androiduser")
    Call<String> completeivrphonecallverification(
                        @Query("msISDN") String msISDN,
                        @Query("Imei") String Imei,
                        @Query("regCode") String regCode,
                        @Query("Channel") String channel,
                        @Query("sivvarStamp") String sivvarStamp);


    /*
     * Please note tha Identity is same as Imei of the device making the network request
     * */
    @POST("sivvar/categories")
    Call<String> categories(
            @Query("msISDN") String msISDN,
            @Query("Identity") String Identity,
            @Query("Categories") String Categories,
            @Query("sivvarStamp") String sivvarStamp);

    /*
    * Please note tha Identity is same as Imei of the device making the network request
    * */
    @POST("categories/services")
    Call<String> serviceCategories(
            @Query("msISDN") String msISDN,
            @Query("Identity") String Identity,
            @Query("categoryID") String categotyId,
            @Query("sivvarStamp") String sivvarStamp);

    /*
    * Please note tha Identity is same as Imei of the device making the network request
    * */
    @POST("favourite/services")
    Call<String> favourites(
            @Query("msISDN") String msISDN,
            @Query("Identity") String Identity,
            @Query("FavouriteServices") String Favourites,
            @Query("sivvarStamp") String sivvarStamp);

/*
    * Please note tha Identity is same as Imei of the device making the network request
    * */
    @POST("sivvar/subservices")
    Call<String> subServices(
            @Query("msISDN") String msISDN,
            @Query("Identity") String Identity,
            @Query("serviceID") String serviceId,
            @Query("sivvarStamp") String sivvarStamp);
}