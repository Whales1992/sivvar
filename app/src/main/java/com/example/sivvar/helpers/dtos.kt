package com.example.sivvar.helpers

import android.util.Log
import com.example.sivvar.constants.SivvarKey
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

/**
 * @author ADIO Kingsley O. @editor Adewale
 * @since 20 May, 2017
 */
open class BaseParams(vararg params: Any) {

    @SerializedName("sivvarStamp") val sivvarStamp = run {
        val rawString = params.joinToString("") {
            when (it) {
                is SivvarKey -> it.value
                else -> it.toString()
            }
        }
        rawString.let(Sha512_Generator::sha512).toLowerCase()

        //Log.e("STAMP", ""+rawString.let(Sha512_Generator::sha512).toLowerCase())
    }
}

fun generateStamp(retry: String, imei: String, phoneNumber: String) : JsonObject{
    val s : String = (SivvarKey.INIT_T.value+retry+imei+phoneNumber+ SivvarKey.INIT_T.value)
    Log.e("PARAM", ""+s)

    val params = JsonObject()
    params.addProperty("msISDN", phoneNumber)
    params.addProperty("Imei", imei)
    params.addProperty("retry", retry)
    params.addProperty("sivvarStamp", Sha512_Generator.sha512(s).toLowerCase())

    Log.e("PARAM", "{{$params}}")
   return params
}

fun generateCompleteIVRStamp(imei: String, phoneNumber: String, regCode: String, channel: String) : String{
    val s : String = (imei+phoneNumber+ SivvarKey.INIT_T.value+regCode+channel+ SivvarKey.INIT_T.value)
    Log.e("IVR COMPLETE", ""+s)
    return Sha512_Generator.sha512(s).toLowerCase()
}

/*
* This method does the generating of sivverStamp for SMS API using SHA512
* */
fun generateStampSMS(imei: String, phoneNumber: String) : String{
    val s : String = (SivvarKey.INIT_T.value+imei+phoneNumber+ SivvarKey.INIT_T.value)
    return Sha512_Generator.sha512(s).toLowerCase()
}

/*
* This method does the generating of sivverStamp for Categories API using SHA512
* Note: @param identity is equivalent to imei
* */
fun generateStampCategory(identity: String, phoneNumber: String) : String{
    val s : String = (identity+ SivvarKey.SIVVAR_UIUX_KEY.value+ SivvarKey.SIVVAR_CATS.value+phoneNumber+ SivvarKey.SIVVAR_UIUX_KEY.value)
    return Sha512_Generator.sha512(s).toLowerCase()
}

/*
* This method does the generating of sivverStamp for Categories API using SHA512
* Note: @param identity is equivalent to imei
* */
fun generateStampServicesCategory(identity: String, phoneNumber: String, channelId :String) : String{
    val s : String = (identity+ SivvarKey.SIVVAR_UIUX_KEY.value+channelId+phoneNumber+ SivvarKey.SIVVAR_UIUX_KEY.value)
    return Sha512_Generator.sha512(s).toLowerCase()
}

/*
* This method does the generating of sivverStamp for Services that belongs to a specific Category API using SHA512
* Note: @param identity is equivalent to imei and @param channelId is the channel Id gotten
* from the Category API call.
* */
fun generateStampFavourites(identity: String, phoneNumber: String) : String{
    val s : String = (identity+ SivvarKey.SIVVAR_UIUX_KEY.value+ SivvarKey.SIVVAR_FAV.value+phoneNumber+ SivvarKey.SIVVAR_UIUX_KEY.value)
    return Sha512_Generator.sha512(s).toLowerCase()
}


/*
* This method does the generating of sivverStamp for SubServices API using SHA512
* Note: @param identity is equivalent to imei and @param serviceID is the service Id
*/
fun generateStampSubServices(identity: String, phoneNumber: String, serviceID :String) : String{
    val s : String = (identity+ SivvarKey.SIVVAR_UIUX_KEY.value+serviceID+phoneNumber+ SivvarKey.SIVVAR_UIUX_KEY.value)
    return Sha512_Generator.sha512(s).toLowerCase()
}


class PhoneRegisterParams(
        @SerializedName("msISDN") val phoneNumber: String,
        @SerializedName("Imei") val imei: String,
        @SerializedName("retry") val retry: Boolean
) : BaseParams(SivvarKey.INIT, retry, imei, phoneNumber, SivvarKey.INIT)

class SmsRegisterParams(
        @SerializedName("msISDN") val phoneNumber: String,
        @SerializedName("Imei") val imei: String
) : BaseParams(SivvarKey.INIT, imei, phoneNumber, SivvarKey.INIT)

class VerifyPhoneRegisterParams(
        @SerializedName("msISDN") val phoneNumber: String,
        @SerializedName("Imei") val imei: String
) : BaseParams(imei, phoneNumber, SivvarKey.INIT, SivvarKey.INIT)

class VerifySmsRegisterParams(
        @SerializedName("msISDN") val phoneNumber: String,
        @SerializedName("Imei") val imei: String,
        @SerializedName("regCode") val regCode: String
) : BaseParams(regCode, imei, phoneNumber, SivvarKey.INIT, SivvarKey.INIT)
//
//class FavoriteServiceParams(
//        @SerializedName("msISDN") val phoneNumber: String,
//        @SerializedName("Favourites") val page: PageRule = PageRule.ALL
//) : BaseParams(SivvarKey.SERVICES, phoneNumber, phoneNumber, SivvarKey.SERVICES)
//
//class IndustryParams(
//        @SerializedName("msISDN") val phoneNumber: String,
//        @SerializedName("Industries") val page: PageRule = PageRule.ALL
//) : BaseParams(SivvarKey.SERVICES, phoneNumber, phoneNumber, SivvarKey.SERVICES)
//
//class IndustryServiceParams(
//        @SerializedName("msISDN") val phoneNumber: String,
//        @SerializedName("industryName") val industryName: String
//) : BaseParams(SivvarKey.SERVICES, phoneNumber, phoneNumber, SivvarKey.SERVICES)
//
//class SubServiceParams(
//        @SerializedName("msISDN") val phoneNumber: String,
//        @SerializedName("serviceName") val serviceName: String
//) : BaseParams(SivvarKey.SERVICES, phoneNumber, phoneNumber, SivvarKey.SERVICES)
//
//class InfoMessageParams(
//        @SerializedName("msISDN") val phoneNumber: String,
//        @SerializedName("msgInfo") val page: PageRule = PageRule.ALL
//) : BaseParams(SivvarKey.SERVICES, phoneNumber, phoneNumber, SivvarKey.SERVICES)

