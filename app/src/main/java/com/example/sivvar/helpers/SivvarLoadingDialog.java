package com.example.sivvar.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;

import com.example.sivvar.R;

public class SivvarLoadingDialog extends Dialog {

    public SivvarLoadingDialog(Context context) {
        super(context);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_custom);

        ProgressBar pb = findViewById(R.id.progressbar);

    }
}