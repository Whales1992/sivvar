package com.example.sivvar.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sivvar.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utility {

    public static void navigateWithParams(Activity from, Activity to, String key, String params) {
        Intent i = new Intent(from, to.getClass());
        i.putExtra(key, params);
        from.startActivity(i);
        from.overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public static void navigate(Activity from, Activity to) {
        Bundle bundle = new Bundle();
        Intent i = new Intent(from, to.getClass());
        i.putExtras(bundle);
        from.startActivity(i);
        from.overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null)
            view = new View(activity);

        if(imm!=null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static void initToolbar(Activity activity, String titlee){
        try{
            ImageButton backbtn = activity.findViewById(R.id.backbtn);
            TextView title = activity.findViewById(R.id.title);

            title.setText(titlee);
            backbtn.setOnClickListener(v -> activity.finish());
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    public static void initToolbar(Activity activity, boolean with_titile, String titlee, boolean with_menu){
        try{
            ImageButton backbtn, menu;
            TextView title;

            backbtn = activity.findViewById(R.id.backbtn);
            menu = activity.findViewById(R.id.menu);

            backbtn.setOnClickListener(v -> activity.finish());

            if(!with_menu)
                menu.setVisibility(View.GONE);

//            menu.setOnClickListener(v -> activity.finish());

            title = activity.findViewById(R.id.title);
            if(with_titile){
                title.setVisibility(View.VISIBLE);
                title.setText(titlee);
            }

        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    public static String format(Date date, Template template) {
        return format(date, template.get());
    }

    public static String format(Date date, String format) {
        if (date == null) return "";
        format = format.equals("") ? "yyyy-MM-dd HH:mm:ss" : format;
        return new SimpleDateFormat(format, Locale.getDefault())
                .format(date);
    }





    public static boolean isToday(Date date){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar now = Calendar.getInstance();

        return isSame(cal1, now);
    }

    public static boolean isYesterday(Date date){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -1);

        return isSame(cal1, now);
    }

    private static boolean isSame(Calendar cal1, Calendar cal2){
        return (
                cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                        cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH));
    }

    public static String dateString(Date date){
        return new SimpleDateFormat("dd MMM YYYY").format(date);
    }

    public static String timeString(Date date){
        return new SimpleDateFormat("hh:mm a").format(date);
    }





    public enum Template {
        STRING_DAY_MONTH_YEAR("d MMMM yyyy"),
        STRING_DAY_MONTH("d MMMM"),
        TIME("HH:mm");

        private String template;

        Template(String template) {
            this.template = template;
        }

        public String get() {
            return template;
        }
    }


    public static void slideUp(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public static void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


}
