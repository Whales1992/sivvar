package com.example.sivvar.helpers;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseUtility {
    public static boolean isSuccessful(String response){
        try {
            if(response != null){
            JSONObject resObj = new JSONObject(response);
            String status = resObj.getString("status");
                return status.equalsIgnoreCase("1");
        }
        return false;
    } catch (JSONException ex) {
        ex.printStackTrace();
        return false;
    }
}

    public static String getMessage(String response){
        try {
            JSONObject resObj = new JSONObject(response);
            String message = resObj.getString("message");
            return message;
        }
        catch (JSONException e) {
            return "A temporary server error occurred, please try again soon";
        }catch (Exception e) {
            return "A temporary server error occurred, please try again soon";
        }
    }

    public static String getData(String response){
        try {
            JSONObject resObj = new JSONObject(response);
            String data = resObj.getString("responseMessage");
            return data;
        } catch (JSONException e) {
            return "A server error occurred, please try again soon";
        }
    }
}