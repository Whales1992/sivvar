package com.example.sivvar.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.models.UserModel;
import com.google.gson.Gson;

public class User {

    /**
     * Returns Users data matching @Class UserModel
     */
    public static UserModel getUserModel(Context context) {
        String u = PreferenceManager.getDefaultSharedPreferences(context).getString("user", "");
        Gson gson = new Gson();
        return gson.fromJson(u, UserModel.class);
    }


    /**
     * Stores Users data in side shared preference on successful login
     */
    public static void storeUser(String json, Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString("user", json);
        editor.apply();
    }


    /**
     * Checks if User is logged in or not
     */
    public static boolean isLoggedIn(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains("user");
    }


    /**
     * Log Users out of the device and clear all their datas store in side shared preference
     */
    public static void logOut(Context context) {
        SharedPreferences user = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = user.edit();
        editor.clear();
        editor.apply();
    }


    /**
     * Stores Users country code inside shared preference
     */
    public static void saveCountryCode(String countryCode, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("country_code", countryCode);
        editor.apply();
    }


    /**
     * Get Users country code from shared preference
     */
    public static String getCountryCode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("country_code", "");
    }


    /**
     * Stores Users device id/Imei inside shared preference
     */
    public static void saveImei(String Imei, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Imei", Imei);
        editor.apply();
    }


    /**
     * Get Users device id/Imei from shared preference
     */
    public static String getImei(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("Imei", "");
    }


    /**
     * Stores Users phone number inside shared preference
     */
    public static void savePhone(String msISDN, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("msISDN", msISDN);
        editor.apply();
    }


    /**
     * Gets Users phone number from shared preference
     */
    public static String getPhone(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("msISDN", "");
    }


    /**
     * Stores Users sivvar_stamp inside shared preference
     */
    public static void saveStamp(String msISDN, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("msISDN", msISDN);
        editor.apply();
    }


    /**
     * Gets Users sivvar_stamp from shared preference
     */
    public static String getStamp(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("msISDN", "");
    }

    /**
     * Here we store the state of SIVVAR readiness to the shared preference for onResume we can easily
     * get what state it was before onPause that resets the readiness.
     */
    public static void saveSIVVARstate(String readyState, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        if(preferences.contains("sip_state"))
            editor.remove("sip_state");

        editor.putString("sip_state", readyState);
        editor.apply();
    }


    /**
     * Gets SIVVAR saved Readiness state from shared preference
     */
    public static String getSIVVARstate(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("sip_state", "none");
    }

    public static void removeSIVVARstate(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        if(preferences.contains("sip_state"))
            editor.remove("sip_state");
    }
}