package com.example.sivvar.helpers;

interface PermissionsCallback {
    void onAccept();
    void onDecline();
}
