package com.example.sivvar.observables.categories;

public interface CategoryRepositoryObservers {
    void onFinish();
}