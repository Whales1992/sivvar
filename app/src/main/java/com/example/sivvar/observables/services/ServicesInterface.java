package com.example.sivvar.observables.services;

public interface ServicesInterface {
    void registerObserver(ServicesRepositoryObservers repositoryObserver);
    void removeObserver(ServicesRepositoryObservers repositoryObserver);
    void notifyObservers();
}
