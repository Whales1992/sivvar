package com.example.sivvar.observables.favourites;

public interface FavouriteRepositoryObservers {
    void onFinish();
}