package com.example.sivvar.observables.favouritesSubServices.favourites;

import com.example.sivvar.models.SubServices;

import java.util.ArrayList;
import java.util.List;

public class FavouritesSubServicesSubServicesObservers implements FavouritesSubServicesInterface {
    private ArrayList<FavouriteSubServicesRepositoryObservers> mObservers;

    private List<SubServices> subServicesList = new ArrayList<>();

    private static FavouritesSubServicesSubServicesObservers SINGLETON = null;
    public static FavouritesSubServicesSubServicesObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new FavouritesSubServicesSubServicesObservers();
        return SINGLETON;
    }

    private FavouritesSubServicesSubServicesObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(FavouriteSubServicesRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(FavouriteSubServicesRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (FavouriteSubServicesRepositoryObservers observer: mObservers) {
                observer.populateList(this.subServicesList);
        }
    }

    public void populateList(List<SubServices> subServicesList){
        this.subServicesList = subServicesList;
        notifyObservers();
    }
}