package com.example.sivvar.observables.favouritesSubServices.favourites;

public interface FavouritesSubServicesInterface {
    void registerObserver(FavouriteSubServicesRepositoryObservers repositoryObserver);
    void removeObserver(FavouriteSubServicesRepositoryObservers repositoryObserver);
    void notifyObservers();
}