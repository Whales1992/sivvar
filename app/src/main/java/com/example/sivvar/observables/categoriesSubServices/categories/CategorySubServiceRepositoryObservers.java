package com.example.sivvar.observables.categoriesSubServices.categories;

import com.example.sivvar.models.CategoryServices;

import java.util.List;

public interface CategorySubServiceRepositoryObservers {
    void populateList(List<CategoryServices> categoryServices);
}