package com.example.sivvar.observables.favouritesSubServices.favouritesStepDown;

import com.example.sivvar.models.SubServices;

import java.util.ArrayList;
import java.util.List;

public class FavouritesSubServicesSubServicesObserversDown implements FavouritesSubServicesInterfaceDown {
    private ArrayList<FavouriteSubServicesRepositoryObserversDown> mObservers;

    private List<SubServices> subServicesList = new ArrayList<>();

    private static FavouritesSubServicesSubServicesObserversDown SINGLETON = null;
    public static FavouritesSubServicesSubServicesObserversDown getInstance(){
        if(SINGLETON==null)
            SINGLETON = new FavouritesSubServicesSubServicesObserversDown();
        return SINGLETON;
    }

    private FavouritesSubServicesSubServicesObserversDown() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(FavouriteSubServicesRepositoryObserversDown repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(FavouriteSubServicesRepositoryObserversDown repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (FavouriteSubServicesRepositoryObserversDown observer: mObservers) {
                observer.populateList(this.subServicesList);
        }
    }

    public void populateList(List<SubServices> subServicesList){
        this.subServicesList = subServicesList;
        notifyObservers();
    }
}