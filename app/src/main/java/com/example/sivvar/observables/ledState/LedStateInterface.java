package com.example.sivvar.observables.ledState;

public interface LedStateInterface {
    void registerObserver(LedStateRepositoryObservers repositoryObserver);
    void removeObserver(LedStateRepositoryObservers repositoryObserver);
    void notifyObservers();
}