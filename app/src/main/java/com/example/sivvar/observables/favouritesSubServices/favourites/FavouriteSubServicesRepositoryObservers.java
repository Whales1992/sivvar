package com.example.sivvar.observables.favouritesSubServices.favourites;

import com.example.sivvar.models.SubServices;

import java.util.List;

public interface FavouriteSubServicesRepositoryObservers {
    void populateList(List<SubServices> subServicesList);
}