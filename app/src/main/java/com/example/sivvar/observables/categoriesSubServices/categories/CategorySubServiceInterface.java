package com.example.sivvar.observables.categoriesSubServices.categories;

public interface CategorySubServiceInterface {
    void registerObserver(CategorySubServiceRepositoryObservers repositoryObserver);
    void removeObserver(CategorySubServiceRepositoryObservers repositoryObserver);
    void notifyObservers();
}