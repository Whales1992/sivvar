package com.example.sivvar.observables.services;

import com.example.sivvar.models.CategoryServices;
import java.util.List;

public interface ServicesRepositoryObservers {
    void addServices(List<CategoryServices> categoryServices);
}