package com.example.sivvar.observables.categories;

public interface CategoryInterface {
    void registerObserver(CategoryRepositoryObservers repositoryObserver);
    void removeObserver(CategoryRepositoryObservers repositoryObserver);
    void notifyObservers();
}