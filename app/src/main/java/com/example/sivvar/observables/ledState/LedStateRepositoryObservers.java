package com.example.sivvar.observables.ledState;

public interface LedStateRepositoryObservers {
    void onUpdate(String s);
}