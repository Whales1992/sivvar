package com.example.sivvar.observables.ledState;

import java.util.ArrayList;

public class LedStateObservers implements LedStateInterface {
    private ArrayList<LedStateRepositoryObservers> mObservers;

    private static LedStateObservers SINGLETON = null;
    private String state;

    public static LedStateObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new LedStateObservers();
        return SINGLETON;
    }

    private LedStateObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(LedStateRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(LedStateRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (LedStateRepositoryObservers observer: mObservers) {
                observer.onUpdate(this.state);
        }
    }


    public void update(String state){
        this.state = state;
        notifyObservers();
    }
}