package com.example.sivvar.observables.favourites;

import com.example.sivvar.observables.favourites.FavouriteRepositoryObservers;

public interface FavouritesInterface {
    void registerObserver(FavouriteRepositoryObservers repositoryObserver);
    void removeObserver(FavouriteRepositoryObservers repositoryObserver);
    void notifyObservers();
}