package com.example.sivvar.observables.favourites;

import java.util.ArrayList;

public class FavouritesObservers implements FavouritesInterface {
    private ArrayList<FavouriteRepositoryObservers> mObservers;

    private boolean finis = false;

    private static FavouritesObservers SINGLETON = null;
    public static FavouritesObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new FavouritesObservers();
        return SINGLETON;
    }

    private FavouritesObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(FavouriteRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(FavouriteRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (FavouriteRepositoryObservers observer: mObservers) {
            if(finis)
                observer.onFinish();
        }
    }

    public void finis(){
        this.finis = true;
        notifyObservers();
    }
}