package com.example.sivvar.observables.categoriesSubServices.categories;

import com.example.sivvar.models.CategoryServices;

import java.util.ArrayList;
import java.util.List;

public class CategorySubServiceObservers implements CategorySubServiceInterface {
    private ArrayList<CategorySubServiceRepositoryObservers> mObservers;

    private List<CategoryServices> categoryServices;

    private static CategorySubServiceObservers SINGLETON = null;
    public static CategorySubServiceObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new CategorySubServiceObservers();
        return SINGLETON;
    }

    private CategorySubServiceObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(CategorySubServiceRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(CategorySubServiceRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (CategorySubServiceRepositoryObservers observer: mObservers) {
            if(categoryServices!=null)
                observer.populateList(categoryServices);
        }
    }

    public void populateList(List<CategoryServices> categoryServices){
        this.categoryServices = categoryServices;
        notifyObservers();
    }
}