package com.example.sivvar.observables.categories;

import java.util.ArrayList;

public class CategoryObservers implements CategoryInterface {
    private ArrayList<CategoryRepositoryObservers> mObservers;

    private boolean finis = false;

    private static CategoryObservers SINGLETON = null;
    public static CategoryObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new CategoryObservers();
        return SINGLETON;
    }

    private CategoryObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(CategoryRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(CategoryRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (CategoryRepositoryObservers observer: mObservers) {
            if(finis)
                observer.onFinish();
        }
    }

    public void finis(){
        this.finis = true;
        notifyObservers();
    }
}