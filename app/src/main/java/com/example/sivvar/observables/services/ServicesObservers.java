package com.example.sivvar.observables.services;

import com.example.sivvar.models.CategoryServices;
import java.util.ArrayList;
import java.util.List;

public class ServicesObservers implements ServicesInterface {
    private ArrayList<ServicesRepositoryObservers> mObservers;

    private List<CategoryServices> categoryServices = null;

    private static ServicesObservers SINGLETON = null;
    public static ServicesObservers getInstance(){
        if(SINGLETON==null)
            SINGLETON = new ServicesObservers();
        return SINGLETON;
    }

    private ServicesObservers() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(ServicesRepositoryObservers repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(ServicesRepositoryObservers repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (ServicesRepositoryObservers observer: mObservers) {
            if(categoryServices !=null)
                observer.addServices(categoryServices);
        }
    }

    public void addService(List<CategoryServices> categoryServices) {
        this.categoryServices = categoryServices;
        notifyObservers();
    }
}