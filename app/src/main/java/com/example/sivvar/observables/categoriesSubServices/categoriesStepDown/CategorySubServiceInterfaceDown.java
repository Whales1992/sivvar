package com.example.sivvar.observables.categoriesSubServices.categoriesStepDown;

public interface CategorySubServiceInterfaceDown {
    void registerObserver(CategorySubServiceRepositoryObserversDown repositoryObserver);
    void removeObserver(CategorySubServiceRepositoryObserversDown repositoryObserver);
    void notifyObservers();
}