package com.example.sivvar.observables.favouritesSubServices.favouritesStepDown;

public interface FavouritesSubServicesInterfaceDown {
    void registerObserver(FavouriteSubServicesRepositoryObserversDown repositoryObserver);
    void removeObserver(FavouriteSubServicesRepositoryObserversDown repositoryObserver);
    void notifyObservers();
}