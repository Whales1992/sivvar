package com.example.sivvar.observables.categoriesSubServices.categoriesStepDown;

import com.example.sivvar.models.CategoryServices;

import java.util.List;

public interface CategorySubServiceRepositoryObserversDown {
    void populateList(List<CategoryServices> categoryServices);
}