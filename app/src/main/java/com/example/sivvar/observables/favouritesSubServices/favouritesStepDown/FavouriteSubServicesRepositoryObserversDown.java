package com.example.sivvar.observables.favouritesSubServices.favouritesStepDown;

import com.example.sivvar.models.SubServices;

import java.util.List;

public interface FavouriteSubServicesRepositoryObserversDown {
    void populateList(List<SubServices> subServicesList);
}