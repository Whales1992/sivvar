package com.example.sivvar.observables.categoriesSubServices.categoriesStepDown;

import com.example.sivvar.models.CategoryServices;

import java.util.ArrayList;
import java.util.List;

public class CategorySubServiceObserversDown implements CategorySubServiceInterfaceDown {
    private ArrayList<CategorySubServiceRepositoryObserversDown> mObservers;

    private List<CategoryServices> categoryServices;

    private static CategorySubServiceObserversDown SINGLETON = null;
    public static CategorySubServiceObserversDown getInstance(){
        if(SINGLETON==null)
            SINGLETON = new CategorySubServiceObserversDown();
        return SINGLETON;
    }

    private CategorySubServiceObserversDown() { mObservers = new ArrayList<>(); }

    @Override
    public void registerObserver(CategorySubServiceRepositoryObserversDown repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(CategorySubServiceRepositoryObserversDown repositoryObserver) {
        mObservers.remove(repositoryObserver);
    }

    @Override
    public void notifyObservers() {
        for (CategorySubServiceRepositoryObserversDown observer: mObservers) {
            if(categoryServices!=null)
                observer.populateList(categoryServices);
        }
    }

    public void populateList(List<CategoryServices> categoryServices){
        this.categoryServices = categoryServices;
        notifyObservers();
    }
}