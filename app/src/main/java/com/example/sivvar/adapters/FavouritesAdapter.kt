package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.FavouritesRender
import com.example.sivvar.models.Favourites
import com.example.sivvar.models.MyFavourites

class FavouritesAdapter (private val list: List<MyFavourites>, private val favouritesRender: FavouritesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<FavouritesAdapter.CategoryViewHolder>(), Filterable {

    private var listt : List<MyFavourites> = list

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listt = list
                } else {
                    val filteredList = ArrayList<MyFavourites>()
                    for (row in list) {
                        if (row.service_name.startsWith(charString.toLowerCase(), true)) {
                            filteredList.add(row)
                        }
                    }
                    listt = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listt
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listt = filterResults.values as List<MyFavourites>
                notifyDataSetChanged()
            }
        }
    }

    init {
        listt = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CategoryViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
            val favourites: MyFavourites = this.listt[position]
            holder.bind(favourites, favouritesRender)
        }

    override fun getItemCount(): Int = listt.size

    override fun getItemViewType(position: Int): Int {
            return position
        }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_favourites, parent, false)) {
        private var categoryTitle: TextView? = null
        private var serviceTitle: TextView? = null
        private var serviceLogo: ImageView? = null
        private var parentt: LinearLayout? = null

        init {
            categoryTitle = itemView.findViewById(R.id.categoryTitle)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
            serviceLogo = itemView.findViewById(R.id.serviceLogo)
            parentt = itemView.findViewById(R.id.parent)
        }

        fun bind(favourites: MyFavourites, favouritesRender: FavouritesRender) {
            favouritesRender.renderFavourites(favourites, categoryTitle!!, serviceTitle!!, serviceLogo!!, parentt!!)
        }
    }
}