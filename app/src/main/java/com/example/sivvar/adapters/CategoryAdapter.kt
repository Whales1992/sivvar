package com.example.sivvar.adapters

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.CategoriesRender
import com.example.sivvar.models.Categories
import com.example.sivvar.models.SubServices


class CategoryAdapter (private val list: List<Categories>, private val categoriesRender: CategoriesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>(), Filterable {

    private var listt : List<Categories> = list

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listt = list
                } else {
                    val filteredList = ArrayList<Categories>()
                    for (row in list) {
                        if (row.category_name.startsWith(charString.toLowerCase(), true)) {
                            filteredList.add(row)
                        }
                    }
                    listt = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listt
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listt = filterResults.values as List<Categories>
                notifyDataSetChanged()
            }
        }
    }

    init {
        listt = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CategoryViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
            val categoryEntity: Categories = listt[position]
            holder.bind(categoryEntity, categoriesRender)
        }

        override fun getItemCount(): Int = listt.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_category, parent, false)) {
        private var catTitle: TextView? = null
        private var cardview: androidx.cardview.widget.CardView? = null
        private var logo: ImageView? = null

        init {
            catTitle = itemView.findViewById(R.id.catTitle)
            cardview = itemView.findViewById(R.id.cardview)
            logo = itemView.findViewById(R.id.logo)
        }

        fun bind(categories: Categories, categoriesRender: CategoriesRender) {
            categoriesRender.renderCategories(categories, catTitle!!, cardview!!, logo!!)
        }
    }
}