package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.RenderSubService
import com.example.sivvar.models.CategoryServices
import com.example.sivvar.models.SubServices
import org.jetbrains.annotations.Nullable
import java.util.ArrayList

class ServicesAdapterCat (categoryServicesL: List<CategoryServices>, private val renderSubServices: RenderSubService) : androidx.recyclerview.widget.RecyclerView.Adapter<ServicesAdapterCat.ServiceViewHolder>(), Filterable {

    private var categoryServices: List<CategoryServices> = categoryServicesL

    private var catlist: List<CategoryServices> = ArrayList()

    override fun getFilter(): Filter {
            return object : Filter() {
                override fun performFiltering(charSequence: CharSequence): FilterResults {
                    val charString = charSequence.toString()
                    if (charString.isEmpty()) {
                        catlist = categoryServices
                    } else {
                        val filteredList = ArrayList<CategoryServices>()
                        for (row in categoryServices) {
                            if (row.service_name.startsWith(charString.toLowerCase(), true)) {
                                filteredList.add(row)
                            }
                        }
                        catlist = filteredList
                    }

                    val filterResults = FilterResults()
                    filterResults.values = catlist
                    return filterResults
                }

                override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                    catlist = filterResults.values as List<CategoryServices>
                    notifyDataSetChanged()
                }
            }
    }

    init {
        catlist = categoryServices
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
            val categoryServices: CategoryServices = catlist[position]

//            holder.bind(categoryServices, renderSubServices)
        }

        override fun getItemCount(): Int {
             return catlist.size
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_subs, parent, false)) {
        private var parentt: LinearLayout? = null
        private var serviceTitle: TextView? = null
        private var categoryTitle: TextView? = null
        private var logo: ImageView? = null

        init {
            parentt = itemView.findViewById(R.id.parent)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
            categoryTitle = itemView.findViewById(R.id.categoryTitle)
            logo = itemView.findViewById(R.id.logo)
        }

//        fun bind(categoryServices: CategoryServices, render: RenderSubService) {
////            render.renderSubService(categoryServices, logo!!, serviceTitle!!, categoryTitle!!, parentt!!)
//        }
    }
}