package com.example.sivvar.adapters

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.CallhistoriesRender
import com.example.sivvar.interfaces.renders.CategoriesRender
import com.example.sivvar.models.CallHistory
import com.example.sivvar.models.Categories
import com.example.sivvar.models.SubServices


class CallHistoriesAdapter (private val list: List<CallHistory>, private val callHistoryRender: CallhistoriesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<CallHistoriesAdapter.CategoryViewHolder>(), Filterable {

    private var listt : List<CallHistory> = list
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listt = list
                } else {
                    val filteredList = ArrayList<CallHistory>()
                    for (row in list) {
                        if (row.serviceName.startsWith(charString.toLowerCase(), true)) {
                            filteredList.add(row)
                        }
                    }
                    listt = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listt
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listt = filterResults.values as List<CallHistory>
                notifyDataSetChanged()
            }
        }
    }
    init {
        listt = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CategoryViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val callHistory: CallHistory = listt[position]
        holder.bind(callHistory, callHistoryRender)
    }

    override fun getItemCount(): Int = listt.size

    override fun getItemViewType(position: Int): Int {
            return position
        }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_history, parent, false)) {
        private var parentt: ConstraintLayout? = null
        private var serviceTitle: TextView? = null
        private var logo: ImageView? = null
        private var menuBtn: ImageButton? = null

        init {
            parentt = itemView.findViewById(R.id.parent)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
            logo = itemView.findViewById(R.id.logo)
            menuBtn = itemView.findViewById(R.id.menuBtn)
        }

        fun bind(callHistory: CallHistory, callHistoriesAdapter: CallhistoriesRender) {
            callHistoriesAdapter.renderCallHistory(callHistory, parentt!!, serviceTitle!!, logo!!, menuBtn!!)
        }
    }
}