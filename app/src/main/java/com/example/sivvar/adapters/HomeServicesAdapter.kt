package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.CategoryServicesRender
import com.example.sivvar.models.CategoryServices

class HomeServicesAdapter (private val list: List<CategoryServices>, private val categoryServicesRender: CategoryServicesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<HomeServicesAdapter.ServiceViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
            val categoryServices: CategoryServices = list[position]
            holder.bind(categoryServices, categoryServicesRender)
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.service_list_item, parent, false)) {
        private var serviceLogo: ImageView? = null
        private var serviceName: TextView? = null

        init {
            serviceLogo = itemView.findViewById(R.id.serviceLogo)
            serviceName = itemView.findViewById(R.id.serviceName)
        }

        fun bind(categoryServices: CategoryServices, categoryServicesRender: CategoryServicesRender) {
            categoryServicesRender.renderServices(categoryServices, serviceName, serviceLogo)
            serviceName?.text = categoryServices.service_name
        }
    }
}