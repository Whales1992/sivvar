package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.HomeCategoriesRender
import com.example.sivvar.models.Categories


class HomeCategoryAdapter (private val list: List<Categories>, private val homeCategoriesRender: HomeCategoriesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<HomeCategoryAdapter.CategoryViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CategoryViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
            val categoryEntity: Categories = list[position]
            holder.bind(categoryEntity, homeCategoriesRender)
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.category_list_item, parent, false)) {
        private var categoryTitle: TextView? = null
        private var showmore: TextView? = null
        private var serviceList: androidx.recyclerview.widget.RecyclerView? = null

        init {
            categoryTitle = itemView.findViewById(R.id.category_title)
            showmore = itemView.findViewById(R.id.showmore)
            serviceList = itemView.findViewById(R.id.service_list)
        }

        fun bind(categories: Categories, homeCategoriesRender: HomeCategoriesRender) {
            homeCategoriesRender.renderCategories(categories, categoryTitle, showmore, serviceList)
        }
    }
}