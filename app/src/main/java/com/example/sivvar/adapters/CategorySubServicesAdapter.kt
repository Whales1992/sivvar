package com.example.sivvar.adapters

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.RenderCategorySubService
import com.example.sivvar.models.CategoryServices

class CategorySubServicesAdapter (private val list: List<CategoryServices>, private val renderCategorySubService: RenderCategorySubService) : androidx.recyclerview.widget.RecyclerView.Adapter<CategorySubServicesAdapter.ServiceViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
            val categorySubService: CategoryServices = list[position]
            holder.bind(categorySubService, renderCategorySubService)
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_subs, parent, false)) {
        private var parentt: LinearLayout? = null
        private var cardview: androidx.cardview.widget.CardView? = null
        private var logo: ImageView? = null
        private var categoryTitle: TextView? = null
        private var serviceTitle: TextView? = null

        init {
            parentt = itemView.findViewById(R.id.parent)
            cardview = itemView.findViewById(R.id.cardview)
            logo = itemView.findViewById(R.id.logo)
            categoryTitle = itemView.findViewById(R.id.categoryTitle)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
        }

        fun bind(categorySubService: CategoryServices, renderCategorySubService: RenderCategorySubService) {
            renderCategorySubService.renderCategorySubServices(categorySubService, logo!!, serviceTitle!!, categoryTitle!!, parentt!!)
        }
    }
}