package com.example.sivvar.adapters

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.RenderSubService
import com.example.sivvar.models.Categories
import com.example.sivvar.models.SubServices

class FavouriteSubServicesAdapter (private val list: List<SubServices>, private val renderSubService: RenderSubService) : androidx.recyclerview.widget.RecyclerView.Adapter<FavouriteSubServicesAdapter.ServiceViewHolder>(), Filterable {

    private var listt : List<SubServices> = list

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listt = list
                } else {
                    val filteredList = ArrayList<SubServices>()
                    for (row in list) {
                        if (row.category_name.startsWith(charString.toLowerCase(), true)) {
                            filteredList.add(row)
                        }
                    }
                    listt = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listt
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                listt = filterResults.values as List<SubServices>
                notifyDataSetChanged()
            }
        }
    }

    init {
        listt = list
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
            val subServices: SubServices = listt[position]
            holder.bind(subServices, renderSubService)
        }

        override fun getItemCount(): Int = listt.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_subs, parent, false)) {
        private var parentt: LinearLayout? = null
        private var cardview: androidx.cardview.widget.CardView? = null
        private var logo: ImageView? = null
        private var categoryTitle: TextView? = null
        private var serviceTitle: TextView? = null

        init {
            parentt = itemView.findViewById(R.id.parent)
            cardview = itemView.findViewById(R.id.cardview)
            logo = itemView.findViewById(R.id.logo)
            categoryTitle = itemView.findViewById(R.id.categoryTitle)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
        }

        fun bind(subServices: SubServices, renderSubService: RenderSubService) {
            renderSubService.renderSubService(subServices, logo!!, serviceTitle!!, categoryTitle!!, parentt!!)
        }
    }
}