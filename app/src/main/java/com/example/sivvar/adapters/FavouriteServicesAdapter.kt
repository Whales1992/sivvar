package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.FavouritesServicesRender
import com.example.sivvar.models.Favourites

class FavouriteServicesAdapter (private val list: List<Favourites>, private val favouritesServicesRender: FavouritesServicesRender) : androidx.recyclerview.widget.RecyclerView.Adapter<FavouriteServicesAdapter.ServiceViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
            val favouriteService: Favourites = list[position]
            holder.bind(favouriteService, favouritesServicesRender)
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.service_list_item, parent, false)) {
        private var serviceLogo: ImageView? = null
        private var serviceName: TextView? = null

        init {
            serviceLogo = itemView.findViewById(R.id.serviceLogo)
            serviceName = itemView.findViewById(R.id.serviceName)
        }

        fun bind(favourites: Favourites, favouritesServicesRender: FavouritesServicesRender) {
            favouritesServicesRender.renderServices(favourites, serviceName, serviceLogo)
        }
    }
}