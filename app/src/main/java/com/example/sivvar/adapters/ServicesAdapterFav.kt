package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.RenderSubService
import com.example.sivvar.models.CategoryServices
import com.example.sivvar.models.SubServices
import org.jetbrains.annotations.Nullable
import java.util.ArrayList

class ServicesAdapterFav (subServicesL: List<SubServices>, private val renderSubServices: RenderSubService) : androidx.recyclerview.widget.RecyclerView.Adapter<ServicesAdapterFav.ServiceViewHolder>(), Filterable {

    private var subServices: List<SubServices> = subServicesL

    private var favlist: List<SubServices> = ArrayList()

    override fun getFilter(): Filter {
            return object : Filter() {
                override fun performFiltering(charSequence: CharSequence): FilterResults {
                    val charString = charSequence.toString()
                    if (charString.isEmpty()) {
                        favlist = subServices
                    } else {
                        val filteredList = ArrayList<SubServices>()
                        for (row in subServices) {
                            if (row.service_name.startsWith(charString.toLowerCase(), true)) {
                                filteredList.add(row)
                            }
                        }
                        favlist = filteredList
                    }

                    val filterResults = FilterResults()
                    filterResults.values = favlist
                    return filterResults
                }

                override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                    favlist = filterResults.values as List<SubServices>
                    notifyDataSetChanged()
                }
            }
    }

    init {
        favlist = subServices
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return ServiceViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
                val subServices: SubServices = favlist[position]

                holder.bind(subServices, renderSubServices)
        }

        override fun getItemCount(): Int {
                return favlist.size
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class ServiceViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_subs, parent, false)) {
        private var parentt: LinearLayout? = null
        private var serviceTitle: TextView? = null
        private var categoryTitle: TextView? = null
        private var logo: ImageView? = null

        init {
            parentt = itemView.findViewById(R.id.parent)
            serviceTitle = itemView.findViewById(R.id.serviceTitle)
            categoryTitle = itemView.findViewById(R.id.categoryTitle)
            logo = itemView.findViewById(R.id.logo)
        }

        fun bind(subServices: SubServices, render: RenderSubService) {
            render.renderSubService(subServices, logo!!, serviceTitle!!, categoryTitle!!, parentt!!)
        }
    }
}