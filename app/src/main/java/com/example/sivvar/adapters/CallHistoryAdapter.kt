package com.example.sivvar.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.sivvar.R
import com.example.sivvar.interfaces.renders.CallhistoryRender
import com.example.sivvar.models.CallHistory

class CallHistoryAdapter (private val list: List<CallHistory>, private val callhistoryRender: CallhistoryRender) : androidx.recyclerview.widget.RecyclerView.Adapter<CallHistoryAdapter.CategoryViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CategoryViewHolder(inflater, parent)
        }

        override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
            val callHistory: CallHistory = list[position]
            holder.bind(callHistory, callhistoryRender)
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int {
            return position
        }

    class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(inflater.inflate(R.layout.item_callhistory, parent, false)) {
        private var dateTag: TextView? = null
        private var speakTime: TextView? = null
        private var timeStamp: TextView? = null

        init {
            dateTag = itemView.findViewById(R.id.dateTag)
            speakTime = itemView.findViewById(R.id.speakTime)
            timeStamp = itemView.findViewById(R.id.timeStamp)
        }

        fun bind(callHistory: CallHistory, callhistoryRender: CallhistoryRender) {
            callhistoryRender.renderCallHistory(callHistory, dateTag!!, speakTime!!, timeStamp!!)
        }
    }
}