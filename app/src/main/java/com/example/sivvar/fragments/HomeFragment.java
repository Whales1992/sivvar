package com.example.sivvar.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.activities.services.FavSubServices;
import com.example.sivvar.activities.services.FavouriteService01;
import com.example.sivvar.adapters.FavouritesAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.FavouritesRender;
import com.example.sivvar.models.MyFavourites;
import com.example.sivvar.observables.favourites.FavouriteRepositoryObservers;
import com.example.sivvar.observables.favourites.FavouritesObservers;
import com.example.sivvar.services.foregrounds.SivvarFetchFavouritesForeground;
import com.example.sivvar.services.foregrounds.WorkerResultReceiver;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sivvar.helpers.DtosKt.generateStampFavourites;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        FavouritesRender, FavouriteRepositoryObservers, WorkerResultReceiver.Receiver{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private WorkerResultReceiver workerResultReceiver;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayout parent;

    private TextView user_name, user_phone;

    private RecyclerView favouriteRecyclerView;

    private List<MyFavourites> favouritesList = new ArrayList<>();

    private SivvarLoadingDialog loadingDialog;

    private EditText search_edit;

    private static Context context;

    private FavouritesAdapter favouritesAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final LinearLayout l = (LinearLayout) inflater.inflate(R.layout.fragment_home, container, false);
        context = getContext();
        setContexts();
        findViewsByIds(l);
        setToolbar();

        favouritesList = MyFavourites.getMyFavoutires();

        if(favouritesList.isEmpty()){
            getFavourites();
//            Snackbar.make(parent, "Hi there, If you notice red(not connected) for long, simply relaunch your app.", Snackbar.LENGTH_INDEFINITE).show();
        }

        initRecyclerViewListItems();

        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                favouritesAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        keyboardDetection();

        return l;
    }

    private void keyboardDetection() {
        parent.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = parent.getRootView().getHeight() - parent.getHeight();
            if (heightDiff > Utility.dpToPx(getContext(), 200)) { // if more than 200 dp, it's probably a keyboard...
                if(MainActivity.navView!=null){
                    MainActivity.navView.setVisibility(View.GONE);
                }
            }else {
                if(MainActivity.navView!=null){
                    MainActivity.navView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void getFavourites(){
        try{
            loadingDialog.show();

            JsonObject param = new JsonObject();
            param.addProperty("msISDN", MainActivity.me.getPhoneNumber());
            param.addProperty("Identity", MainActivity.me.getImei());
            param.addProperty("FavouriteServices", SivvarKey.SIVVAR_FAV.getValue());
            param.addProperty("sivvarStamp", generateStampFavourites(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber()));

            Type type = new TypeToken<JsonObject>(){}.getType();
            String s = new Gson().toJson(param, type);
            Intent fetchFavouritesIntent = new Intent();
            fetchFavouritesIntent.putExtra(SivvarKey.FAVOURITE_JOB_KEY.getValue(), s);
            fetchFavouritesIntent.putExtra(SivvarKey.FAVOURITE_JOB_RECEIVER.getValue(), workerResultReceiver);
            fetchFavouritesIntent.setAction(SivvarKey.FAVOURITE_JOB_ACTION.getValue());

            SivvarFetchFavouritesForeground.enqueueWork(getContext(), fetchFavouritesIntent, Integer.parseInt(SivvarKey.FAVOURITE_JOB_ID.getValue()));
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    private void initRecyclerViewListItems() {
        try{
                favouritesAdapter = new FavouritesAdapter(favouritesList, this);
                LinearLayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                favouriteRecyclerView.setLayoutManager(manager);
                favouritesAdapter.setHasStableIds(true);
                ((SimpleItemAnimator) favouriteRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

                favouriteRecyclerView.setAdapter(favouritesAdapter);

                favouritesAdapter.notifyDataSetChanged();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    private void setContexts() {
        loadingDialog = new SivvarLoadingDialog(getContext());

        workerResultReceiver = new WorkerResultReceiver(new Handler());
        workerResultReceiver.setReceiver(this);

        FavouritesObservers.getInstance().registerObserver(this);
    }

    private void findViewsByIds(LinearLayout l) {
        parent = l.findViewById(R.id.parent);
        search_edit = l.findViewById(R.id.search_edit);
        user_name = l.findViewById(R.id.user_name);
        user_phone = l.findViewById(R.id.user_phone);
        favouriteRecyclerView = l.findViewById(R.id.favouriteRecyclerView);

        swipeRefreshLayout = l.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.callinggreen,
                R.color.colorAccent
        );
    }

    private void setToolbar(){
        String name = ""+MainActivity.me.getFirstName();
        user_name.setText(name);

        String phone = ""+MainActivity.me.getPhoneNumber();
        user_phone.setText(phone);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //TODO:: Handle receivers
    }

    @Override
    public void onFinish() {
        loadingDialog.dismiss();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            favouritesList = MyFavourites.getMyFavoutires();
            initRecyclerViewListItems();
        });

        handler.post(() -> {
            if(swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void renderFavourites(@NotNull MyFavourites favourites, @NotNull TextView categoryTitle,
                                 @NotNull  TextView serviceTitle, @NotNull ImageView serviceLogo,
                                 @NotNull LinearLayout parent) {
        try{
            serviceTitle.setText(favourites.getService_name());
            categoryTitle.setText(favourites.getCategory_name());

            String logo = favourites.getService_logo();

            Glide
                .with(context)
                .load(logo)
                .fitCenter()
                .addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(() -> Glide.with(context).load(R.drawable.swiden).into(serviceLogo));
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            })
                    .into(serviceLogo);

            parent.setOnClickListener(v -> {
                favourites.setUsage(favourites.getUsage()+1);
                favourites.save();

                if(favourites.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
                    String s = new Gson().toJson(favourites, new TypeToken<MyFavourites>(){}.getType());
                    Utility.navigateWithParams(getActivity(), new FavSubServices(), "favourites", s);
                }else{
                    String s = new Gson().toJson(favourites, new TypeToken<MyFavourites>(){}.getType());
                    Utility.navigateWithParams(getActivity(), new FavouriteService01(), "service", s);
                }
            });
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onRefresh() {
        getFavourites();
    }

    @Override
    public void onResume() {
        super.onResume();

        favouritesList = MyFavourites.getMyFavoutires();
        initRecyclerViewListItems();
    }
}