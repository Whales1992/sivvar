package com.example.sivvar.fragments;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.activities.services.CatSubServices;
import com.example.sivvar.adapters.CategoryAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.CategoriesRender;
import com.example.sivvar.models.Categories;
import com.example.sivvar.observables.categories.CategoryObservers;
import com.example.sivvar.observables.categories.CategoryRepositoryObservers;
import com.example.sivvar.services.foregrounds.SivvarFetchCategoriesForeground;
import com.example.sivvar.services.foregrounds.WorkerResultReceiver;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.jetbrains.annotations.NotNull;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.os.Looper.getMainLooper;
import static com.example.sivvar.helpers.DtosKt.generateStampCategory;

public class CategoryFragment extends Fragment implements CategoriesRender,
        CategoryRepositoryObservers, WorkerResultReceiver.Receiver{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView categoriesList;
    private CategoryAdapter adapter;
    private List<Categories> categories = new ArrayList<>();

    private WorkerResultReceiver workerResultReceiver;

    private EditText search_edit;

    private SivvarLoadingDialog loadingDialog;

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final LinearLayout l = (LinearLayout) inflater.inflate(R.layout.fragment_category, container, false);

        TextView title = l.findViewById(R.id.title);
        title.setText("Categories");

        setContexts();
        findViewByIds(l);

        categories = Categories.getCategories();

        initList();

        if(categories.isEmpty()){
            getCategories();
        }

        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        return l;
    }

    private void setContexts() {
        loadingDialog = new SivvarLoadingDialog(getContext());

        workerResultReceiver = new WorkerResultReceiver(new Handler());
        workerResultReceiver.setReceiver(this);

        CategoryObservers.getInstance().registerObserver(this);
    }

    private void findViewByIds(LinearLayout l){
        categoriesList = l.findViewById(R.id.categoriesList);
        search_edit = l.findViewById(R.id.search_edit);
    }

    private void initList(){
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        if(categories!=null){
            adapter = new CategoryAdapter(categories, this);
            categoriesList.setAdapter(adapter);
            categoriesList.setLayoutManager(layoutManager);

            adapter.notifyDataSetChanged();
        }
    }

    private void getCategories(){
        try{
            loadingDialog.show();

            JsonObject param = new JsonObject();
            param.addProperty("msISDN", MainActivity.me.getPhoneNumber());
            param.addProperty("Identity",MainActivity.me.getImei());
            param.addProperty("Categories", SivvarKey.SIVVAR_CATS.getValue());
            param.addProperty("sivvarStamp", generateStampCategory(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber()));

            Type type = new TypeToken<JsonObject>(){}.getType();
            String s = new Gson().toJson(param, type);
            Intent fetchCatHistory = new Intent();
            fetchCatHistory.putExtra(SivvarKey.CATEGORY_JOB_KEY.getValue(), s);
            fetchCatHistory.putExtra(SivvarKey.CATEGORY_JOB_RECEIVER.getValue(), workerResultReceiver);
            fetchCatHistory.setAction(SivvarKey.CATEGORY_JOB_ACTION.getValue());

            SivvarFetchCategoriesForeground.enqueueWork(getContext(), fetchCatHistory, Integer.parseInt(SivvarKey.CATEGORY_JOB_ID.getValue()));
        }catch (Exception ex){
            Log.e("Exceptionn", ""+ex.getMessage());
        }
    }

    @Override
    public void renderCategories(@NotNull Categories categories, @NotNull TextView category_title, @NotNull CardView cardView, @NotNull ImageView logo) {
        try{
            String title = ""+categories.getCategory_name();
            category_title.setText(title);

            String logoLink = ""+categories.getCategory_logo();

            Glide
                .with(getContext())
                .load(logoLink)
                .fitCenter()
                .addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
               Handler handler = new Handler(getMainLooper());
               handler.post(() -> Glide.with(getContext()).load(R.drawable.swiden).into(logo));
               return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(logo);

            cardView.setOnClickListener(v -> {
                String s = new Gson().toJson(categories, new TypeToken<Categories>(){}.getType());
                Utility.navigateWithParams(getActivity(), new CatSubServices(), "category", s);
            });

        }catch (Exception ex){
            Log.e("EXCEPTION", ""+ex.getMessage());
        }
    }

    @Override
    public void onFinish() {
        loadingDialog.dismiss();
        categories = Categories.getCategories();
        initList();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //TODO:: .....
    }
}