package com.example.sivvar.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sivvar.R;
import com.example.sivvar.activities.settings.About;
import com.example.sivvar.activities.settings.Feedbacks;
import com.example.sivvar.activities.settings.Notifications;
import com.example.sivvar.activities.settings.Profile;
import com.example.sivvar.activities.settings.Support;
import com.example.sivvar.helpers.Utility;

public class SettingsFragment extends Fragment{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public SettingsFragment() {
        // Required empty public constructor
    }

    private String appVersion;
    private TextView showVersion, versionInfo, profile, notifications, feedbacks, support, about, exitapp;
    private Button getUpdates;
    private LinearLayout update;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ConstraintLayout l = (ConstraintLayout) inflater.inflate(R.layout.fragment_settings, container, false);

        TextView title = l.findViewById(R.id.title);
        title.setText("Settings");

        getAppVerion(getContext());

        findViewByIds(l);
        setContents();
        setClickListeners();

        return l;
    }

    private void setClickListeners() {
        profile.setOnClickListener(v -> Utility.navigate(getActivity(), new Profile()));

        notifications.setOnClickListener(v -> Utility.navigate(getActivity(), new Notifications()));

        feedbacks.setOnClickListener(v -> Utility.navigate(getActivity(), new Feedbacks()));

        support.setOnClickListener(v -> Utility.navigate(getActivity(), new Support()));

        about.setOnClickListener(v -> Utility.navigate(getActivity(), new About()));

        update.setOnClickListener(v -> Toast.makeText(getContext(), "Thank you, but you already have the latest version.", Toast.LENGTH_LONG).show());

        exitapp.setOnClickListener(v -> logoutDialog());
    }

    private void setContents() {
        showVersion.setText("v"+appVersion);

        String info = "You have the latest update";
        versionInfo.setText(info);
    }

    private void findViewByIds(ConstraintLayout l) {
        showVersion = l.findViewById(R.id.showVersion);
        versionInfo = l.findViewById(R.id.versionInfo);
        getUpdates = l.findViewById(R.id.getUpdates);

        profile = l.findViewById(R.id.profile);
        notifications = l.findViewById(R.id.notifications);
        feedbacks = l.findViewById(R.id.feedbacks);
        support = l.findViewById(R.id.support);
        about = l.findViewById(R.id.about);
        update = l.findViewById(R.id.update);
        exitapp = l.findViewById(R.id.exitapp);
    }

    private void getAppVerion(Context context){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Simply show a dialog to confirm if a user really want to log out.
     */
    private void logoutDialog(){
        try{
            new AlertDialog.Builder(getContext())
                    .setTitle("Exit SIVVAR")
                    .setMessage("Are you sure you want to exit ?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", (dialog, which) -> {
                            dialog.dismiss();
                            getActivity().finish();
                    })

                    .setNegativeButton("No", (dialog, which) ->{
                        dialog.dismiss();
                    })
                    .show();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }
}