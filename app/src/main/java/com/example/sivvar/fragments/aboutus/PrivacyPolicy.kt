package com.example.sivvar.fragments.aboutus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sivvar.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*

class PrivacyPolicy : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = false
        webView.loadUrl("https://sivvarpage.github.io/Privacypolicy/SIVVAR%20PRIVACY%20POLICY.html")
    }
}
