package com.example.sivvar.fragments;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.adapters.CallHistoriesAdapter;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.CallhistoriesRender;
import com.example.sivvar.models.CallHistory;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static android.os.Looper.getMainLooper;

public class CallHistoryFragment extends Fragment implements CallhistoriesRender {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView historyList;
    private TextView emptyList;

    private EditText search_edit;

    private LinearLayout l;

    private List<CallHistory> callHistoryList = new ArrayList<>();
    private CallHistoriesAdapter adapter;

    public CallHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final LinearLayout l = (LinearLayout) inflater.inflate(R.layout.fragment_history, container, false);

        this.l = l;

        TextView title = l.findViewById(R.id.title);
        title.setText("Call History");

        findViewByIds(l);

        initList();

        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });
        return l;
    }

    private void findViewByIds(LinearLayout l){
        historyList = l.findViewById(R.id.historyList);
        search_edit = l.findViewById(R.id.search_edit);
        emptyList = l.findViewById(R.id.emptyList);
    }

    @Override
    public void onResume(){
        super.onResume();
        initList();
    }

    private void initList(){
        callHistoryList = CallHistory.getCallHistory();

        adapter = new CallHistoriesAdapter(callHistoryList, this);
        historyList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        historyList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(callHistoryList.isEmpty()){
            emptyList.setVisibility(View.VISIBLE);
        }else{
            emptyList.setVisibility(View.GONE);
        }
    }

    @Override
    public void renderCallHistory(@NotNull CallHistory callHistory, @NotNull ConstraintLayout parent, @NotNull TextView serviceTitle, @NotNull ImageView logo, @NotNull ImageButton menuBtn) {
        serviceTitle.setText(callHistory.serviceName);

        String logoLink = ""+callHistory.logo;

        Glide
            .with(getContext())
            .load(logoLink)
            .fitCenter()
            .centerCrop().addListener(new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            Handler handler = new Handler(getMainLooper());
            handler.post(() -> Glide.with(getContext()).load(R.drawable.swiden).into(logo));
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            return false;
        }
        })
        .into(logo);

        menuBtn.setOnClickListener(v -> optionMenu(menuBtn, callHistory));

        parent.setOnClickListener(v -> optionMenu(menuBtn, callHistory));

    }

   /**
    * This is to confirm if a user really want to delete a perticular history
   */
   private void optionMenu(View popupButton, CallHistory callHistory){
       try{
           PopupMenu p = new PopupMenu(getContext(), popupButton);
           p.getMenuInflater().inflate(R.menu.call_history_menu, p .getMenu());

           p.setOnMenuItemClickListener(item -> {
               if(item.getTitle().toString().equalsIgnoreCase("Details")){
                   String date = Utility.format(callHistory.date, "");
                   new AlertDialog.Builder(getContext())
                           .setTitle(callHistory.serviceName)
                           .setMessage("Talk time: "+callHistory.speakTime+"\n"+"Date & Time: "+date)
                           .show();
               }else if(item.getTitle().toString().equalsIgnoreCase("Delete")){
                   new AlertDialog.Builder(getContext())
                           .setTitle("Delete History")
                           .setMessage("Are you sure you want to perform this action ?s")
                           .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                           .setPositiveButton("Yes", (dialog, which) ->{
                               callHistory.delete();
                               dialog.dismiss();
                               initList();
                           })
                           .show();
               }
//               else{
//                   new CallingDialog(this, null, null, subServices, l.getWidth(), l.getHeight());
//               }

               return true;
           });

           p.show();
       }catch (Exception ex){
           Log.e("Exception", ""+ex.getMessage());
       }
   }
}