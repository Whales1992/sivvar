package com.example.sivvar.activities.services;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.adapters.CategorySubServicesAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.RenderCategorySubService;
import com.example.sivvar.models.Categories;
import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.categoriesSubServices.categories.CategorySubServiceObservers;
import com.example.sivvar.observables.categoriesSubServices.categories.CategorySubServiceRepositoryObservers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.example.sivvar.helpers.DtosKt.generateStampServicesCategory;

public class CatSubServices extends AppCompatActivity implements CategorySubServiceRepositoryObservers, RenderCategorySubService, SwipeRefreshLayout.OnRefreshListener {
    private Categories category;
    private List<CategoryServices> categoryServicesList = new ArrayList<>();
    private RecyclerView recyclerView;

    private SwipeRefreshLayout pullRefresh;
    private TextView emptyList;

    private SivvarLoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_services);
        CategorySubServiceObservers.getInstance().registerObserver(this);
        loadingDialog = new SivvarLoadingDialog(this);

        findViewByIds();

        try{
            if(getIntent().hasExtra("category")){
                String s = getIntent().getStringExtra("category");
                category = new Gson().fromJson(s, new TypeToken<Categories>(){}.getType());
                Utility.initToolbar(this,  true, category.getCategory_name(), false);
            }
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }

//        List<CategoryServices> categoryServices = CategoryServices.getCategoryServicesById(category.getCategory_id());
//        if(categoryServices.isEmpty()){
            getServices(category.getCategory_id());
//        }else{
//            categoryServicesList = categoryServices;
//            initList();
//        }
    }

    private void findViewByIds(){
        recyclerView = findViewById(R.id.recyclerView);
        emptyList = findViewById(R.id.emptyList);

        pullRefresh = findViewById(R.id.pullRefresh);
        pullRefresh.setOnRefreshListener(this);
    }

    private void initList(){
        try{
            CategorySubServicesAdapter adapter = new CategorySubServicesAdapter(categoryServicesList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            if(categoryServicesList.isEmpty()){
                emptyList.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }else{
                emptyList.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void populateList(List<CategoryServices> categoryServices) {
        this.categoryServicesList = categoryServices;

        Handler handler = new Handler(getMainLooper());
        handler.post(this::initList);

        handler.post(() -> {
            if(categoryServices == null || categoryServices.isEmpty()){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
    * This function does the fetching and save locally for faster loading experience afterward.
    * @param categoryId  is the needed id to fetch list of services that belongs to each of the categories
    */
    private void getServices(String categoryId){
        loadingDialog.show();

        JsonObject param = new JsonObject();
        param.addProperty("msISDN", MainActivity.me.getPhoneNumber());
        param.addProperty("Identity",MainActivity.me.getImei());
        param.addProperty("categoryID", categoryId);
        param.addProperty("sivvarStamp", generateStampServicesCategory(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber(), categoryId));

        ApiCalls apiCalls = new ApiCalls("services", this, 60, 60, 60);
        apiCalls.getServiceCategories(param, new ResponseCallback() {
            @Override
            public String onSuccess(String res) {
                loadingDialog.cancel();
                pullRefresh.setRefreshing(false);

                if(ResponseUtil.Companion.isSuccessfullV3(res)){
                    Log.e("SUCCESS", ""+res);
                    String s = ResponseUtil.Companion.getResponseDataV3(res);

                    List<CategoryServices> categoryServices = CategoryServices.arrayCategoryServicesFromData(s);
                    List<CategoryServices> parsableList = new ArrayList<>();

                    for(CategoryServices categoryServices1 : categoryServices){
                        CategoryServices categoryServices2 = new CategoryServices(
                                categoryServices1.getService_dial_code(),
                                categoryId,
                                categoryServices1.getService_name(),
                                categoryServices1.getHas_sub_service(),
                                categoryServices1.getIs_audio_service(),
                                categoryServices1.getService_status(),
                                categoryServices1.getService_logo(),
                                categoryServices1.getService_id(),
                                categoryServices1.getIs_video_service(),
                                categoryServices1.getService_description(),
                                categoryServices1.getCategory_name());

                        categoryServices2.save();
                        parsableList.add(categoryServices2);
                    }

                    CategorySubServiceObservers.getInstance().populateList(parsableList);
                } else{
                    showAlertDialog(ResponseUtil.Companion.getMessage(res));
                    Log.e("ERROR", ""+res);
                }
                return null;
            }

            @Override
            public String onFailure(String res) {
                pullRefresh.setRefreshing(false);
                loadingDialog.cancel();

                Log.e("FAILED", ""+res);
                showAlertDialog("Something went wrong, please confirm that you have internet connection and pull this page to refresh or try again later");
                return null;
            }
        });
    }

    /**
     * Simply show a dialog and takes the arguments that fit it purpose of showing.
     */
    private void showAlertDialog(String msg){
        try{
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Service not available")
                    .setNegativeButton("Dismiss", (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    })
                    .show();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onRefresh() {
        try{
            getServices(category.getCategory_id());
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void renderCategorySubServices(@NotNull CategoryServices subServices,
                                          @NotNull ImageView serviceLogo, @NotNull TextView serviceName,
                                          @NotNull TextView catTitle, @NotNull LinearLayout parent) {
        String logoLink = ""+subServices.getService_logo();

        Glide
                .with(this)
                .load(logoLink)
                .centerCrop().addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Handler handler = new Handler(getMainLooper());
                handler.post(() -> Glide.with(CatSubServices.this).load(R.drawable.swiden).into(serviceLogo));
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        })
                .into(serviceLogo);

        serviceName.setText(subServices.getService_name());
        catTitle.setText(subServices.getCategory_name());

        parent.setOnClickListener(v -> {
            if(subServices.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
                String s = new Gson().toJson(subServices, new TypeToken<CategoryServices>(){}.getType());
                Utility.navigateWithParams(this, new CatSubServicesStepDown(), "subServices", s);
            }else{
                String s = new Gson().toJson(subServices, new TypeToken<CategoryServices>(){}.getType());
                Utility.navigateWithParams(this, new CategoryService02(), "service", s);
            }
        });
    }

}