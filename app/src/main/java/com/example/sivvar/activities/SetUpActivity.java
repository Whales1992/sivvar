//package com.example.sivvar.activities;
//
//import android.Manifest;
//import android.app.AlertDialog;
//import android.app.ProgressDialog;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.LocationManager;
//import android.os.Handler;
//import android.os.Looper;
//import android.provider.Settings;
//import androidx.core.app.ActivityCompat;
//import androidx.core.content.ContextCompat;
//import androidx.appcompat.app.AppCompatActivity;
//import android.os.Bundle;
//import android.telephony.TelephonyManager;
//import android.text.SpannableString;
//import android.text.Spanned;
//import android.text.TextPaint;
//import android.text.method.LinkMovementMethod;
//import android.text.style.ClickableSpan;
//import android.util.Log;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.sivvar.fragments.aboutus.PrivacyPolicy;
//import com.example.sivvar.fragments.aboutus.TermsOfServices;
//import com.example.sivvar.helpers.PhoneRegisterParams;
//import com.example.sivvar.helpers.SivvarLoadingDialog;
//import com.example.sivvar.helpers.Utility;
//import com.example.sivvar.helpers.VerifyPhoneRegisterParams;
//import com.example.sivvar.network.ApiCalls;
//import com.example.sivvar.network.ResponseCallback;
//import com.example.sivvar.network.ResponseUtil;
//import com.example.sivvar.R;
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.google.gson.reflect.TypeToken;
//import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
//import java.lang.reflect.Method;
//import java.lang.reflect.Type;
//
//import static com.example.sivvar.helpers.DtosKt.generateStamp;
//import static com.example.sivvar.helpers.DtosKt.generateStampSMS;
//
//public class SetUpActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {
//    private SearchableSpinner spinnerCountries;
//    private String[] countryCode;
//    private String selectedCountryCode;
//    private ImageView arrow_down, flag;
//    private Button i_agree;
//    private SivvarLoadingDialog sivvarLoadingDialog;
//    private TextView terms, connection_state, privacyPolicy;
//    private EditText phone_field;
//    private String ip="";
//
//    private AlertDialog alertDialog;
//
//    private AlertDialog.Builder builder;
//
//    private static final int PERMISSION_READ_STATE = 1101;
//    private static final int PERMISSION_READ_CALL_LOG = 1111;
//    private static final int PERMISSION_AUDIO = 1011;
//    private static final int PERMISSION_CAMERA = 1001;
//    private static final int PERMISSION_LOCATION = 1112;
//
//    private Handler mHandler = new Handler();
//
//    private boolean retry;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_set_up);
//
//        statusCheck();
//
//        setContexts();
//        findViewByIds();
//        setFlagData();
//
//        setClickablePoliciesText();
//
//        setOnclickListeners();
//    }
//
//    private void setClickablePoliciesText() {
//        try{
//            SpannableString s = new SpannableString(getResources().getString(R.string.privacy));
//            ClickableSpan clickableSpan = new ClickableSpan() {
//                @Override
//                public void onClick(View textView) {
//                    Utility.navigate(SetUpActivity.this, new TermsOfServices());
//                }
//                @Override
//                public void updateDrawState(TextPaint ds) {
//                    super.updateDrawState(ds);
//                    ds.setUnderlineText(false);
//                }
//            };
//            s.setSpan(clickableSpan, 31, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//            terms.setText(s);
//            terms.setMovementMethod(LinkMovementMethod.getInstance());
//            terms.setHighlightColor(Color.GREEN);
//
//
//            SpannableString ss = new SpannableString(getResources().getString(R.string.privacy0));
//            ClickableSpan clickableSpann = new ClickableSpan() {
//                @Override
//                public void onClick(View textView) {
//                    Utility.navigate(SetUpActivity.this, new PrivacyPolicy());
//                }
//                @Override
//                public void updateDrawState(TextPaint ds) {
//                    super.updateDrawState(ds);
//                    ds.setUnderlineText(false);
//                }
//            };
//            ss.setSpan(clickableSpann, 20, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//            privacyPolicy.setText(ss);
//            privacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
//            privacyPolicy.setHighlightColor(Color.GREEN);
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private void setContexts() {
//        try{
//            sivvarLoadingDialog = new SivvarLoadingDialog(this);
//
//            NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();
//            networkStateReceiver.addListener(this);
//
//            mProgress = new ProgressDialog(this);
//            mProgress.setCancelable(false);
//            mProgress.setIndeterminate(true);
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private void setOnclickListeners() {
//        try{
//            arrow_down.setOnClickListener(v -> spinnerCountries.callOnClick());
//
//            flag.setOnClickListener(v -> spinnerCountries.callOnClick());
//
//            i_agree.setOnClickListener(v ->{
//                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
//                        == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
//                        == PackageManager.PERMISSION_GRANTED) {
//                    requestIVR();
////                    requestPhoneCall();
////                    requestSMS();
//                }else
//                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALL_LOG}, PERMISSION_READ_STATE);
//            });
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private void setFlagData() {
//        try{
//            countryCode = new String[]{"+93", "+355", "+213", "+1684", "+376", "+244", "+1264",
//                    "+0", "+1268", "+54", "+374", "+297", "+61", "+43", "+994", "+1242", "+973",
//                    "+880", "+1246", "+375", "+32", "+501", "+229", "+1441", "+975", "+591", "+387",
//                    "+267", "+0", "+55", "+246", "+673", "+359", "+226", "+257", "+855", "+237", "+1",
//                    "+238", "+1345", "+236", "+235", "+56", "+86", "+61", "+672", "+57", "+269", "+242",
//                    "+242", "+682", "+506", "+225", "+385", "+53", "+357", "+420", "+45", "+253", "+1767",
//                    "+1809", "+670", "+593", "+20", "+503", "+240", "+291", "+372", "+251", "+61", "+500",
//                    "+298", "+679", "+358", "+33", "+594", "+689", "+0", "+241", "+220", "+995", "+49",
//                    "+233", "+350", "+30", "+299", "+1473", "+590", "+1671", "+502", "+44", "+224", "+245",
//                    "+592", "+509", "+0", "+504", "+852", "+36", "+354", "+91", "+62", "+98", "+964", "+353",
//                    "+972", "+39", "+1876", "+81", "+44", "+962", "+7", "+254", "+686", "+850", "+82",
//                    "+965", "+996", "+856", "+371", "+961", "+266", "+231", "+218", "+423", "+370", "+352",
//                    "+853", "+389", "+261", "+265", "+60", "+960", "+223", "+356", "+44", "+692", "+596",
//                    "+222", "+230", "+269", "+52", "+691", "+373", "+377", "+976", "+1664", "+212", "+258",
//                    "+95", "+264", "+674", "+977", "+599", "+31", "+687", "+64", "+505", "+227", "+234",
//                    "+683", "+672", "+1670", "+47", "+968", "+92", "+680", "+970", "+507", "+675", "+595",
//                    "+51", "+63", "+0", "+48", "+351", "+1787", "+974", "+262", "+40", "+70", "+250", "+290",
//                    "+1869", "+1758", "+508", "+1784", "+684", "+378", "+239", "+966", "+221", "+381", "+248",
//                    "+232", "+65", "+421", "+386", "+44", "+677", "+252", "+27", "+0", "+211", "+34",
//                    "+94", "+249", "+597", "+47", "+268", "+46", "+41", "+963", "+886", "+992", "+255",
//                    "+66", "+228", "+690", "+676", "+1868", "+216", "+90", "+7370", "+1649", "+688", "+256",
//                    "+380", "+971", "+44", "+1", "+1", "+598", "+998", "+678", "+39", "+58", "+84", "+1284",
//                    "+1340", "+681", "+212", "+967", "+38", "+260", "+263"};
//            ArrayAdapter countriesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryCode);
//            spinnerCountries.setAdapter(countriesAdapter);
//
//            Handler handler = new Handler();
//            handler.postDelayed(() -> {
//                try {
//                    spinnerCountries.setSelection(159);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }, 500);
//
//            spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    selectedCountryCode = countryCode[spinnerCountries.getSelectedItemPosition()];
//                    witchFlags(spinnerCountries.getSelectedItemPosition());
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private void findViewByIds() {
//        try{
//            spinnerCountries = findViewById(R.id.spinnerCountries);
//            arrow_down = findViewById(R.id.arrow_down);
//            flag = findViewById(R.id.flag);
//            i_agree = findViewById(R.id.i_agree);
//            terms = findViewById(R.id.terms);
//            privacyPolicy = findViewById(R.id.privacyPolicy);
//            phone_field = findViewById(R.id.phone_field);
//            connection_state = findViewById(R.id.connection_state);
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//    }
//
//    private void witchFlags(int position){
//        switch (position){
//            case 0:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 1:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 2:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 3:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 4:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 5:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 6:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 7:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 8:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 9:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 10:
//                flag.setImageResource(R.drawable.unknownflag);
//                break;
//            case 159:
//                flag.setImageResource(R.drawable.nigeria_flag);
//                break;
//            case 93:
//                flag.setImageResource(R.drawable.swiden);
//                break;
//
//        }
//    }
//
//    /**
//    * Verify signin or signup using phone call method
//    */
//    private void requestPhoneCall(){
//        if(phone_field.getText().toString().trim().isEmpty()){
//            Log.e("COUNTRY-CODE", selectedCountryCode+"");
//            phone_field.setError("Cannot Be empty!!!");
//            return;
//        }
//
//        try{
//            sivvarLoadingDialog.show();
//
//            String phone = phone_field.getText().toString();
//            phone = sterilizePhoneNumber(phone);
//
//            phone  = selectedCountryCode.replace("+", "")+phone;
//            String uuID = getDeviceImei();
//
//            ApiCalls apiCalls = new ApiCalls("init", this, 30, 30, 40);
//            String finalPhone = phone;
//            apiCalls.initByPhoneCall(new PhoneRegisterParams(phone, uuID, retry), new ResponseCallback() {
//                @Override
//                public String onSuccess(String res) {
//
//                    sivvarLoadingDialog.dismiss();
//
//                    if(ResponseUtil.Companion.isSuccessfull(res)){
//                        String data = ResponseUtil.Companion.getData(res);
//                        Log.e("DATA", ""+data);
//                        Log.e("RES DATA", ""+ResponseUtil.Companion.getResponseDataV3(res));
//
//                        beginListenCall(ResponseUtil.Companion.getResponseDataV3(res));
//                    } else if(ResponseUtil.Companion.existing(res)){
//                        Intent i = new Intent(SetUpActivity.this, CreateProfile.class);
//
//                        JsonObject data = new JsonObject();
//                        data.addProperty("phone", finalPhone);
//                        data.addProperty("countryCode", selectedCountryCode.replace("+", ""));
//                        data.addProperty("Imei", uuID);
//                        data.addProperty("responseData", ResponseUtil.Companion.getData(res));
//
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<JsonObject>(){}.getType();
//                        String d = gson.toJson(data, type);
//
//                        Utility.navigateWithParams(SetUpActivity.this, new CreateProfile(), "data", d);
//                        startActivity(i);
//                        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
//                    }else{
//                        showDialog("Exception", ResponseUtil.Companion.getMessage(res));
//                    }
//
//                    return null;
//                }
//
//                @Override
//                public String onFailure(String res) {
//                    sivvarLoadingDialog.dismiss();
//
//                    showDialog("Exception", res);
//                    return null;
//                }
//            });
//
//        }catch (Exception ex){
//            Log.e("EXCEPTION", ex.getMessage()+"");
//
//            showDialog("Exception", ex.getMessage());
//        }
//    }
//
//    /**
//    * This is to make sure phone number is well formatted.
//    */
//    private String sterilizePhoneNumber(String phone) {
//        if(phone.startsWith("+234")){
//            phone = phone.replace("+234", "");
//            if(phone.startsWith("0"))
//                phone = phone.replaceFirst("0", "");
//        }else if(phone.startsWith("234")){
//            phone = phone.replace("234", "");
//            if(phone.startsWith("0"))
//                phone = phone.replaceFirst("0", "");
//        }else if(phone.startsWith("+")){
//            phone = phone.replace("+", "");
//            if(phone.startsWith("0"))
//                phone = phone.replaceFirst("0", "");
//        }else{
//            if(phone.startsWith("0"))
//                phone = phone.replaceFirst("0", "");
//        }
//
//        return phone;
//    }
//
//    /**
//     * Verify signin or signup using sms method
//     */
//    private void requestSMS(){
//        if(phone_field.getText().toString().trim().isEmpty()){
//            phone_field.setError("Cannot Be empty!!!");
//            return;
//        }
//
//        try{
//            sivvarLoadingDialog.show();
//
//            String phone = phone_field.getText().toString();
//            phone = sterilizePhoneNumber(phone);
//
//            phone  = selectedCountryCode.replace("+", "")+phone;
//            String uuID = getDeviceImei();
//
//            ApiCalls apiCalls = new ApiCalls("init_new", this, 60, 60, 60);
//            JsonObject object = new JsonObject();
//            object.addProperty("msISDN", phone);
//            object.addProperty("Imei", uuID);
//            object.addProperty("sivvarStamp", generateStampSMS(uuID, phone));
//            String finalPhone = phone;
//
//            Log.e("SMS API PARAM", ""+object.toString());
//
//            apiCalls.initBySms(object, new ResponseCallback() {
//                @Override
//                public String onSuccess(String res) {
//                    sivvarLoadingDialog.dismiss();
//
//                    if(ResponseUtil.Companion.isSuccessfullIVR(res)){
//                        JsonObject param = new JsonObject();
//                        param.addProperty("phone", finalPhone);
//                        param.addProperty("countryCode", selectedCountryCode.replace("+", ""));
//                        param.addProperty("Imei", uuID);
//                        param.addProperty("channel", "SMS");
//                        param.addProperty("requestCode", ResponseUtil.Companion.getDataIVR(res));
//
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<JsonObject>(){}.getType();
//                        String d = gson.toJson(param, type);
//
//                        Utility.navigateWithParams(SetUpActivity.this, new OtpActivity(), "data", d);
//                        finish();
//                    }else if(ResponseUtil.Companion.isSuccessfullIVRretuning(res)){
//                        JsonObject param = new JsonObject();
//                        param.addProperty("phone", finalPhone);
//                        param.addProperty("countryCode", selectedCountryCode.replace("+", ""));
//                        param.addProperty("Imei", uuID);
//                        param.addProperty("requestCode", ResponseUtil.Companion.getDataIVR(res));
//
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<JsonObject>(){}.getType();
//                        String d = gson.toJson(param, type);
//
//                        Utility.navigateWithParams(SetUpActivity.this, new CreateProfile(), "data", d);
//                        finish();
//                    }else{
//                        showDialog("Exception", ResponseUtil.Companion.getMessage(res));
//                    }
//
//                    return null;
//                }
//
//                @Override
//                public String onFailure(String res) {
//                    sivvarLoadingDialog.dismiss();
//
//                    showDialog("Exception", res);
//                    return null;
//                }
//            });
//
//        }catch (Exception ex){
//            showDialog("Exception", ex.getMessage());
//        }
//    }
//
//    /**
//     * Verify signin or signup using ivr method
//     */
//    private void requestIVR(){
//        if(phone_field.getText().toString().trim().isEmpty()){
//            Log.e("COUNTRY-CODE", selectedCountryCode+"");
//            phone_field.setError("Cannot Be empty!!!");
//            return;
//        }
//
//        try{
//            sivvarLoadingDialog.show();
//
//            String phone = phone_field.getText().toString();
//            phone = sterilizePhoneNumber(phone);
//
//            phone  = selectedCountryCode.replace("+", "")+phone;
//            String uuID = getDeviceImei();
//
//            ApiCalls apiCalls = new ApiCalls("init_new", this, 30, 30, 40);
//            JsonObject param = generateStamp(String.valueOf(retry), uuID, phone);
//            String finalPhone = phone;
//            apiCalls.ivrphonecallverification(param, new ResponseCallback() {
//                @Override
//                public String onSuccess(String res) {
//
//                    sivvarLoadingDialog.dismiss();
//
//                    if(ResponseUtil.Companion.isSuccessfullIVR(res)){
//                        param.addProperty("countryCode", selectedCountryCode.replace("+", ""));
//                        param.addProperty("phone", finalPhone);
//                        param.addProperty("Imei", uuID);
//                        param.addProperty("channel", "VOICE");
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<JsonObject>(){}.getType();
//                        String d = gson.toJson(param, type);
//
//                        Utility.navigateWithParams(SetUpActivity.this, new OtpActivity(), "data", d);
//                        finish();
//                    }else{
//                        showDialog("Exception", ResponseUtil.Companion.getMessage(res));
//                    }
//
//                    return null;
//                }
//
//                @Override
//                public String onFailure(String res) {
//                    sivvarLoadingDialog.dismiss();
//
//                    showDialog( "Exception", "Something went wrong, Please confirm you have internet connection and try again.");
//                    return null;
//                }
//            });
//
//        }catch (Exception ex){
//            showDialog( "Exception", "Something went wrong, Please confirm you have internet connection and try again.");
//        }
//
//        retry = true;
//    }
//
//    /**
//     * This is the method that gets the devie IMEI/ID this method is not well tested on all devices yet
//     * so you might need to modify when necessary.
//     */
//    public String getDeviceImei() {
//        try{
//            TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//            if (mTelephonyManager != null) {
//                if (ActivityCompat.checkSelfPermission(SetUpActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                            ip = mTelephonyManager.getDeviceId();
//                            if (ip == null)
//                                ip = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//                        }
//
//
//                ip = mTelephonyManager.getDeviceId();
//            }
//
//            if (ip == null) {
//                ip = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//            }
//
//            Log.e("DEVICE IMEI", ip+"");
//
//            return ip;
//        }catch (Exception ex){
//            Log.e("EXCEPTION", ex.getMessage()+"");
//            return null;
//        }
//    }
//
//    /**
//    * This function create and displays the AlertDialog with the messages parsed
//    * via the @params, and also it sets the AlertDialog object back to null.
//    */
//    private void showDialog(String title, String msg){
//        try{
//
//            Handler handler = new Handler(Looper.getMainLooper());
//            handler.post(() -> {
//
//                if(alertDialog==null){
//                    alertDialog = new AlertDialog.Builder(SetUpActivity.this)
//                    .setTitle(title)
//                    .setMessage(msg)
//                    .setCancelable(false)
//                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
//                        dialog.dismiss();
//                        alertDialog = null;
//                    })
//                    .show();
//                }
//
//            });
//
//        }catch (Exception ex){
//        Log.e("EXCEPTION", ex.getMessage()+"");
//        }
//    }
//
//    private void completeVerifyCall(){
//        try {
//
//            if (phone_field.getText().toString().trim().isEmpty()) {
//                phone_field.setError("Cannot Be empty!!!");
//                return;
//            }
//
//                sivvarLoadingDialog.show();
//
//                String phone = phone_field.getText().toString();
//                phone = sterilizePhoneNumber(phone);
//
//                phone = selectedCountryCode.replace("+", "") + phone;
//                String uuID = getDeviceImei();
//
//                ApiCalls apiCalls = new ApiCalls("init", SetUpActivity.this, 30, 30, 40);
//                String finalPhone = phone;
//                apiCalls.initConcludeCallVerification(new VerifyPhoneRegisterParams(phone, uuID), new ResponseCallback() {
//                    @Override
//                    public String onSuccess(String res) {
//                        sivvarLoadingDialog.dismiss();
//                        Log.e("Success", res+"");
//
//                        JsonObject data = new JsonObject();
//                        data.addProperty("phone", finalPhone);
//                        data.addProperty("countryCode", selectedCountryCode.replace("+", ""));
//                        data.addProperty("Imei", uuID);
//                        data.addProperty("reponseData", ResponseUtil.Companion.getData(res));
//
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<JsonObject>(){}.getType();
//                        String d = gson.toJson(data, type);
//                        Utility.navigateWithParams(SetUpActivity.this, new CreateProfile(), "data", d);
//                        return null;
//                    }
//
//                    @Override
//                    public String onFailure(String res) {
//                        sivvarLoadingDialog.dismiss();
//
//                        showDialog("Error", "An error occurred, try again.");
//                        return null;
//                    }
//                });
//            } catch (Exception ex) {
//                sivvarLoadingDialog.dismiss();
//
//                showDialog("Error", ""+ex.getMessage());
//            }
//    }
//
//    private void beginListenCall(String caller) {
//        mProgress.setMessage("Awaiting phone call...");
//        mProgress.show();
//        mCaller = caller.replaceAll("\\D", "");
//        IntentFilter filter = new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
//        registerReceiver(callReceiver, filter);
//
//        mHandler.removeCallbacks(mCallTimeout);
//        mHandler.postDelayed(mCallTimeout, 20000);
//    }
//
//    private void stopListenCall() {
//        try {
//            unregisterReceiver(callReceiver);
//        } catch (Exception ig) {
//            Log.e("EXCEPTION", ig.getMessage()+"");
//        }
//    }
//
//    private String mCaller;
//    private ProgressDialog mProgress;
//
//    private Runnable mCallTimeout = new Runnable() {
//
//        @Override
//        public void run() {
//            stopListenCall();
//            mProgress.dismiss();
//
//            if(!retry){
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(SetUpActivity.this)
//                        .setCancelable(false)
//                        .setMessage("Waiting for call timed out")
//                        .setPositiveButton(R.string.retry, (dialog, which) -> {
//                            dialog.dismiss();
//                            requestPhoneCall();
//                        });
//
//                builder.show();
//
//            }else {
//                AlertDialog.Builder builder = new AlertDialog.Builder(SetUpActivity.this)
//                        .setCancelable(false)
//                        .setMessage("Choose Another Method ?")
//                        .setNegativeButton(R.string.sms, (dialog, which) ->{
//                            dialog.dismiss();
//                            requestSMS();
//                        })
//                        .setPositiveButton(R.string.call, ((dialog, which) -> {
//                            dialog.dismiss();
//                            requestPhoneCall();
//                        }));
//
//                builder.show();
//            }
//        }
//    };
//
////    private BroadcastReceiver callReceiver = new BroadcastReceiver() {
////
////    @Override
////    public void onReceive(Context context, Intent intent) { if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(intent.getAction())) {
////                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
////                String caller = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
////                caller = caller.replaceAll("\\D", "");
////
////                if (TelephonyManager.EXTRA_STATE_RINGING.equals(state) && caller.equals(mCaller)) {
////                    endCall();
////                    mHandler.removeCallbacks(mCallTimeout);
////                    mProgress.dismiss();
////                    unregisterReceiver(this);
////
////                    Log.e("GOT HERE", "Receiving call");
////                    completeVerifyCall();
////                }
////            }}};
//
////    private void endCall() {
////        Context context = getApplicationContext();
////        try {
////            Log.e("CALL", "Ending call ...");
////            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
////            Class<?> c = Class.forName(telephony.getClass().getName());
////
////            Method m = c.getDeclaredMethod("getITelephony");
////            m.setAccessible(true);
////
////            Object iTelephony = m.invoke(telephony);
////
////            Method m2 = iTelephony.getClass().getDeclaredMethod("silenceRinger");
////            Method m3 = iTelephony.getClass().getDeclaredMethod("endCall");
////
////            m2.invoke(iTelephony);
////            m3.invoke(iTelephony);
////        } catch (Exception e) {
////            Log.e("Exception", e+"while ending phonecall");
////        }
////    }
////
////    @Override
////    public void networkAvailable() {
////        if (connection_state != null)
////            connection_state.setVisibility(View.GONE);
////
////        i_agree.setActivated(true);
////    }
////
////    @Override
////    public void networkUnavailable() {
////        if (connection_state != null)
////            connection_state.setVisibility(View.VISIBLE);
////
////        i_agree.setActivated(false);
////    }
////
////    /** Some required permissions needs to be validated manually by the user
////     * Here we ask for record audio, camera, location and others to be able to make calls with sound
////     * and also confirm device local Once granted we don't have to ask them again, but if denied we can
////     */
////    private void getNecessaryPermissions() {
////        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
////                != PackageManager.PERMISSION_GRANTED
////                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
////                != PackageManager.PERMISSION_GRANTED){
////            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
////                    Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_LOCATION);
////        }
////    }
//
////    @Override
////    public void onRequestPermissionsResult(int requestCode,
////                                           String[] permissions, int[] grantResults) {
////        switch (requestCode) {
////            case PERMISSION_READ_STATE: {
////                if (grantResults.length > 0
////                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
////                    new AlertDialog.Builder(SetUpActivity.this)
////                            .setTitle("Permission Needed")
////                            .setMessage("Sivvar Needs to read your phone state to proceed.")
////                            .setCancelable(false)
////                            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
////                                dialog.dismiss();
////                                getNecessaryPermissions();
////                            })
////                            .show();
////                }
////                break;
////            }
////
////            case PERMISSION_READ_CALL_LOG:{
////                if (grantResults.length > 0
////                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
////                    new AlertDialog.Builder(SetUpActivity.this)
////                            .setTitle("Permission Needed")
////                            .setMessage("Sivvar Needs to read your call log to proceed with call verification.")
////                            .setCancelable(false)
////                            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
////                                dialog.dismiss();
////                                getNecessaryPermissions();
////                            })
////                            .show();
////                }
////
////                break;
////            }
////
////            case PERMISSION_AUDIO:{
////                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                } else {
////                    new AlertDialog.Builder(SetUpActivity.this)
////                            .setTitle("Permission Needed")
////                            .setMessage("Sivvar Needs your audio access to proceed.")
////                            .setCancelable(false)
////                            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
////                                dialog.dismiss();
////                                getNecessaryPermissions();
////                            })
////                            .show();
////                }
////                break;
////            }
////
////            case PERMISSION_CAMERA:{
////                if (grantResults.length > 0
////                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                } else {
////                    new AlertDialog.Builder(SetUpActivity.this)
////                            .setTitle("Permission Needed")
////                            .setMessage("Sivvar Needs your camera access for video calls to proceed.")
////                            .setCancelable(false)
////                            .setPositiveButton(android.R.string.ok, (dialog, which) -> {
////                                dialog.dismiss();
////                                getNecessaryPermissions();
////                            })
////                            .show();
////                }
////                break;
////            }
////
////            case PERMISSION_LOCATION:
////                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
////                    builder = new AlertDialog.Builder(SetUpActivity.this);
////                    builder.setTitle("Permission Needed");
////                    builder.setMessage("Sorry, but SIVVAR needs your location permission to continue");
////                    builder.setCancelable(false);
////                    builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
////                        dialog.cancel();
////                        getNecessaryPermissions();
////                    });
////
////                    builder.show();
////                }
////                break;
////        }
////    }
//
////    public void statusCheck() {
////        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
////
////        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
////            buildAlertMessageNoGps();
////        }else {
////            getNecessaryPermissions();
////        }
////    }
////
////    private void buildAlertMessageNoGps() {
////            builder = new AlertDialog.Builder(this);
////            builder.setMessage("Your GPS seems to be disabled, SIVVAR need it to proceed, activate ?")
////                    .setCancelable(false)
////
////                    .setPositiveButton("Yes", (dialog, id) ->{
////                        dialog.cancel();
////                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
////                    })
////
////                    .setNegativeButton("No", (dialog, id) ->{
////                        dialog.cancel();
////                        statusCheck();
////                    });
////
////            builder.show();
////    }
//
//    @Override
//    public void onResume(){
//        super.onResume();
////        statusCheck();
//    }
//}