package com.example.sivvar.activities;

import android.app.AlertDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.User;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.models.UserModel;
import com.example.sivvar.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CreateProfile extends AppCompatActivity {

    private EditText firstname_field, lastname_field, email_field;
    private Button setup;
    private SivvarLoadingDialog sivvarLoadingDialog;

//    private String countryCode, phone, imei, responseData;
    private JsonObject parseData = new JsonObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        Intent intent = getIntent();

        if(intent != null){
            Gson gson = new Gson();
            Type type = new TypeToken<JsonObject>(){}.getType();
            parseData = gson.fromJson(intent.getStringExtra("data"), type);

            Log.e("SUKURA", ""+parseData);
        }

        setcontects();
        findViewByIds();
        setOnclickListeners();
    }

    private void setOnclickListeners() {
        setup.setOnClickListener(v -> {
            try{
                if(notFieldisEmpty()){
                    sivvarLoadingDialog.show();

                        UserModel userModel = new UserModel();
                        userModel.setCountryCode(parseData.get("countryCode").getAsString());
                        userModel.setEmailAddress(email_field.getText().toString().trim());
                        userModel.setFirstName(firstname_field.getText().toString().trim());
                        userModel.setImei(parseData.get("Imei").getAsString());
                        userModel.setLastName(lastname_field.getText().toString().trim());
                        userModel.setPhoneNumber(parseData.get("phone").getAsString());
                        userModel.setResponseData(parseData.get("responseData").getAsString());

                        Gson gson = new Gson();
                        Type type = new TypeToken<UserModel>(){}.getType();

                        String s = gson.toJson(userModel, type);
                        User.storeUser(s, CreateProfile.this);

                        sivvarLoadingDialog.dismiss();

                        Utility.navigateWithParams(CreateProfile.this, new HurrayActivity(), "title", userModel.getFirstName());
                        finish();
                }
            }catch (Exception ex){
                sivvarLoadingDialog.dismiss();

                Log.e("Exception", ex.getMessage()+"");
                showDialog("Error", "Something went wrong, try again.'");
            }
        });
    }

    private boolean notFieldisEmpty() {
        if(firstname_field.getText().toString().isEmpty()){
            firstname_field.setError("Cannot be Empty");
            return false;
        }

        if(lastname_field.getText().toString().isEmpty()){
            lastname_field.setError("Cannot be Empty");
            return false;
        }

        if(email_field.getText().toString().isEmpty()){
            email_field.setError("Cannot be Empty");
            return false;
        }

        return true;
    }

    private void setcontects() {
        sivvarLoadingDialog = new SivvarLoadingDialog(this);
    }

    private void findViewByIds() {
        firstname_field = findViewById(R.id.firstname_field);
        lastname_field = findViewById(R.id.lastname_field);
        email_field = findViewById(R.id.email_field);

        setup = findViewById(R.id.setup);
    }


    /*
     * This function create and displays the AlertDialog with the messages parsed
     * and title.s
     * */
    private void showDialog(String title, String msg){
        try{
                new AlertDialog.Builder(CreateProfile.this)
                    .setTitle(title)
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();
        }catch (Exception ex){
            Log.e("EXCEPTION", ex.getMessage()+"");
        }
    }

}