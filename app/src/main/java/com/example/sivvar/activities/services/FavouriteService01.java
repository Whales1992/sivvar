package com.example.sivvar.activities.services;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.CallingDialog;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.adapters.CallHistoryAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.CallhistoryRender;
import com.example.sivvar.models.CallHistory;
import com.example.sivvar.models.MyFavourites;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class FavouriteService01 extends AppCompatActivity implements CallhistoryRender{

    private MyFavourites favourites;
    private TextView serviceTitle, serviceCat, serviceDetails;
    private Button callbtn;
    private ImageView serLogo;
    private ConstraintLayout parent;

    private List<CallHistory> callHisotry = new ArrayList<>();
    private RecyclerView history;
    private CallHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_service);

        try{
            if(getIntent().hasExtra("service")){
                String s = getIntent().getStringExtra("service");
                favourites = MyFavourites.objectFromData(s);
            }
        }catch (Exception ex){
            Log.e("Exception-11", ""+ex.getMessage());
        }

        Utility.initToolbar(this, true, favourites.getService_name(), true);

        findViewByIds();
        setContents();

        initHistory();
    }

    private void initHistory() {
        callHisotry = CallHistory.getCallHistoryByServiceId(favourites.getService_id());

        adapter = new CallHistoryAdapter(callHisotry, this);
        history.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        history.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    private void findViewByIds(){
        serviceTitle = findViewById(R.id.serviceTitle);
        serviceCat = findViewById(R.id.serviceCat);
        serviceDetails = findViewById(R.id.serviceDetails);
        callbtn = findViewById(R.id.callbtn);
        serLogo = findViewById(R.id.serLogo);
        parent = findViewById(R.id.parent);
        history = findViewById(R.id.history);
    }

    private void setContents() {
        String serName = ""+favourites.getService_name();
        serviceTitle.setText(serName);

        String serCatName = ""+favourites.getCategory_name();
        serviceCat.setText(serCatName);

        String serDecs = favourites.getService_description().equalsIgnoreCase("none")
                ? "No Description" : favourites.getService_description();

        serviceDetails.setText(serDecs);

        String imgLink = ""+favourites.getService_logo();

        Glide
            .with(this)
            .load(imgLink)
            .centerCrop().addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Glide.with(FavouriteService01.this).load(R.drawable.swiden).into(serLogo);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        })
                .into(serLogo);

        if(favourites.getFavourite_status().equals(SivvarKey.ACTIVE.getValue())){

            if(MainActivity.proxyState.equals(SivvarKey.CONNECTED.getValue())){
                callbtn.setBackground(getResources().getDrawable(R.drawable.curve_border_green));
                callbtn.setTextColor(getResources().getColor(R.color.white));

                callbtn.setOnClickListener(v ->new CallingDialog(this, null, favourites, null, parent.getWidth(), parent.getHeight()));
            }else if(MainActivity.proxyState.equals(SivvarKey.FAILED.getValue())){
                callbtn.setBackground(getResources().getDrawable(R.drawable.curve_border_red));
                callbtn.setTextColor(getResources().getColor(R.color.white));
            }else {
                callbtn.setBackground(getResources().getDrawable(R.drawable.curve_border_orange));
                callbtn.setTextColor(getResources().getColor(R.color.white));
            }

        }else {
            callbtn.setBackground(getResources().getDrawable(R.drawable.curve_border_red));
            callbtn.setTextColor(getResources().getColor(R.color.white));
        }
    }

    @Override
    public void renderCallHistory(@NotNull CallHistory callHistory, @NotNull TextView dateTag, @NotNull TextView speakTime, @NotNull TextView timeStamp) {
        if(Utility.isToday(callHistory.date))
            dateTag.setText("Today");
        else if(Utility.isYesterday(callHistory.date))
            dateTag.setText("Yesterday");
        else
            dateTag.setText(Utility.dateString(callHistory.date));

        speakTime.setText(callHistory.speakTime);

        timeStamp.setText(Utility.timeString(callHistory.date));
    }
}
