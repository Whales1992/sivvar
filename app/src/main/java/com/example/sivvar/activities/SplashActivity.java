package com.example.sivvar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.LinphoneService;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.helpers.User;
import com.example.sivvar.landing.LandingActivity;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
    private Handler mHandler;
    private TextView service_status;
    private ArrayList<String> permissionsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        service_status = findViewById(R.id.service_status);

        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }

        mHandler = new Handler();
        try{
        if(User.isLoggedIn(this)){
            // Check whether the Service is already running
            if (LinphoneService.isReady()) {
                onServiceReady();
            } else {
                service_status.setText(getResources().getString(R.string.service_startup));
                // If it's not, let's start it
                startService(new Intent().setClass(this, LinphoneService.class));
                // And wait for it to be ready, so we can safely use it afterwards
                new ServiceWaitThread().start();
            }
        } else{
            Intent i = new Intent(this, LandingActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
            finish();
        }
            } catch (RuntimeException ex){
            ex.printStackTrace();
        } catch (Exception ex){
                ex.printStackTrace();
        }
    }

    private void onServiceReady() {
        // Once the service is ready, we can move on in the application
        // We'll forward the intent action, type and extras so it can be handled
        // by the next activity if needed, it's not the launcher job to do that

        service_status.setText(getResources().getString(R.string.service_isready));

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        finish();
    }

    // This thread will periodically check if the Service is ready, and then call onServiceReady
    private class ServiceWaitThread extends Thread {
        public void run() {
            while (!LinphoneService.isReady()) {
                try {
                    runOnUiThread(() -> service_status.setText(getResources().getString(R.string.service_ready)));

                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException("waiting thread sleep() has been interrupted");
                }
            }
            // As we're in a thread, we can't do UI stuff in it, must post a runnable in UI thread
            mHandler.post(SplashActivity.this::onServiceReady);
        }
    }
}
