package com.example.sivvar.activities.services;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.adapters.FavouriteSubServicesAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.RenderSubService;
import com.example.sivvar.models.SubServices;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.favouritesSubServices.favouritesStepDown.FavouriteSubServicesRepositoryObserversDown;
import com.example.sivvar.observables.favouritesSubServices.favouritesStepDown.FavouritesSubServicesSubServicesObserversDown;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.example.sivvar.helpers.DtosKt.generateStampSubServices;

public class FavSubServicesStepDown extends AppCompatActivity implements FavouriteSubServicesRepositoryObserversDown, RenderSubService, SwipeRefreshLayout.OnRefreshListener {
    private SubServices subServices;
    private List<SubServices> subServicesList = new ArrayList<>();
    private RecyclerView recyclerView;

    private SwipeRefreshLayout pullRefresh;
    private TextView emptyList;

    private SivvarLoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_services);
        FavouritesSubServicesSubServicesObserversDown.getInstance().registerObserver(this);
        loadingDialog = new SivvarLoadingDialog(this);

        findViewByIds();

        try{
            if(getIntent().hasExtra("subServices")){
                String s = getIntent().getStringExtra("subServices");
                subServices = new Gson().fromJson(s, new TypeToken<SubServices>(){}.getType());
                Utility.initToolbar(this, true, subServices.getService_name(), false);
            }
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }

        Utility.initToolbar(this, true, subServices.getService_name(), false);
//
//        if(subServices.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
//            List<SubServices> subServicesList = SubServices.getSubservicesList(subServices.getId());
//            if(subServicesList.isEmpty()){
        getSubServices(subServices.getService_id(), subServices.getId());
//            }else{
//                SubServices.getSubservicesDown(subServices.getId());
//            }
//        }
    }

    private void findViewByIds(){
        recyclerView = findViewById(R.id.recyclerView);
        pullRefresh = findViewById(R.id.pullRefresh);
        emptyList = findViewById(R.id.emptyList);

        pullRefresh = findViewById(R.id.pullRefresh);
        pullRefresh.setOnRefreshListener(this);
    }

    private void initList(){
        try{
            FavouriteSubServicesAdapter adapter = new FavouriteSubServicesAdapter(subServicesList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void populateList(List<SubServices> subServicesList) {
        this.subServicesList = subServicesList;

        Handler handler = new Handler(getMainLooper());

        handler.post(this::initList);

        handler.post(() -> {
            if(subServicesList == null || subServicesList.isEmpty()){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void renderSubService(@NotNull SubServices subServices, @NotNull ImageView serviceLogo, @NotNull TextView serviceName, @NotNull TextView catTitle, @NotNull LinearLayout parent) {
        String logoLink = ""+subServices.getService_logo();

        Glide
            .with(this)
            .load(logoLink)
            .centerCrop().addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Handler handler = new Handler(getMainLooper());
                handler.post(() -> Glide.with(FavSubServicesStepDown.this).load(R.drawable.swiden).into(serviceLogo));
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        })
            .into(serviceLogo);

        serviceName.setText(subServices.getService_name());
        catTitle.setText(subServices.getCategory_name());

        parent.setOnClickListener(v -> {
            if(subServices.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
                String s = new Gson().toJson(subServices, new TypeToken<SubServices>(){}.getType());
                Utility.navigateWithParams(this, new FavSubServicesStepDown(), "subServices", s);
            }else{
                String s = new Gson().toJson(subServices, new TypeToken<SubServices>(){}.getType());
                Utility.navigateWithParams(this, new FavouriteService02(), "service", s);
            }
    });
    }

    private void getSubServices(int servicesID, Long favouriteID){
        try{
            loadingDialog.show();

            JsonObject param = new JsonObject();
            param.addProperty("msISDN", MainActivity.me.getPhoneNumber());
            param.addProperty("Identity", MainActivity.me.getImei());
            param.addProperty("serviceID", servicesID);
            param.addProperty("sivvarStamp", generateStampSubServices(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber(), String.valueOf(servicesID)));

            ApiCalls apiCalls = new ApiCalls("services", this, 60, 60, 60);
            apiCalls.getSubServices(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    loadingDialog.cancel();
                    pullRefresh.setRefreshing(false);

                    if (ResponseUtil.Companion.isSuccessfullV3(res)) {
                        try {
                            String s = ResponseUtil.Companion.getResponseDataV3(res);
                            Log.e("RESPONSE", ""+s);

                            List<SubServices> subServicesList = SubServices.arraySubServicesFromData(s);
                            List<SubServices> parsableList = new ArrayList<>();

                            //We need to delete previous data
                            List<SubServices> subServicesListLocal = SubServices.getSubservicesList(favouriteID);
                            for(SubServices subServices1 : subServicesListLocal){
                                subServices1.delete();
                            }

                            for(SubServices subServices: subServicesList){
                                SubServices subServices1 = new SubServices(favouriteID, subServices.getService_dial_code(), subServices.getService_name(),
                                        subServices.getHas_sub_service(), subServices.getIs_audio_service(), subServices.getService_status(), subServices.getService_logo(),
                                        subServices.getService_id(), subServices.getIs_video_service(), subServices.getService_description(), subServices.getCategory_name());
                                subServices1.save();
                                parsableList.add(subServices1);
                            }

                            FavouritesSubServicesSubServicesObserversDown.getInstance().populateList(parsableList);
                        } catch (Exception ex) {
                            Log.e("Exception", "" + ex.getMessage());
                            showAlertDialog(ex.getMessage());
                        }
                    }else{
                        loadingDialog.cancel();

                        showAlertDialog(ResponseUtil.Companion.getMessage(res));
                    }

                    return null;
                }

                @Override
                public String onFailure(String res) {
                    pullRefresh.setRefreshing(false);

                    loadingDialog.cancel();
                    showAlertDialog("Something went wrong, please confirm your internet connection status, pull this page to refresh or try again later.");
                    return null;
                }
            });
        }catch (Exception ex){
            pullRefresh.setRefreshing(false);
            loadingDialog.cancel();

            Log.e("Exception", ""+ex.getMessage());
        }
    }

    /**
     * Simply show a dialog and takes the arguments that fit it purpose of showing.
     */
    private void showAlertDialog(String msg){
        try{
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Service not available")
                    .setNegativeButton("Dismiss", (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    })
                    .show();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onRefresh() {
        try{
            getSubServices(subServices.getService_id(), subServices.getId());
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }
}