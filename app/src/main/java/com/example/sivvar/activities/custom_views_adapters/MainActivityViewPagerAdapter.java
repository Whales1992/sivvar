package com.example.sivvar.activities.custom_views_adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.sivvar.fragments.CallHistoryFragment;
import com.example.sivvar.fragments.CategoryFragment;
import com.example.sivvar.fragments.HomeFragment;
import com.example.sivvar.fragments.SettingsFragment;

/**
 * Created by Wale on 6/04/19.
 */

public class MainActivityViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private final Bundle fragmentBundle;

    public MainActivityViewPagerAdapter(FragmentManager fm, int NumOfTabs, Bundle bundle) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        fragmentBundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                final HomeFragment b = new HomeFragment();
                b.setArguments(this.fragmentBundle);
                return b;

            case 1:
                final CategoryFragment a = new CategoryFragment();
                a.setArguments(this.fragmentBundle);
                return a;


            case 2:
                final CallHistoryFragment d = new CallHistoryFragment();
                d.setArguments(this.fragmentBundle);
                return d;

            case 3:
                final SettingsFragment c = new SettingsFragment();
                c.setArguments(this.fragmentBundle);
                return c;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
