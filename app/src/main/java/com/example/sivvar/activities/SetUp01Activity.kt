package com.example.sivvar.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sivvar.R
import com.example.sivvar.helpers.Utility
import kotlinx.android.synthetic.main.activity_set_up.*

class SetUp01Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_up)

        continueBtn.setOnClickListener {
            Utility.navigate(this, OtpActivityV1())
        }
    }
}
