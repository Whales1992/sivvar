package com.example.sivvar.activities.services;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sivvar.R;
import com.example.sivvar.interfaces.renders.RenderCategorySubService;
import com.example.sivvar.models.Categories;
import com.example.sivvar.models.CategoryServices;

import org.jetbrains.annotations.NotNull;

public class ServicesCat extends AppCompatActivity implements RenderCategorySubService {

    private TextView title, unavail;
    private RecyclerView list;

    private Categories categories;

    String titleText;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        setContext();
        findViewByIds();

        setContents();
    }

    private void setContext() {
        context = this;
    }

    private void setContents() {
        try{
            title.setText(titleText);
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    private void findViewByIds() {
        title = findViewById(R.id.title);
        list = findViewById(R.id.list);
        unavail = findViewById(R.id.unavail);
    }

//    @Override
//    public void renderCategory(@NotNull CategoryServices categoryServices, @NotNull ImageView serviceLogo, @NotNull TextView serviceName, @NotNull TextView catTitle, @NotNull LinearLayout parent) {
//        try{
//            serviceName.setText(categoryServices.getService_name());
//            catTitle.setText(categoryServices.getCategory_name());
//
//            String logo = categoryServices.getService_logo();
//
//            Glide
//                    .with(context)
//                    .load(logo)
//                    .centerCrop().addListener(new RequestListener<Drawable>() {
//                @Override
//                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                    Handler handler = new Handler(getMainLooper());
//                    handler.post(() -> Glide.with(context).load(R.drawable.swiden).into(serviceLogo));
//
//                    return false;
//                }
//
//                @Override
//                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                    return false;
//                }
//            })
//                    .into(serviceLogo);
//
//            parent.setOnClickListener(v -> {
//
//                if(categoryServices.has_sub_service.equals(SivvarKey.TRUE.getValue())){
//                    //TODO:: ... Take to level down
//                }else{
//                    String s = new Gson().toJson(categoryServices, new TypeToken<CategoryServices>(){}.getType());
//                    Utility.navigateWithParams(this, new ServiceCat(), "category", s);
//                }
//            });
//        }catch (Exception ex){
//            Log.e("Exception", ""+ex.getMessage());
//        }
//    }

    @Override
    public void renderCategorySubServices(@NotNull CategoryServices subServices, @NotNull ImageView serviceLogo, @NotNull TextView serviceName, @NotNull TextView catTitle, @NotNull LinearLayout parent) {

    }
}
