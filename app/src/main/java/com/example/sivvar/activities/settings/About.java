package com.example.sivvar.activities.settings;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.sivvar.R;
import com.example.sivvar.fragments.aboutus.PrivacyPolicy;
import com.example.sivvar.fragments.aboutus.TermsOfServices;
import com.example.sivvar.helpers.Utility;

public class About extends AppCompatActivity {

    private static String appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getAppVerion(this);
        Utility.initToolbar(this, true, "About", false);

        TextView privacy = findViewById(R.id.privacy);
        privacy.setOnClickListener(v -> Utility.navigate(About.this, new PrivacyPolicy())
        );

        TextView terms = findViewById(R.id.terms);
        terms.setOnClickListener(v -> Utility.navigate(About.this, new TermsOfServices()));

        TextView versionInfo = findViewById(R.id.versionInfo);
        versionInfo.setText(appVersion);
    }

    private void getAppVerion(Context context){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}