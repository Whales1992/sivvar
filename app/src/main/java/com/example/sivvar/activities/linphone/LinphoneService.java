package com.example.sivvar.activities.linphone;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import com.example.sivvar.R;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.User;
import com.example.sivvar.models.UserModel;

import org.linphone.core.AccountCreator;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.Factory;
import org.linphone.core.LogCollectionState;
import org.linphone.core.ProxyConfig;
import org.linphone.core.RegistrationState;
import org.linphone.core.TransportType;
import org.linphone.core.tools.Log;
import org.linphone.mediastream.Version;

/**
 * This is to initialize linphone service and it's the very first class to call else our
 * sip server will not be initialize, hence, will not be available to make any data call.
 * @author Belledon, and Beautifuly Modified by @Kolawole Adewale
 * @version 1.1
 */
public class LinphoneService extends Service {
//    public static final String START_LINPHONE_LOGS = " ==== Device information dump ====";
    public static LinphoneService sInstance = null;

    public Handler mHandler;
    public Timer mTimer;

    public static Core mCore;

    public static UserModel me;

    private CoreListenerStub mCoreListener;

    /** interface for clients that bind */
    public IBinder mBinder;

    /** indicates whether onRebind should be used */
    boolean mAllowRebind;

    public static boolean isReady() {
        return sInstance != null;
    }

    public static LinphoneService getInstance() {
        return sInstance;
    }

    public static Core getCore() {
        try{
            if(sInstance!=null && mCore!=null)
                return mCore;
            else{
                sInstance = new LinphoneService();
                return mCore;
            }
        }catch (Exception ex){
            Log.e("WALE EXE6", "CAUGHT => "+ex.getMessage());
        }

        sInstance = new LinphoneService();
        return mCore;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {
        Log.e("REBIND", "Rebinding to service");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try{
            me = User.getUserModel(this);

            // The first call to liblinphone SDK MUST BE to a Factory method
            // So let's enable the library debug logs & log collection
            String basePath = getFilesDir().getAbsolutePath();
            Factory.instance().setLogCollectionPath(basePath);
            Factory.instance().enableLogCollection(LogCollectionState.Enabled);
            Factory.instance().setDebugMode(true, getString(R.string.app_name));

            // Dump some useful information about the device we're running on
//            Log.e(START_LINPHONE_LOGS);
            dumpDeviceInformation();
            dumpInstalledLinphoneInformation();

            mCoreListener = new CoreListenerStub() {
                @Override
                public void onRegistrationStateChanged(Core lc, ProxyConfig cfg, RegistrationState cstate, String message){
                    MainActivity.getInstance().updateLed(cstate);
                }
            };

            mHandler = new Handler();
            try{
                final Thread mThread = new Thread() {
                    @Override
                    public void run() {
                        startCore();
                    }
                };
                mThread.start();
            }catch (Exception ex){
                Log.e("M-CORE EXCEPTION", "Something went wrong here ... "+ ex.getMessage()+"   OR   "+ex.getMessage());
            }

        }catch (Exception ex){
            Log.e("WALE EXE5", "CAUGHT => "+ex.getMessage());
        }
    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try{
            sInstance = this;

            if(mCore==null)
                startCore();

            if(mCoreListener==null)
                setmCoreListener();

            // We also MUST call the iterate() method of the Core on a regular basis
            TimerTask lTask =
                    new TimerTask() {
                        @Override
                        public void run() {
                            mHandler.post(
                                    () -> {
                                        if (mCore != null) {
                                            mCore.iterate();
                                        }
                                    });
                        }
                    };

            mTimer = new Timer("Linphone scheduler");
            mTimer.schedule(lTask, 0, 20);
        }catch (Exception ex){
            Log.e("WALE EXE4", "CAUGHT => "+ex.getMessage());
        }

        return START_STICKY;
    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if(mCore!=null && mTimer!=null){
                mTimer.cancel();
                mCore.stop();
                mCore = null;
                sInstance = null;
            }
        }catch (Exception ex){
            Log.e("WALE EXE3", "CAUGHT => "+ex.getMessage());
        }
    }

    /** For this sample we will kill the Service at the same time we kill the app */
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    /** We will create a directory for user signed certificates if needed */
    private void setCert() {
        try{
            String basePath = getFilesDir().getAbsolutePath();
            String userCerts = basePath + "/user-certs";
            File f = new File(userCerts);
            if (!f.exists()) {
                if (!f.mkdir()) {
                    Log.e(userCerts + " can't be created.");
                }
            }
            mCore.setUserCertificatesPath(userCerts);
        }catch (Exception ex){
            Log.e("WALE EXE2", "CAUGHT => "+ex.getMessage());
        }
    }

    private void dumpDeviceInformation() {
        StringBuilder sb = new StringBuilder();
        sb.append("DEVICE=").append(Build.DEVICE).append("\n");
        sb.append("MODEL=").append(Build.MODEL).append("\n");
        sb.append("MANUFACTURER=").append(Build.MANUFACTURER).append("\n");
        sb.append("SDK=").append(Build.VERSION.SDK_INT).append("\n");
        sb.append("Supported ABIs=");
        for (String abi : Version.getCpuAbis()) {
            sb.append(abi).append(", ");
        }
        sb.append("\n");
        Log.e(sb.toString());
    }

    private void dumpInstalledLinphoneInformation() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException nnfe) {
            Log.e(nnfe);
        }

        if (info != null) {
            Log.e(
                    "[Service] Linphone version is ",
                    info.versionName + " (" + info.versionCode + ")");
        } else {
            Log.e("[Service] Linphone version is unknown");
        }
    }

    private void copyFromPackage(int resourceId, String target) throws IOException {
        FileOutputStream lOutputStream = openFileOutput(target, 0);
        InputStream lInputStream = getResources().openRawResource(resourceId);
        int readByte;
        byte[] buff = new byte[8048];
        while ((readByte = lInputStream.read(buff)) != -1) {
            lOutputStream.write(buff, 0, readByte);
        }
        lOutputStream.flush();
        lOutputStream.close();
        lInputStream.close();
    }

    /**Here we create the CoreListenere and parse the states to a static method in MainActivity,
     *  @see MainActivity for the way it's been used.*/
    private void setmCoreListener(){
        mCoreListener = new CoreListenerStub() {
            @Override
            public void onRegistrationStateChanged(Core lc, ProxyConfig cfg, RegistrationState cstate, String message){
                MainActivity.getInstance().updateLed(cstate);
            }
        };
    }

    /**This simply start the core if it has not been started, and call the registration method along*/
    private void startCore(){
        try{
            String basePath = getFilesDir().getAbsolutePath();
            mCore = Factory.instance().createCore(basePath + "/.linphonerc", basePath + "/linphonerc", getApplicationContext());
            mCore.addListener(mCoreListener);
            mCore.setUserAgent("SIVVARAPP-android-2AV2", null);
            mCore.setHttpProxyPort(50070);
            mCore.enableAdaptiveRateControl(true);

            mCore.enableMic(true);
            mCore.setMaxCalls(1);
            mCore.setExpectedBandwidth(Integer.valueOf(SivvarKey.AUDIO_QUALITY_G729.getValue()));

            setCert(); //Core is ready to be configured
            mCore.start();

            registerAccount();
        }catch (Exception ex){
            Log.e("WALE EXE0", "CAUGHT => "+ex.getMessage());
        }
    }

    /**Here we begin the registration after the core has been successfully initialized and ready for use*/
    private void registerAccount(){
        try{
            Handler handler = new Handler(getMainLooper());
            handler.post(() -> {
                if(mCore!=null){
                    AccountCreator mAccountCreator = mCore.createAccountCreator(null);
                    mAccountCreator.setUsername(me.getPhoneNumber());
                    mAccountCreator.setDomain(SivvarKey.DOMAIN.getValue());
                    mAccountCreator.setPassword(me.getResponseData());
                    mAccountCreator.setTransport(TransportType.Udp);

                    // This will automatically create the proxy config and auth info and add them to the Core
                    ProxyConfig cfg = mAccountCreator.createProxyConfig();

                    cfg.enableQualityReporting(false);
                    cfg.done();

                    mAccountCreator.setProxyConfig(cfg);
                    mCore.setDefaultProxyConfig(cfg);
                }
            });
        }catch (Exception ex){
            Log.e("WALE EXE1", "CAUGHT => "+ex.getMessage());
        }
    }
}