package com.example.sivvar.activities.services;

import android.app.AlertDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.sivvar.R;
import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.adapters.CategorySubServicesAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.interfaces.renders.RenderCategorySubService;
import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.categoriesSubServices.categoriesStepDown.CategorySubServiceObserversDown;
import com.example.sivvar.observables.categoriesSubServices.categoriesStepDown.CategorySubServiceRepositoryObserversDown;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.example.sivvar.helpers.DtosKt.generateStampSubServices;

public class CatSubServicesStepDown extends AppCompatActivity implements CategorySubServiceRepositoryObserversDown, RenderCategorySubService, SwipeRefreshLayout.OnRefreshListener {
    private CategoryServices subServices;
    private List<CategoryServices> subServicesList = new ArrayList<>();
    private RecyclerView recyclerView;

    private SwipeRefreshLayout pullRefresh;
    private TextView emptyList;

    private SivvarLoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_services);
        CategorySubServiceObserversDown.getInstance().registerObserver(this);
        loadingDialog = new SivvarLoadingDialog(this);

        findViewByIds();

        try{
            if(getIntent().hasExtra("subServices")){
                String s = getIntent().getStringExtra("subServices");
                subServices = new Gson().fromJson(s, new TypeToken<CategoryServices>(){}.getType());
                Utility.initToolbar(this, true, subServices.getService_name(), true);
            }
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }

        Utility.initToolbar(this, true, subServices.getService_name(), false);

//        if(subServices.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
//            List<CategoryServices> subServicesList = CategoryServices.getCategoryServicesById(String.valueOf(subServices.category_id));
//            if(subServicesList.isEmpty()){
                getSubServices(Integer.parseInt(subServices.getService_id()), subServices.category_id);
//            }else{
//                CategoryServices.getSubservicesDown(subServices.getService_id());
//            }
//        }
    }

    private void findViewByIds(){
        recyclerView = findViewById(R.id.recyclerView);
        pullRefresh = findViewById(R.id.pullRefresh);
        emptyList = findViewById(R.id.emptyList);

        pullRefresh = findViewById(R.id.pullRefresh);
        pullRefresh.setOnRefreshListener(this);
    }

    private void initList(){
        try{
            CategorySubServicesAdapter adapter = new CategorySubServicesAdapter(subServicesList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void populateList(List<CategoryServices> categoryServices) {
        this.subServicesList = categoryServices;

        Handler handler = new Handler(getMainLooper());

        handler.post(this::initList);

        handler.post(() -> {
            if(categoryServices == null || categoryServices.isEmpty()){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getSubServices(int servicesID, String categoryId){
        try{
            loadingDialog.show();

            JsonObject param = new JsonObject();
            param.addProperty("msISDN", MainActivity.me.getPhoneNumber());
            param.addProperty("Identity", MainActivity.me.getImei());
            param.addProperty("serviceID", servicesID);
            param.addProperty("sivvarStamp", generateStampSubServices(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber(), String.valueOf(servicesID)));

            ApiCalls apiCalls = new ApiCalls("services", this, 60, 60, 60);
            apiCalls.getSubServices(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    loadingDialog.cancel();
                    pullRefresh.setRefreshing(false);

                    if (ResponseUtil.Companion.isSuccessfullV3(res)) {
                        try {
                            String s = ResponseUtil.Companion.getResponseDataV3(res);
                            Log.e("RESPONSE", ""+s);

                            List<CategoryServices> categoryServicesList = CategoryServices.arrayCategoryServicesFromData(s);
                            List<CategoryServices> parsableList = new ArrayList<>();

                            //We need to delete previous data
                            List<CategoryServices> catSubServicesListLocal = CategoryServices.getCategoryServicesById(categoryId);
                            for(CategoryServices categoryServices : catSubServicesListLocal){
                                categoryServices.delete();
                            }
//
                            for(CategoryServices categoryServices: categoryServicesList){
                                CategoryServices subServices1 = new CategoryServices(categoryServices.getService_dial_code(), categoryId, categoryServices.getService_name(),
                                        categoryServices.getHas_sub_service(), categoryServices.getIs_audio_service(), categoryServices.getService_status(), categoryServices.getService_logo(),
                                        categoryServices.getService_id(), categoryServices.getIs_video_service(), categoryServices.getService_description(), categoryServices.getCategory_name());
//                                subServices1.save();
                                parsableList.add(subServices1);
                            }

                            CategorySubServiceObserversDown.getInstance().populateList(parsableList);
                        } catch (Exception ex) {
                            Log.e("Exception", "" + ex.getMessage());
                            showAlertDialog(ex.getMessage());
                        }
                    }else{
                        loadingDialog.cancel();

                        showAlertDialog(ResponseUtil.Companion.getMessage(res));
                    }

                    return null;
                }

                @Override
                public String onFailure(String res) {
                    pullRefresh.setRefreshing(false);

                    loadingDialog.cancel();
                    showAlertDialog("Something went wrong, please confirm your internet connection status, pull this page to refresh or try again later.");
                    return null;
                }
            });
        }catch (Exception ex){
            pullRefresh.setRefreshing(false);
            loadingDialog.cancel();

            Log.e("Exception", ""+ex.getMessage());
        }
    }

    /**
     * Simply show a dialog and takes the arguments that fit it purpose of showing.
     */
    private void showAlertDialog(String msg){
        try{
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("No service available")
                    .setNegativeButton("Dismiss", (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    })
                    .show();
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onRefresh() {
        try{
            getSubServices(Integer.parseInt(subServices.getService_id()), subServices.category_id);
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void renderCategorySubServices(@NotNull CategoryServices subServices, @NotNull ImageView serviceLogo, @NotNull TextView serviceName, @NotNull TextView catTitle, @NotNull LinearLayout parent) {
        String logoLink = ""+subServices.getService_logo();

        Glide
                .with(this)
                .load(logoLink)
                .centerCrop().addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                Handler handler = new Handler(getMainLooper());
                handler.post(() -> Glide.with(CatSubServicesStepDown.this).load(R.drawable.swiden).into(serviceLogo));
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        })
                .into(serviceLogo);

        serviceName.setText(subServices.getService_name());
        catTitle.setText(subServices.getCategory_name());

        parent.setOnClickListener(v -> {
            if(subServices.getHas_sub_service().equals(SivvarKey.TRUE.getValue())){
                String s = new Gson().toJson(subServices, new TypeToken<CategoryServices>(){}.getType());
                Utility.navigateWithParams(this, new CatSubServicesStepDown(), "subServices", s);
            }else{
                String s = new Gson().toJson(subServices, new TypeToken<CategoryServices>(){}.getType());
                Utility.navigateWithParams(this, new CategoryService02(), "service", s);
            }
        });
    }
}