package com.example.sivvar.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.example.sivvar.R
import kotlinx.android.synthetic.main.activity_create_pin.*

class CreatePinActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pin)

        onTextChangeListener(one, two)
        onTextChangeListener(two, three)
        onTextChangeListener(three, four)
        onTextChangeListener(four, five)
        onTextChangeListener(five, six)
    }
    
    private fun onTextChangeListener(view: EditText, next: EditText)
    {
        view.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(p0!!.isNotEmpty())
                {
                    changeFocus(next)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
    }


    private fun changeFocus(view: EditText){
        view.isFocusableInTouchMode = true;
        view.requestFocus();
    }

}