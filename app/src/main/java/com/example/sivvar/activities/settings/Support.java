package com.example.sivvar.activities.settings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.sivvar.R;
import com.example.sivvar.helpers.Utility;

public class Support extends AppCompatActivity {

    private LinearLayout live_chat, sivvar_call, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supports);
        Utility.initToolbar(this, true, "Support", false);
        
        findViewByIds();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        live_chat.setOnClickListener(v -> comingSoonAlert());
        sivvar_call.setOnClickListener(v -> comingSoonAlert());
        email.setOnClickListener(v -> comingSoonAlert());
    }

    private void findViewByIds() {
        live_chat = findViewById(R.id.live_chat);
        sivvar_call = findViewById(R.id.sivvar_call);
        email = findViewById(R.id.email);
    }

    private void comingSoonAlert(){
        new AlertDialog.Builder(this)
                .setMessage("Under construction, check back later.")
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }
}