package com.example.sivvar.activities.settings;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.sivvar.R;
import com.example.sivvar.helpers.Utility;

public class Notifications extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        Utility.initToolbar(this, true, "Notifications", false);
    }
}