package com.example.sivvar.activities.settings;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sivvar.R;
import com.example.sivvar.helpers.User;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.models.UserModel;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class Profile extends AppCompatActivity {

    private Button changeemail;
    private EditText email, firstname, lastname, phonefield;
    private TextView changenumber;

    private UserModel me;

    private SearchableSpinner spinnerCountries;
    private String[] countryCode;
    private String selectedCountryCode;
    private ImageView arrow_down, flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Utility.initToolbar(this, true, "Profile", false);
        me = User.getUserModel(this);

        findViewByIds();
        setContents();
        setOnClickListeners();

        setFlagData();
    }

    private void setOnClickListeners() {
        changeemail.setOnClickListener(v -> email.setEnabled(true));
        changenumber.setOnClickListener(v -> phonefield.setEnabled(true));

        arrow_down.setOnClickListener(v -> spinnerCountries.callOnClick());
        flag.setOnClickListener(v -> spinnerCountries.callOnClick());
    }

    private void setContents() {
        email.setEnabled(false);
        phonefield.setEnabled(false);

        email.setText(me.getEmailAddress());
        phonefield.setText(me.getPhoneNumber().replaceFirst("234", ""));

        firstname.setText(me.getFirstName());
        lastname.setText(me.getLastName());
    }

    private void findViewByIds() {
        changeemail = findViewById(R.id.changeemail);
        email = findViewById(R.id.email);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        phonefield = findViewById(R.id.phonefield);
        changenumber = findViewById(R.id.changenumber);

        spinnerCountries = findViewById(R.id.spinnerCountries);
        arrow_down = findViewById(R.id.arrow_down);
        flag = findViewById(R.id.flag);
    }

    private void setFlagData() {
        try{
            countryCode = new String[]{"+93", "+355", "+213", "+1684", "+376", "+244", "+1264", "+0", "+1268", "+54", "+374", "+297", "+61", "+43", "+994", "+1242", "+973", "+880", "+1246", "+375", "+32", "+501", "+229", "+1441", "+975", "+591", "+387", "+267", "+0", "+55", "+246", "+673", "+359", "+226", "+257", "+855", "+237", "+1", "+238", "+1345", "+236", "+235", "+56", "+86", "+61", "+672", "+57", "+269", "+242", "+242", "+682", "+506", "+225", "+385", "+53", "+357", "+420", "+45", "+253", "+1767", "+1809", "+670", "+593", "+20", "+503", "+240", "+291", "+372", "+251", "+61", "+500", "+298", "+679", "+358", "+33", "+594", "+689", "+0", "+241", "+220", "+995", "+49", "+233", "+350", "+30", "+299", "+1473", "+590", "+1671", "+502", "+44", "+224", "+245", "+592", "+509", "+0", "+504", "+852", "+36", "+354", "+91", "+62", "+98", "+964", "+353", "+972", "+39", "+1876", "+81", "+44", "+962", "+7", "+254", "+686", "+850", "+82", "+965", "+996", "+856", "+371", "+961", "+266", "+231", "+218", "+423", "+370", "+352", "+853", "+389", "+261", "+265", "+60", "+960", "+223", "+356", "+44", "+692", "+596", "+222", "+230", "+269", "+52", "+691", "+373", "+377", "+976", "+1664", "+212", "+258", "+95", "+264", "+674", "+977", "+599", "+31", "+687", "+64", "+505", "+227", "+234", "+683", "+672", "+1670", "+47", "+968", "+92", "+680", "+970", "+507", "+675", "+595", "+51", "+63", "+0", "+48", "+351", "+1787", "+974", "+262", "+40", "+70", "+250", "+290", "+1869", "+1758", "+508", "+1784", "+684", "+378", "+239", "+966", "+221", "+381", "+248", "+232", "+65", "+421", "+386", "+44", "+677", "+252", "+27", "+0", "+211", "+34", "+94", "+249", "+597", "+47", "+268", "+46", "+41", "+963", "+886", "+992", "+255", "+66", "+228", "+690", "+676", "+1868", "+216", "+90", "+7370", "+1649", "+688", "+256", "+380", "+971", "+44", "+1", "+1", "+598", "+998", "+678", "+39", "+58", "+84", "+1284", "+1340", "+681", "+212", "+967", "+38", "+260", "+263"};
            ArrayAdapter countriesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryCode);
            spinnerCountries.setAdapter(countriesAdapter);

            Handler handler = new Handler();
            handler.postDelayed(() -> {
                try {
                    if(me.getCountryCode().equals("234"))
                        spinnerCountries.setSelection(159);
                    else{
                        //TODO:: switch statement to check and select appropriate
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }, 500);

            spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedCountryCode = countryCode[spinnerCountries.getSelectedItemPosition()];
                    witchFlags(spinnerCountries.getSelectedItemPosition());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void witchFlags(int position){
        switch (position){
            case 0:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 1:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 2:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 3:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 4:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 5:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 6:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 7:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 8:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 9:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 10:
                flag.setImageResource(R.drawable.unknownflag);
                break;
            case 159:
                flag.setImageResource(R.drawable.nigeria_flag);
                break;
            case 93:
                flag.setImageResource(R.drawable.swiden);
                break;

        }
    }
}
