package com.example.sivvar.activities.linphone;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sivvar.BaseApplication;
import com.example.sivvar.R;
import com.example.sivvar.activities.custom_views_adapters.CustomViewPager;
import com.example.sivvar.activities.custom_views_adapters.MainActivityViewPagerAdapter;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.User;
import com.example.sivvar.interfaces.renders.linphone.CallState;
import com.example.sivvar.models.UserModel;

import org.linphone.core.Call;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.ProxyConfig;
import org.linphone.core.RegistrationState;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    public static CoreListenerStub mCoreListener;
    public static String proxyState = SivvarKey.IN_PROGRESS.getValue();
    public CustomViewPager viewPager;
    public static CallState callState;
    public static UserModel me;
    public static BottomNavigationView navView;

    public Handler mHandler;

    public static Core core;

    public CardView stateColor;
    public TextView stateText;

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {

            case R.id.navigation_home:
                viewPager.setCurrentItem(0, false);
                return true;

            case R.id.navigation_categories:
                viewPager.setCurrentItem(1, false);
                return true;

            case R.id.navigation_call_history:
                viewPager.setCurrentItem(2, false);
                return true;

            case R.id.navigation_settings:
                viewPager.setCurrentItem(3, false);
                return true;

        }
        return false;
    };

    public static MainActivity mInstance = null;
    public static MainActivity getInstance(){
        if(mInstance !=null)
            return mInstance;
        else
            return new MainActivity();
    }

    public static AudioManager mAudioManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setContexts();

        initAudioManger();

        findViewByIds();

        initViewPager();

        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.navigation_home);

        initMCoreListener();

        mInstance = this;
    }

    private void initAudioManger() {
            mAudioManager = ((AudioManager) getSystemService(Context.AUDIO_SERVICE));
        if(mAudioManager!=null){
            mAudioManager.setMode(AudioManager.MODE_NORMAL);
        }
    }

    public void initMCoreListener(){
        try{
            startService();

            Thread thread = new Thread(){
                @Override
                public void run(){
                    setListener();
                }
            };

            thread.start();
       }catch (Exception ex){
            Log.e("Account Exception", ""+ex.getMessage());
        }

        mInstance = this;
    }

    private void setListener() {
        mCoreListener = new CoreListenerStub() {
            @Override
            public void onRegistrationStateChanged(Core core, ProxyConfig cfg, RegistrationState state, String message) {
                runOnUiThread(() -> updateLed(state));
            }

            @Override
            public void onCallStateChanged(Core core, Call call, Call.State state, String message) {
                if(callState!=null){
                    callState.onStateChanged(core, call, state, message);
                }
            }
        };

        if(LinphoneService.getCore()!=null){
            LinphoneService.getCore().addListener(mCoreListener);
            core = LinphoneService.getCore();
        }
    }

    public void resumeMCoreListener(){
        try{
            if(LinphoneService.getCore()==null){
                initMCoreListener();
            }else{
                Handler handler = new Handler();
                handler.postDelayed(this::setListener, 1000);
            }
        }catch (Exception ex){
            Log.e("Account Exception", ""+ex.getMessage());
        }

        mInstance = this;
    }

    private void initViewPager() {
        Bundle bundle = getIntent().getExtras();
        MainActivityViewPagerAdapter adapter = new MainActivityViewPagerAdapter
                (getSupportFragmentManager(), 4, bundle);

        viewPager.setAdapter(adapter);
    }

    private void setContexts() {
        me = User.getUserModel(this);
        mHandler = new Handler();
    }

    private void findViewByIds() {
        LinearLayout rootView = findViewById(R.id.container);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.disableScroll(true);

        stateColor = rootView.findViewById(R.id.stateColor);
        stateText = rootView.findViewById(R.id.stateText);
    }

    /**
     * Ask runtime permissions, such as record audio and camera
     * We don't need them here but once the user has granted them we won't have to ask again
     */
    @Override
    protected void onStart() {
        super.onStart();
        checkAndRequestCallPermissions();
    }

    /** The best way to use Core listeners in Activities is to add them in onResume
     * and to remove them in onPause
     */
    @Override
    protected void onResume() {
        super.onResume();

        resumeMCoreListener();

        String res = User.getSIVVARstate(this);

        if(!res.equalsIgnoreCase("none")){
            if(res.equalsIgnoreCase(SivvarKey.CONNECTED.getValue())){
                runOnUiThread(() -> stateColor.setCardBackgroundColor(getResources().getColor(R.color.green)));
                res = "SIVVAR Is Ready";
            } else {
                runOnUiThread(() -> stateColor.setCardBackgroundColor(getResources().getColor(R.color.red)));
                res = "SIVVAR Not Ready";
            }

            String finalRes1 = res;
            runOnUiThread(() -> stateText.setText(finalRes1));
        }

        Handler handler = new Handler();
        String finalRes = res;
        handler.post(() -> {
            if(finalRes.equalsIgnoreCase("SIVVAR Is Ready"))
                BaseApplication.Companion.updateNotification(SivvarKey.CONNECTED.getValue(), MainActivity.this);
            else
                BaseApplication.Companion.updateNotification(SivvarKey.DISCONNECTED.getValue(), MainActivity.this);
        });
    }

    public static void setCallStateInterface(CallState callstate){
        callState = callstate;
    }

    @Override
    public void onRequestPermissionsResult(
        int requestCode, String[] permissions, int[] grantResults) {

        for (int i = 0; i < permissions.length; i++) {
            Log.e("[Permission] ",
                    permissions[i]
                            + " is "
                            + (grantResults[i] == PackageManager.PERMISSION_GRANTED
                            ? "granted"
                            : "denied"));
        }
    }

    public void updateLed(RegistrationState state) {
        switch (state) {
            case Ok:
                proxyState = SivvarKey.CONNECTED.getValue();

                stateColor.setCardBackgroundColor(getResources().getColor(R.color.green));
                stateText.setText("SIVVAR Is Ready");

                BaseApplication.Companion.updateNotification(proxyState, this);
                break;

            case None: // default state
                proxyState = SivvarKey.IDLE.getValue();

                stateColor.setCardBackgroundColor(getResources().getColor(R.color.idle));
                stateText.setText("SIVVAR Not Ready");

                BaseApplication.Companion.updateNotification(proxyState, this);
                break;

            case Cleared: // This state is when you're disconnected
                proxyState = SivvarKey.DISCONNECTED.getValue();

                stateColor.setCardBackgroundColor(getResources().getColor(R.color.red));
                stateText.setText("SIVVAR Not Ready");

                BaseApplication.Companion.updateNotification(proxyState, this);
                break;

            case Failed: // This one means an error happened, for example a bad password
                proxyState = SivvarKey.FAILED.getValue();

                stateColor.setCardBackgroundColor(getResources().getColor(R.color.red));
                stateText.setText("SIVVAR Not Ready");

                BaseApplication.Companion.updateNotification(proxyState, this);
                break;

            case Progress: // Connection is in progress, next state will be either Ok or Failed
                proxyState = SivvarKey.IN_PROGRESS.getValue();

                stateColor.setCardBackgroundColor(getResources().getColor(R.color.orange));
                stateText.setText("SIVVAR Getting Ready");

                BaseApplication.Companion.updateNotification(proxyState, this);
                break;
        }
    }

    private void checkAndRequestCallPermissions() {
        ArrayList<String> permissionsList = new ArrayList<>();

        /** Some required permissions needs to be validated manually by the user
        * Here we ask for record audio and camera to be able to make video calls with sound
        * Once granted we don't have to ask them again, but if denied we can
        */
         int recordAudio = getPackageManager().checkPermission(Manifest.permission.RECORD_AUDIO, getPackageName());
        int camera = getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
        int audioControl = getPackageManager().checkPermission(Manifest.permission.MODIFY_AUDIO_SETTINGS, getPackageName());

        Log.e("[Permission]", "Record audio permission is "
                        + (recordAudio == PackageManager.PERMISSION_GRANTED
                        ? "granted"
                        : "denied"));

        Log.e("[Permission]",  "Camera permission is "
                        + (camera == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

        if (recordAudio != PackageManager.PERMISSION_GRANTED) {
            Log.e("[Permission]", "Asking for record audio");
            permissionsList.add(Manifest.permission.RECORD_AUDIO);
        }

        if (camera != PackageManager.PERMISSION_GRANTED) {
            Log.e("[Permission]", "Asking for camera");
            permissionsList.add(Manifest.permission.CAMERA);
        }

        if (audioControl != PackageManager.PERMISSION_GRANTED) {
            Log.e("[Permission]", "Asking for camera");
            permissionsList.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
        }

        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }

    // Method to start the service
    public void startService() {
        startService(new Intent(MainActivity.this, LinphoneService.class));
    }

    // Method to stop the service
    public void stopService() {
        stopService(new Intent(getBaseContext(), LinphoneService.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        User.removeSIVVARstate(this);
        stopService();
    }

    public Core getCore() {
        return core;
    }

    @Override
    protected void onPause() {
        super.onPause();
        User.saveSIVVARstate(proxyState, this);
    }
}