package com.example.sivvar.activities;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.CountDownTimer;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sivvar.helpers.SivvarLoadingDialog;
import com.example.sivvar.R;
import com.example.sivvar.helpers.Utility;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import static com.example.sivvar.helpers.DtosKt.generateCompleteIVRStamp;
import static com.example.sivvar.helpers.DtosKt.generateStamp;
import static com.example.sivvar.helpers.DtosKt.generateStampSMS;

public class OtpActivity extends AppCompatActivity {

    private TextView enter_code_wrong_number;
    private EditText one, two, three, four;
    private Button resend_sms;

    private String code = "";
    private String data= "+2348061114444";
    private String countryCode = "";

    private JsonObject parseData = new JsonObject();

    private LinearLayout parent;

    private SivvarLoadingDialog sivvarLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        try{
            if(getIntent().hasExtra("data")){
                data = getIntent().getStringExtra("data");

                Gson gson = new Gson();
                Type type = new TypeToken<JsonObject>(){}.getType();
                parseData = gson.fromJson(data, type);
            }
        }catch (Exception ex){
            Log.e("EXCEPTION", ""+ex.getMessage());
        }

        setContexts();
        findViewByIds();

        setClickablePoliciesText(parseData.get("phone").toString());

        textWatchers();
        onBackSpace();
        switchFocus(1);

        resend_sms.setClickable(false);
        startCountDownToOtherOptions(getResources().getString(R.string.resendcall));
    }

    public void startCountDownToOtherOptions(String text){
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                resend_sms.setText(text+" in "+millisUntilFinished / 1000 + " Secs");
            }

            public void onFinish() {
                try {
                    resend_sms.setText(text);
                    resend_sms.setClickable(true);
                    resend_sms.setOnClickListener(v -> {
                        if(text.contains(getResources().getString(R.string.try_another))){
                            showDialogForOtherOptions();
                        }else{
                            startCountDownToOtherOptions(getResources().getString(R.string.try_another));
                            requestIVR();
                        }

                        resend_sms.setClickable(false);
                    });

                    try{
                        Utility.hideKeyboard(OtpActivity.this);
                    }catch (Exception ex){
                        Log.e("ERROR", "Could not close keyboard...");
                        ex.printStackTrace();
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }.start();
    }

    private void setContexts() {
        sivvarLoadingDialog = new SivvarLoadingDialog(this);
    }

    private void onBackSpace() {
        one.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                one.setText("");
                switchFocus(1);
            }
            return false;
        });

        two.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                if(two.getText().toString().length()>0){
                    two.setText("");
                    switchFocus(2);
                } else
                    switchFocus(1);
            }
            return false;
        });

        three.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                if(three.getText().toString().length()>0){
                    three.setText("");
                    switchFocus(3);
                } else{
                    switchFocus(2);
                }
            }
            return false;
        });

        four.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL){
                if(four.getText().toString().length()>0){
                    four.setText("");
                    switchFocus(4);
                } else
                    switchFocus(3);
            }
            return false;
        });
    }

    private void textWatchers() {
        one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("STILL", "Got here....");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("LENGTH", s.toString().length()+"");
                if (s.toString().length()>0)
                    switchFocus(2);
            }
        });

        two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("LENGTH", s.toString().length()+"");
                if (s.toString().length()>0) {
                    switchFocus(3);
                }
            }
        });

        three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("LENGTH", s.toString().length()+"");
                if (s.toString().length()>0) {
                    switchFocus(4);
                }
            }
        });

        four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()>0) {
                    getCodeFromEditables();
                    if(code.length()==4){
                        sivvarLoadingDialog.show();

                        ApiCalls apiCalls = new ApiCalls("init_new", OtpActivity.this, 40, 40, 40);

                        JsonObject param = new JsonObject();
                        param.addProperty("msISDN", parseData.get("phone").getAsString());
                        param.addProperty("Imei", parseData.get("Imei").getAsString());
                        param.addProperty("regCode", code);
                        param.addProperty("Channel", parseData.get("channel").getAsString());
                        param.addProperty("sivvarStamp", generateCompleteIVRStamp(parseData.get("Imei").getAsString(), parseData.get("phone").getAsString(), code, parseData.get("channel").getAsString()));

                        apiCalls.completeivrphonecallverification(param, new ResponseCallback() {
                            @Override
                            public String onSuccess(String res) {
                                sivvarLoadingDialog.dismiss();

                                Log.e("DATA-AFTER-CALL", ""+res);

                                if(ResponseUtil.Companion.isSuccessfullIVR(res)){
                                    JsonObject data = new JsonObject();
                                    data.addProperty("countryCode", parseData.get("countryCode").getAsString());
                                    data.addProperty("phone", parseData.get("phone").getAsString());
                                    data.addProperty("Imei", parseData.get("Imei").getAsString());
                                    data.addProperty("responseData", ResponseUtil.Companion.getDataIVR(res));

                                    Gson gson = new Gson();
                                    Type type = new TypeToken<JsonObject>(){}.getType();
                                    String d = gson.toJson(data, type);

                                    Utility.navigateWithParams(OtpActivity.this, new CreateProfile(), "data", d);
                                    finish();
                                }else{
                                    showDialog("Error", ResponseUtil.Companion.getMessageIVR(res));
                                }

                                return null;
                            }

                            @Override
                            public String onFailure(String res) {
                                sivvarLoadingDialog.dismiss();

                                showDialog("Error", "Something went wrong, try again.");
                                return null;
                            }
                        });

                    }else{
                        switchFocus(4);
                        Snackbar.make(parent, "Incorrect opt", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    /*
     * This function create and displays the AlertDialog with the messages parsed
     * via the @params, and also it sets the AlertDialog object back to null.
     * */
    private void showDialog(String title, String msg){
        try{
            new AlertDialog.Builder(OtpActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                })
            .show();
        }catch (Exception ex){
            Log.e("EXCEPTION", ex.getMessage()+"");
        }
    }

    private void showDialogForOtherOptions(){
        try{
            new AlertDialog.Builder(OtpActivity.this)
                .setTitle("Options")
                .setMessage("You can try other options if you're not getting otp with the current method")
                .setCancelable(false)
                .setNegativeButton("Sms", (dialog, which) -> {
                    dialog.dismiss();

                    startCountDownToOtherOptions(getResources().getString(R.string.try_another));
                    parseData.remove("channel");
                    parseData.addProperty("channel", "SMS");
                    requestSMS();
                })

                .setPositiveButton("Call", (dialog, which) -> {
                    dialog.dismiss();

                    startCountDownToOtherOptions(getResources().getString(R.string.try_another));
                    parseData.remove("channel");
                    parseData.addProperty("channel", "VOICE");
                    requestIVR();
                })
            .show();
        }catch (Exception ex){
            Log.e("EXCEPTION", ex.getMessage()+"");
        }
    }

    private void findViewByIds() {
        enter_code_wrong_number = findViewById(R.id.enter_code_wrong_number);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);

        parent = findViewById(R.id.parent);
        resend_sms = findViewById(R.id.resend_sms);
    }

    private void setCode(String code){
        try{
            String[] y = code.split("");
            one.setText(y[0]);
            two.setText(y[1]);
            three.setText(y[2]);
            four.setText(y[3]);
        }catch (Exception ex){
            Log.e("SETTING CODE", ex.getMessage()+"");
        }
    }

    private void switchFocus(int p){
        switch (p){
            case 1:
                one.setFocusable(true);
                one.setFocusableInTouchMode(true);
                one.requestFocus();

                two.setFocusable(false);
                three.setFocusable(false);
                four.setFocusable(false);
                break;

            case 2:
                two.setFocusable(true);
                two.setFocusableInTouchMode(true);
                two.requestFocus();

                one.setFocusable(false);
                three.setFocusable(false);
                four.setFocusable(false);
                break;

            case 3:
                three.setFocusable(true);
                three.setFocusableInTouchMode(true);
                three.requestFocus();

                one.setFocusable(false);
                two.setFocusable(false);
                four.setFocusable(false);
                break;

            case 4:
                four.setFocusable(true);
                four.setFocusableInTouchMode(true);
                four.requestFocus();

                one.setFocusable(false);
                two.setFocusable(false);
                three.setFocusable(false);
                break;
        }
    }

    private void getCodeFromEditables(){
        try{
            code = one.getText().toString()+two.getText().toString()+three.getText().toString()+four.getText().toString();
        }catch (Exception ex){
            Log.e("Exception", ex.getMessage()+"");
        }
    }

    private void setClickablePoliciesText(String phone) {
        try{
            String s = getResources().getString(R.string.enterthecode_)+" "+phone+" "+getResources().getString(R.string.wrongnumber);
            SpannableString ss = new SpannableString(s);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Utility.navigate(OtpActivity.this, new SetUp01Activity());
                    finish();
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, 65, 79, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            enter_code_wrong_number.setText(ss);
            enter_code_wrong_number.setMovementMethod(LinkMovementMethod.getInstance());
            enter_code_wrong_number.setHighlightColor(Color.GREEN);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void requestIVR(){
        try{
            sivvarLoadingDialog.show();

            ApiCalls apiCalls = new ApiCalls("init_new", this, 30, 30, 30);
            JsonObject param = generateStamp(String.valueOf(true), parseData.get("Imei").getAsString(), parseData.get("phone").getAsString());
            apiCalls.ivrphonecallverification(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {

                    sivvarLoadingDialog.dismiss();

                    if(ResponseUtil.Companion.isSuccessfullIVR(res)){
                        //TODO:: Display waiting message
                    }else{
                        showDialog("Exception", ResponseUtil.Companion.getMessage(res));
                    }

                    return null;
                }

                @Override
                public String onFailure(String res) {
                    sivvarLoadingDialog.dismiss();

                    showDialog("Exception", res);
                    return null;
                }
            });

        }catch (Exception ex){
            showDialog( "Exception", ex.getMessage());
        }
    }

    private void requestSMS(){

        try{
            sivvarLoadingDialog.show();
            ApiCalls apiCalls = new ApiCalls("init_new", this, 30, 30, 30);
            JsonObject object = new JsonObject();
            object.addProperty("msISDN", parseData.get("phone").getAsString());
            object.addProperty("Imei", parseData.get("Imei").getAsString());
            object.addProperty("sivvarStamp", generateStampSMS(parseData.get("Imei").getAsString(), parseData.get("phone").getAsString()));

            apiCalls.initBySms(object, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    sivvarLoadingDialog.dismiss();

                    if(ResponseUtil.Companion.isSuccessfullIVR(res)){
                      //Todo:: display wating messages to users
                    }else if(ResponseUtil.Companion.isSuccessfullIVRretuning(res)){
                        JsonObject param = new JsonObject();
                        param.addProperty("phone", parseData.get("phone").getAsString());
                        param.addProperty("countryCode", parseData.get("countryCode").getAsString());
                        param.addProperty("Imei", parseData.get("Imei").getAsString());
                        param.addProperty("requestCode", ResponseUtil.Companion.getDataIVR(res));

                        Gson gson = new Gson();
                        Type type = new TypeToken<JsonObject>(){}.getType();
                        String d = gson.toJson(param, type);

                        Utility.navigateWithParams(OtpActivity.this, new CreateProfile(), "data", d);
                        finish();
                    }else{
                        showDialog("Exception", ResponseUtil.Companion.getMessage(res));
                    }

                    return null;
                }

                @Override
                public String onFailure(String res) {
                    sivvarLoadingDialog.dismiss();

                    showDialog("Exception", res);
                    return null;
                }
            });

        }catch (Exception ex){
            showDialog("Exception", ex.getMessage());
        }
    }

}