package com.example.sivvar.activities.linphone;

import android.app.Activity;
import android.app.Dialog;
import android.media.AudioManager;

import androidx.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sivvar.R;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.interfaces.renders.linphone.CallState;
import com.example.sivvar.models.CallHistory;
import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.models.MyFavourites;
import com.example.sivvar.models.SubServices;

import org.linphone.core.Address;
import org.linphone.core.Call;
import org.linphone.core.CallParams;
import org.linphone.core.Core;
import org.linphone.core.MediaDirection;

import java.util.Timer;
import java.util.TimerTask;

import static android.media.AudioManager.STREAM_VOICE_CALL;

public class CallingDialog extends Dialog implements CallState{
    private final Activity context;
    private static CategoryServices categoryServices;
    private static MyFavourites favourites;
    private static SubServices subServices;

    private TextView hideText;

    private TextView timer, serviceTitle, input_code;
    private ImageView serLogo, speaker, dialpad, muteUnmute;
    private LinearLayout delete, hide, keypad_layout, mute_unmute;
    private ImageView endcall;

    private Button star, arh, zero, one, two, three, four, five, six, seven, eight, nine;

    private String imgLink;

    private String address;
    private static Core core;
    private static Call call;

    private Timer t;
    private boolean hideDTMF;
    private String DTMF_VALUE = "";

    private CallHistory callHistory;

    private AudioManager mAudioManager;
    private static final int LINPHONE_VOLUME_STREAM = STREAM_VOICE_CALL;
    private static final int dbStep = 4;

    public CallingDialog(@NonNull final Activity context, SubServices subServices,
                         MyFavourites favourites, CategoryServices categoryServices,
                         int width, int height) {
        super(context, R.style.full_screen_dialog);
        setContentView(R.layout.calling_layout);
        this.context = context;

        findViewByIds();
        setClickListeners();

        setCore();
        MainActivity.setCallStateInterface(this);

        CallingDialog.subServices = subServices;
        CallingDialog.favourites = favourites;
        CallingDialog.categoryServices = categoryServices;

        try{
            if(subServices!=null){
                imgLink = ""+subServices.getService_logo();
                address = subServices.getService_dial_code();

                serviceTitle.setText(subServices.getService_name());

                //Here is where we create the call history
                callHistory = new CallHistory(subServices.getService_dial_code(), subServices.getService_logo(),
                    subServices.getService_name(), String.valueOf(subServices.getService_id()), "0");
                callHistory.save();

            }else if(favourites!=null){
                imgLink = ""+favourites.getService_logo();
                address = favourites.getService_dial_code();

                serviceTitle.setText(favourites.getService_name());

                //Here is where we create the call history
                callHistory = new CallHistory(favourites.getService_dial_code(), favourites.getService_logo(),
                        favourites.getService_name(), String.valueOf(favourites.getService_id()), "0");
                callHistory.save();
            }else if(categoryServices!=null){
                imgLink = ""+categoryServices.getService_logo();
                address = categoryServices.getService_dial_code();

                serviceTitle.setText(categoryServices.getService_name());

                //Here is where we create the call history
                callHistory = new CallHistory(categoryServices.getService_dial_code(), categoryServices.getService_logo(),
                        categoryServices.getService_name(), String.valueOf(categoryServices.getService_id()), "0");
                callHistory.save();
            }

            Glide
                .with(context)
                .load(imgLink)
                .error(R.drawable.swiden)
                .centerCrop()
                .into(serLogo);

            setCanceledOnTouchOutside(false);
            setCancelable(false);
            getWindow().setLayout(width, height);
            getWindow().setGravity(Gravity.FILL);

            setOnDismissListener(dialog -> {
                if(call!=null){
                    callHistory.speakTime = getTime()+" Secs";
                    callHistory.save();

                    call.terminate();
                }
            });

            show();
            makeCall();
        }catch (Exception ex){
            Log.e("Exception-0", ""+ex.getMessage());
        }
    }

    private void setCore() {
        try{
            core = MainActivity.getInstance().getCore();
            mAudioManager = MainActivity.mAudioManager;
        }catch (Exception ex){
            Log.e("Core Exception", ""+ex.getMessage());
        }
    }

    private String asteriskBuildPlus(String v){
        dtmfBuilderPlus(v);

        StringBuilder u = new StringBuilder();
        if(DTMF_VALUE.length()>0){
            char[] a = DTMF_VALUE.toCharArray();
            for(char ignore : a){
                u.append("*");
            }

            return u.toString();
        }else
            return "";
    }

    private String showAsterisks(){
        StringBuilder u = new StringBuilder();
        if(DTMF_VALUE.length()>0){
            char[] a = DTMF_VALUE.toCharArray();
            for(char ignore : a){
                u.append("*");
            }

            return u.toString();
        }else
            return "";
    }

    private String asteriskBuildMinus(){
        dtmfBuilderMinus();

        StringBuilder u = new StringBuilder();
        if(DTMF_VALUE.length()>0){
            char[] a = DTMF_VALUE.toCharArray();
            for(int x = 0; x<a.length; x++){
                if(x!=a.length-1)
                    u.append("*");
            }

            return u.toString();
        }else
            return "";
    }

    private String dtmfBuilderPlus(String v){
        return DTMF_VALUE+=v;
    }

    private String dtmfBuilderMinus(){
        StringBuilder u = new StringBuilder();
        if(DTMF_VALUE.length()>0){
            char[] a = DTMF_VALUE.toCharArray();
            for(int x = 0; x<a.length; x++){
                if(x!=a.length-1)
                    u.append(a[x]);
            }
            DTMF_VALUE = u.toString();
            return u.toString();
        }

        return "";
    }

    private void setClickListeners() {

        dialpad.setOnClickListener(v -> {
            if(keypad_layout.getVisibility() == View.VISIBLE)
                keypad_layout.setVisibility(View.GONE);
            else
                keypad_layout.setVisibility(View.VISIBLE);
        });

        hide.setOnClickListener(v -> {
            if(hideDTMF){
                hideDTMF = false;
                input_code.setText(DTMF_VALUE);
                hideText.setText("Hide");
            } else{
                hideDTMF = true;
                hideText.setText("Show");
                input_code.setText(showAsterisks());
            }
        });

        star.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("*"));
            else
                input_code.setText(dtmfBuilderPlus("*"));

            setDTMF('*');
        });

        arh.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("#"));
            else
                input_code.setText(dtmfBuilderPlus("#"));

            setDTMF('#');
        });

        zero.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("0"));
            else
                input_code.setText(dtmfBuilderPlus("0"));

            setDTMF('0');
        });

        one.setOnClickListener(v ->{
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("1"));
            else
                input_code.setText(dtmfBuilderPlus("1"));

            setDTMF('1');
        });

        two.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("2"));
            else
                input_code.setText(dtmfBuilderPlus("2"));

            setDTMF('2');
        });

        three.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("3"));
            else
                input_code.setText(dtmfBuilderPlus("3"));

            setDTMF('3');
        });

        four.setOnClickListener(v ->{
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("4"));
            else
                input_code.setText(dtmfBuilderPlus("4"));

            setDTMF('4');
        });

        five.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("5"));
            else
                input_code.setText(dtmfBuilderPlus("5"));

            setDTMF('5');
        });

        six.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("6"));
            else
                input_code.setText(dtmfBuilderPlus("6"));

            setDTMF('6');
        });

        seven.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("7"));
            else
                input_code.setText(dtmfBuilderPlus("7"));

            setDTMF('7');
        });

        eight.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("8"));
            else
                input_code.setText(dtmfBuilderPlus("8"));

            setDTMF('8');
        });

        nine.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildPlus("9"));
            else
                input_code.setText(dtmfBuilderPlus("9"));

            setDTMF('9');
        });

        endcall.setOnClickListener(v -> {
            try{
                call.terminate();

                DTMF_VALUE = "";
                hideDTMF = false;

                core.getDownloadBandwidth();
                cancel();
            }catch (Exception ex){
                Log.e("Exception-1", ""+ex.getMessage());

                DTMF_VALUE = "";
                hideDTMF = false;
                cancel();
            }
        });

        delete.setOnClickListener(v -> {
            if(hideDTMF)
                input_code.setText(asteriskBuildMinus());
            else
                input_code.setText(dtmfBuilderMinus());
        });

        mute_unmute.setOnClickListener(v -> {
            if(call.getSpeakerMuted()){
                call.setSpeakerMuted(false);
                muteUnmute.setBackground(context.getResources().getDrawable(R.drawable.unmute));
            } else{
                muteUnmute.setBackground(context.getResources().getDrawable(R.drawable.mute));
                call.setSpeakerMuted(true);
            }
        });

        speaker.setOnClickListener(v -> {
            if(mAudioManager.isSpeakerphoneOn()){
                mAudioManager.setSpeakerphoneOn(false);
                speaker.setBackground(context.getResources().getDrawable(R.drawable.ic_speaker_button));
            }else{
                mAudioManager.setSpeakerphoneOn(true);
                speaker.setBackground(context.getResources().getDrawable(R.drawable.telephone));
            }
        });
    }

    private CallParams getCallParam(CallParams callParams){
        callParams.enableVideo(false);
        callParams.enableAudio(true);
        callParams.addCustomHeader("Script_Caller", "CODEC");
        callParams.enableLowBandwidth(true);
        callParams.setAudioBandwidthLimit(Integer.valueOf(SivvarKey.AUDIO_QUALITY_G729.getValue()));
        callParams.setAudioDirection(MediaDirection.SendRecv);

        return callParams;
    }

    private void findViewByIds() {
        hideText = findViewById(R.id.hideText);
        serviceTitle = findViewById(R.id.serviceTitle);
        serLogo = findViewById(R.id.serLogo);
        timer = findViewById(R.id.timer);

        endcall = findViewById(R.id.endcall);

        keypad_layout = findViewById(R.id.keypad_layout);

        input_code = findViewById(R.id.input_code);
        speaker = findViewById(R.id.speaker);
        dialpad = findViewById(R.id.dialpad);
        mute_unmute = findViewById(R.id.mute_unmute);
        muteUnmute = findViewById(R.id.muteUnmute);
        delete = findViewById(R.id.delete);
        hide = findViewById(R.id.hide);

        star = findViewById(R.id.star);
        arh = findViewById(R.id.arh);
        zero = findViewById(R.id.zero);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);
        seven = findViewById(R.id.seven);
        eight = findViewById(R.id.eight);
        nine = findViewById(R.id.nine);
    }

    private void makeCall(){
        try{
            if(core==null)
                core = MainActivity.getInstance().getCore();

            if(address!=null && !address.trim().isEmpty()){
                Address addressToCall = core.interpretUrl(address);
                CallParams params = core.createCallParams(null);
                if (addressToCall != null) {
                    core.inviteAddressWithParams(addressToCall, getCallParam(params));

                    if(mAudioManager!=null)
                        mAudioManager.setSpeakerphoneOn(true);
                }
            }
        }catch (Exception ex){
            Log.e("Exception-2", ""+ex.getMessage());
        }
    }

    private void setDTMF(char val){
        Log.e("DTMF", ""+val);

        try{
            if(call!=null){
                core.playDtmf(val, 1);
                call.sendDtmf(val);
                core.start();
            }
        }catch (Exception ex){
            Log.e("Exception-3", ""+ex.getMessage());
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onStateChanged(Core core, Call call, Call.State state, String message) {
        if (state == Call.State.End) {
            cancel();
        }else if(state == Call.State.Released){
            stopTimer();
            cancel();
        }else if(state == Call.State.OutgoingProgress){
            Log.e("Tag", "OUT PRO "+message);
        }else if(state == Call.State.OutgoingInit){
            Log.e("Tag", "OUT INIT "+message);
        }else if(state == Call.State.OutgoingEarlyMedia){
            Log.e("Tag", "OUT MEDIA "+message);
        }else if(state == Call.State.OutgoingRinging){
            Log.e("Tag", "OUT RINGIN "+message);
        }else if(state == Call.State.StreamsRunning){
            Log.e("Tag", "STREAM RUNNING "+message);
        }else if(state == Call.State.Connected){
            Log.e("Tag", "CONNECTED "+message);

            CallingDialog.call = call;
            CallingDialog.core.startDtmfStream();
            startTimer();
        }else if(state == Call.State.Idle){
            Log.e("Tag", "IDLE "+message);
        }else if(state == Call.State.Paused){
            Log.e("Tag", "PAUSED "+message);
        }else if(state == Call.State.Updating){
            Log.e("Tag", "UPDATING "+message);
        }else if(state == Call.State.Resuming){
            Log.e("Tag", "OUT RESUMING "+message);
        }else{
            Log.e("Tag", "UNKNOWN "+message);
        }
    }

    private void startTimer() {
        try{
            t = new Timer();
            TimerTask updateTimeSpent = new TimerTask() {
                public void run() {
                    if (timer != null)
                        if (core != null){
                            String time = getTime();
                            context.runOnUiThread(() -> timer.setText(time));
                        }
                }
            };

            t.schedule(updateTimeSpent, 1000L, 1000L);
        }catch (Exception ex){
            Log.e("TIMER EXCEPTION", ""+ex.getMessage());
        }
    }

    private void stopTimer(){
        try{
            if(t!=null){
                t.cancel();
            }
        }catch (Exception ex){
            Log.e("TIMER EXCPETION", ""+ex.getMessage());
        }
    }

    private String getTime(){
        int t = call.getDuration();
        String time;
        if (t<60){
            time = "0:"+t;
        }else{
            int remainder = t%60;
            t = t/60;
            time = t+":"+remainder;
        }

        return time;
    }

    private void adjustVolume(int i) {
//        if (Build.VERSION.SDK_INT < 15) {
//            int oldVolume = mAudioManager.getStreamVolume(LINPHONE_VOLUME_STREAM);
//            int maxVolume = mAudioManager.getStreamMaxVolume(LINPHONE_VOLUME_STREAM);
//
//            int nextVolume = oldVolume +i;
//            if (nextVolume > maxVolume) nextVolume = maxVolume;
//            if (nextVolume < 0) nextVolume = 0;
//
//            core.setPlaybackGainDb((nextVolume - maxVolume)* dbStep);
//        } else
            // starting from ICS, volume must be adjusted by the application, at least for STREAM_VOICE_CALL volume stream
            mAudioManager.adjustStreamVolume(LINPHONE_VOLUME_STREAM, i < 0 ? AudioManager.ADJUST_LOWER : AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
    }
}