package com.example.sivvar.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sivvar.R
import com.example.sivvar.helpers.Utility
import kotlinx.android.synthetic.main.activity_otp_01.*

class OtpActivityV1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_01)

        nextBtn.setOnClickListener{
            Utility.navigate(this, CreatePinActivity())
        }
    }
}