package com.example.sivvar.activities.settings;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.example.sivvar.R;
import com.example.sivvar.helpers.Utility;

import java.util.ArrayList;

public class Call_history extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_history);


        ArrayList<String> arr = new ArrayList<String>();
        arr.add("Buy");
        arr.add("Sell");

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, arr);


        Utility.initToolbar(this, true, "Call history", false);
    }
}
