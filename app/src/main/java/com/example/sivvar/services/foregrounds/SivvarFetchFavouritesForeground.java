package com.example.sivvar.services.foregrounds;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;

import com.example.sivvar.activities.linphone.MainActivity;
import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.models.Favourites;
import com.example.sivvar.models.MyFavourites;
import com.example.sivvar.models.SubServices;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.favourites.FavouritesObservers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import static com.example.sivvar.helpers.DtosKt.generateStampSubServices;

public class SivvarFetchFavouritesForeground extends JobIntentService {

    private static Context contextt;
    private ResultReceiver resultReceiver;

    /*
    * Convenience method for enqueuing work in to this service.
    * */
    public static void enqueueWork(Context context, Intent work, int IntentJobId) {
        if(work!=null){
            contextt = context;
            enqueueWork(context, SivvarFetchFavouritesForeground.class, IntentJobId, work);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String s = intent.getStringExtra(SivvarKey.FAVOURITE_JOB_KEY.getValue());
        Type type = new TypeToken<JsonObject>(){}.getType();
        JsonObject param = new Gson().fromJson(s, type);

        resultReceiver = intent.getParcelableExtra(SivvarKey.FAVOURITE_JOB_RECEIVER.getValue());

            final Thread mThread = new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                fetchFavourites(param);
                Looper.loop();
                }
            };
            mThread.start();
    }

    private void fetchFavourites(JsonObject param){
        try{
            ApiCalls apiCalls = new ApiCalls("services", contextt, 60, 60, 60);
            apiCalls.getFavourites(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    if (ResponseUtil.Companion.isSuccessfullV3(res)) {
                        try {
                            String s = ResponseUtil.Companion.getResponseDataV3(res);
                            Log.e("FAVOURITES", ""+s);

                            List<Favourites> favouritesList = Favourites.arrayFavouritesFromData(s);

                            for(Favourites favourites: favouritesList){
                                Log.e("FAVOURITE ID", ""+favourites.getService_id());
                                updateMyFavourites(favourites.getService_id(), favourites.getService_dial_code(),
                                        favourites.getService_name(), favourites.getFavourite_status(),
                                        favourites.getHas_sub_service(), favourites.getIs_audio_service(),
                                        favourites.getService_logo(), favourites.getIs_video_service(),
                                        favourites.getService_description(), favourites.getCategory_name());
                            }

                            FavouritesObservers.getInstance().finis();
                        } catch (Exception ex) {
                            Log.e("Exception", "" + ex.getMessage());
                        }
                    }
                    return null;
                }

                @Override
                public String onFailure(String res) {
                    return null;
                }
            });
        }catch (Exception ex){
        Log.e("Exception", ""+ex.getMessage());
        }
    }

    private void updateMyFavourites(String service_id,String service_dial_code, String service_name,
                                    String favourite_status, String has_sub_service,
                                    String is_audio_service, String service_logo, String is_video_service,
                                    String service_description, String category_name){

        List<MyFavourites> fav = MyFavourites.find(MyFavourites.class, "SERVICEID = ?", service_id);

        if(fav.isEmpty()){
            MyFavourites myFavourites = new MyFavourites(service_dial_code, service_name, favourite_status,
                    has_sub_service, is_audio_service, service_logo, service_id, is_video_service,
                    service_description, category_name, 1);

            myFavourites.save();
        }else{
            MyFavourites myFavourites = fav.get(0);
            fav.get(0).delete();

            MyFavourites myFavourites1 = new MyFavourites();

            myFavourites1.setService_dial_code(myFavourites.getService_dial_code());
            myFavourites1.setService_name(myFavourites.getService_name());
            myFavourites1.setFavourite_status(myFavourites.getFavourite_status());
            myFavourites1.setHas_sub_service(myFavourites.getHas_sub_service());
            myFavourites1.setIs_audio_service(myFavourites.getIs_audio_service());
            myFavourites1.setService_logo(myFavourites.getService_logo());
            myFavourites1.setService_id(myFavourites.getService_id());
            myFavourites1.setIs_video_service(myFavourites.getIs_video_service());
            myFavourites1.setService_description(myFavourites.getService_description());
            myFavourites1.setCategory_name(myFavourites.getCategory_name());
            myFavourites1.setUsage(myFavourites.getUsage());
            myFavourites1.save();
        }
    }


    private void getSubServices(JsonObject param, String servicesID, Long favouriteID){
        try{
            param.remove("FavouriteServices");
            param.remove("sivvarStamp");
            param.addProperty("serviceID", servicesID);
            param.addProperty("sivvarStamp", generateStampSubServices(MainActivity.me.getImei(), MainActivity.me.getPhoneNumber(), servicesID));

            ApiCalls apiCalls = new ApiCalls("services", contextt, 60, 60, 60);
            apiCalls.getSubServices(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    if (ResponseUtil.Companion.isSuccessfullV3(res)) {
                        try {
                            String s = ResponseUtil.Companion.getResponseDataV3(res);
                            Log.e("RESPONSE", ""+s);

                            List<SubServices> subServicesList = SubServices.arraySubServicesFromData(s);

                            for(SubServices subServices: subServicesList){
                                SubServices subServices1 = new SubServices(favouriteID, subServices.getService_dial_code(), subServices.getService_name(),
                                        subServices.getHas_sub_service(), subServices.getIs_audio_service(), subServices.getService_status(), subServices.getService_logo(),
                                        subServices.getService_id(), subServices.getIs_video_service(), subServices.getService_description(), subServices.getCategory_name());
                                subServices1.save();
                            }

                            FavouritesObservers.getInstance().finis();
                        } catch (Exception ex) {
                            Log.e("Exception", "" + ex.getMessage());
                        }
                    }
                    return null;
                }

                @Override
                public String onFailure(String res) {
                    return null;
                }
            });
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}