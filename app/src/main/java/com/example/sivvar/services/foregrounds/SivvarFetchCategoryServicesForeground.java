package com.example.sivvar.services.foregrounds;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;

import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.services.ServicesObservers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SivvarFetchCategoryServicesForeground extends JobIntentService {

    private static Context contextt;
    private ResultReceiver resultReceiver;

    /*
    * Convenience method for enqueuing work in to this service.
    * */
    public static void enqueueWork(Context context, Intent work, int IntentJobId) {
        if(work!=null){
            contextt = context;
            enqueueWork(context, SivvarFetchCategoryServicesForeground.class, IntentJobId, work);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.e("SimpleJobIntentService", "Executing work: .....");

        String s = intent.getStringExtra(SivvarKey.SERVICE_JOB_KEY.getValue());
        Type type = new TypeToken<JsonObject>(){}.getType();
        JsonObject param = new Gson().fromJson(s, type);

        resultReceiver = intent.getParcelableExtra(SivvarKey.SERVICE_JOB_RECEIVER.getValue());

            final Thread mThread = new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                fetchCategories(param);
                Looper.loop();
                }
            };
            mThread.start();
    }

    private void fetchCategories(JsonObject param){
        try{
            ApiCalls apiCalls = new ApiCalls("services", contextt, 60, 60, 60);
            apiCalls.getServiceCategories(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {
                    if(ResponseUtil.Companion.isSuccessfullV3(res)){
                        Log.e("SUCCESS", ""+res);
                        String s = ResponseUtil.Companion.getResponseDataV3(res);
                        ServicesObservers.getInstance().addService(CategoryServices.arrayCategoryServicesFromData(s));

//                      resultReceiver.send(resultCODE, null);
                    }else{
                        Log.e("ERROR", ""+res);
                    }
                    return null;
                }

                @Override
                public String onFailure(String res) {
                    Log.e("FAILED", ""+res);

                    return null;
                }
            });
        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}