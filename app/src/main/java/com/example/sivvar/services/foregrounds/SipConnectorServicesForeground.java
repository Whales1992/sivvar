package com.example.sivvar.services.foregrounds;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;

import com.example.sivvar.constants.SivvarKey;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SipConnectorServicesForeground extends JobIntentService {

    private static Context contextt;
    private ResultReceiver resultReceiver;

    /*
    * Convenience method for enqueuing work in to this service.
    * */
    public static void enqueueWork(Context context, Intent work, int IntentJobId) {
        if(work!=null){
            contextt = context;
            enqueueWork(context, SipConnectorServicesForeground.class, IntentJobId, work);
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.e("SimpleJobIntentService", "Executing work: .....");

        String s = intent.getStringExtra(SivvarKey.SIP_JOB_KEY.getValue());
        Type type = new TypeToken<JsonObject>(){}.getType();
        JsonObject param = new Gson().fromJson(s, type);

        resultReceiver = intent.getParcelableExtra(SivvarKey.SIP_JOB_RECEIVER.getValue());

            final Thread mThread = new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                connectToProxyServer(param);
                Looper.loop();
                }
            };
            mThread.start();
    }

    private void connectToProxyServer(JsonObject param){
        try{

        }catch (Exception ex){
            Log.e("Exception", ""+ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}