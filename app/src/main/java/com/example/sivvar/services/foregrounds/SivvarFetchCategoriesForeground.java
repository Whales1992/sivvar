package com.example.sivvar.services.foregrounds;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import android.util.Log;

import com.example.sivvar.constants.SivvarKey;
import com.example.sivvar.helpers.User;
import com.example.sivvar.models.Categories;
import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.models.UserModel;
import com.example.sivvar.network.ApiCalls;
import com.example.sivvar.network.ResponseCallback;
import com.example.sivvar.network.ResponseUtil;
import com.example.sivvar.observables.categories.CategoryObservers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class SivvarFetchCategoriesForeground extends JobIntentService {

    private static Context contextt;
    private ResultReceiver resultReceiver;
    private static UserModel me;

    /*
    * Convenience method for enqueuing work in to this service.
    * */
    public static void enqueueWork(Context context, Intent work, int IntentJobId) {
        if(work!=null){
            contextt = context;
            me = User.getUserModel(context);
            enqueueWork(context, SivvarFetchCategoriesForeground.class, IntentJobId, work);
        }
    }


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String s = intent.getStringExtra(SivvarKey.CATEGORY_JOB_KEY.getValue());
        Type type = new TypeToken<JsonObject>(){}.getType();
        JsonObject param = new Gson().fromJson(s, type);

        resultReceiver = intent.getParcelableExtra(SivvarKey.CATEGORY_JOB_RECEIVER.getValue());

        final Thread mThread = new Thread() {
        @Override
        public void run() {
            Looper.prepare();
            fetchCategories(param);
            Looper.loop();
            }
        };
        mThread.start();
    }


    private void fetchCategories(JsonObject param){
        try{
            ApiCalls apiCalls = new ApiCalls("services", contextt, 60, 60, 60);
            apiCalls.getCategories(param, new ResponseCallback() {
                @Override
                public String onSuccess(String res) {

                    if (ResponseUtil.Companion.isSuccessfullV3(res)) {
                        try {
                            String s = ResponseUtil.Companion.getResponseDataV3(res);

                            List<Categories> categoriesList = Categories.arrayCategoriesFromData(s);

                            Log.e("CATEGORY", ""+s);

                            Categories.deleteAll(Categories.class);
                            CategoryServices.deleteAll(CategoryServices.class);

                            for(Categories categories: categoriesList){
                                Categories categories1 = new Categories(categories.getCategory_name(), categories.getCategory_logo(), categories.getCategory_id());
                                categories1.save();
                            }

                            CategoryObservers.getInstance().finis();
                        } catch (Exception ex) {
                            Log.e("Exception", "" + ex.getMessage());
                        }
                    }

                    return null;
                }

                @Override
                public String onFailure(String res) {
                    return null;
                }
            });
        }catch (Exception ex){
        Log.e("Exception", ""+ex.getMessage());
        }
    }

//    private void getServices(String categoryId){
//        JsonObject param = new JsonObject();
//        param.addProperty("msISDN", me.getPhoneNumber());
//        param.addProperty("Identity", me.getImei());
//        param.addProperty("categoryID", categoryId);
//        param.addProperty("sivvarStamp", generateStampServicesCategory(me.getImei(), me.getPhoneNumber(), categoryId));
//
//        ApiCalls apiCalls = new ApiCalls("services", contextt, 60, 60, 60);
//        apiCalls.getServiceCategories(param, new ResponseCallback() {
//            @Override
//            public String onSuccess(String res) {
//                if(ResponseUtil.Companion.isSuccessfullV3(res)){
//                    //Log.e("SUCCESS", ""+res);
//                    String s = ResponseUtil.Companion.getResponseDataV3(res);
//
//                    List<CategoryServices> categoryServices = CategoryServices.arrayCategoryServicesFromData(s);
//
//                    for(CategoryServices categoryServices1 : categoryServices){
//                        CategoryServices categoryServices2 = new CategoryServices(
//                                categoryServices1.getService_dial_code(),
//                                categoryId,
//                                categoryServices1.getService_name(),
//                                categoryServices1.getHas_sub_service(),
//                                categoryServices1.getIs_audio_service(),
//                                categoryServices1.getService_status(),
//                                categoryServices1.getService_logo(),
//                                categoryServices1.getService_id(),
//                                categoryServices1.getIs_video_service(),
//                                categoryServices1.getService_description(),
//                                categoryServices1.getCategory_name());
//
//                        categoryServices2.save();
//                    }
//                }else{
//                    Log.e("ERROR", ""+res);
//                }
//                return null;
//            }
//
//            @Override
//            public String onFailure(String res) {
//                Log.e("FAILED", ""+res);
//
//                return null;
//            }
//        });
//
//        CategoryObservers.getInstance().finis();
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}