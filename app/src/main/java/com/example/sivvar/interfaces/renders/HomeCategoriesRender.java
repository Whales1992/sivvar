package com.example.sivvar.interfaces.renders;

import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import com.example.sivvar.models.Categories;

public interface HomeCategoriesRender {
    void renderCategories(Categories categories, TextView category_title, TextView showmore, RecyclerView service_list);
}
