package com.example.sivvar.interfaces.renders;

import android.widget.ImageView;
import android.widget.TextView;

import com.example.sivvar.models.CategoryServices;

public interface CategoryServicesRender {
    void renderServices(CategoryServices categoryServices, TextView serviceName, ImageView serviceLogo);
}
