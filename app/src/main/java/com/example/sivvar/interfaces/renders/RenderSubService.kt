package com.example.sivvar.interfaces.renders

import androidx.cardview.widget.CardView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.sivvar.models.CategoryServices
import com.example.sivvar.models.SubServices

interface RenderSubService {
    fun renderSubService(subServices: SubServices, serviceLogo: ImageView, serviceName: TextView, catTitle: TextView, parent: LinearLayout)
//    fun renderSubService(subServices: CategoryServices, serviceLogo: ImageView, serviceName: TextView, catTitle: TextView, parent: LinearLayout)
}