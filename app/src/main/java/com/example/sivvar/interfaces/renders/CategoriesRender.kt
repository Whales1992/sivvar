package com.example.sivvar.interfaces.renders

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView

import com.example.sivvar.models.Categories

interface CategoriesRender {
    fun renderCategories(categories: Categories, category_title: TextView, cardView: androidx.cardview.widget.CardView, logo: ImageView)
}