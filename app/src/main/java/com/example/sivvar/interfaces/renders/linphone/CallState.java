package com.example.sivvar.interfaces.renders.linphone;

import org.linphone.core.Call;
import org.linphone.core.Core;

public interface CallState {
    void onStateChanged(Core core, Call call, Call.State state, String message);
}