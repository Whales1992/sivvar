package com.example.sivvar.interfaces.renders

import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.sivvar.models.CategoryServices

interface RenderCategorySubService {
    fun renderCategorySubServices(subServices: CategoryServices, serviceLogo: ImageView, serviceName: TextView, catTitle: TextView, parent: LinearLayout)
}