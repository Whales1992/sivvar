package com.example.sivvar.interfaces.renders

import android.widget.TextView

import com.example.sivvar.models.CallHistory

interface CallhistoryRender {
    fun renderCallHistory(text: CallHistory, dateTag: TextView, speakTime: TextView, timeStamp: TextView)
}