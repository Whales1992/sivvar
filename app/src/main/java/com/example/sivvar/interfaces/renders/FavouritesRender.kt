package com.example.sivvar.interfaces.renders

import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.sivvar.models.Favourites
import com.example.sivvar.models.MyFavourites

interface FavouritesRender {
    fun renderFavourites(favourites: MyFavourites, categoryTitle: TextView, serviceTitle: TextView, serviceLogo: ImageView, parentt: LinearLayout)
}
