package com.example.sivvar.interfaces.renders.linphone

interface LedStates {
    fun Connected(s: String)
    fun Failed(s: String)
    fun Disconnected(s: String)
    fun Inprogress(s: String)
    fun Idle(s: String)
}