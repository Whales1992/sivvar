package com.example.sivvar.interfaces.renders

import androidx.constraintlayout.widget.ConstraintLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView

import com.example.sivvar.models.CallHistory

interface CallhistoriesRender {
    fun renderCallHistory(callHistory: CallHistory, parent: ConstraintLayout, serviceTitle: TextView, logo: ImageView, menuBtn: ImageButton)
}