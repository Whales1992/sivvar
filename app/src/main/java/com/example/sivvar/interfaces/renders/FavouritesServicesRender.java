package com.example.sivvar.interfaces.renders;

import android.widget.ImageView;
import android.widget.TextView;

import com.example.sivvar.models.CategoryServices;
import com.example.sivvar.models.Favourites;

public interface FavouritesServicesRender {
    void renderServices(Favourites favourites, TextView serviceName, ImageView serviceLogo);
}
