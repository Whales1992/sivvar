package com.example.sivvar.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class UserModel {
    /**
     * firstName : Adewale
     * lastName : Kolawole
     * emailAddress : kola.victor@yahoo.com
     * countryCode : +1
     * imei : 2343223423
     * phoneNumber : 2349057458888
     * responseData : 324234234234
     */

    private String firstName;
    private String lastName;
    private String emailAddress;
    private String countryCode;
    private String imei;
    private String phoneNumber;
    private String responseData;

    public static UserModel objectFromData(String str) {

        return new Gson().fromJson(str, UserModel.class);
    }

    public static List<UserModel> arrayUserModelFromData(String str) {

        Type listType = new TypeToken<ArrayList<UserModel>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString(){
        return "{" +
                "firstName='" + getFirstName() + '\'' +
                "lastName='" + getLastName() + '\'' +
                ", emailAddress='" + getEmailAddress() + '\'' +
                ", countryCode='" + getCountryCode() + '\'' +
                ", imei='" + getImei() + '\'' +
                ", phoneNumber='" + getPhoneNumber() + '\'' +
                ", responseData='" + getResponseData() + '\'' +
                '}';
    }
}