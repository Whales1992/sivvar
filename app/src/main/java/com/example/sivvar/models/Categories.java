package com.example.sivvar.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Categories extends SugarRecord {

    /**
     * category_name : Airline
     * category_logo : http://sivvar-services-prod.sivvar.services:9448/sivvar_logos/industries/Airline.png
     * category_id : 1L
     */

    private String category_name;
    private String category_logo;
    private String category_id;


    public Categories(String catName, String catLogo, String catId){
        category_name = catName;
        category_logo = catLogo;
        category_id = catId;
    }

    public Categories(){
        //Default constructor is necessary
    }

    public static List<Categories> getCategories(){
        return Categories.listAll(Categories.class);
    }

    public static Categories objectFromData(String str) {

        return new Gson().fromJson(str, Categories.class);
    }

    public static List<Categories> arrayCategoriesFromData(String str) {

        Type listType = new TypeToken<ArrayList<Categories>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_logo() {
        return category_logo;
    }

    public void setCategory_logo(String category_logo) {
        this.category_logo = category_logo;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
