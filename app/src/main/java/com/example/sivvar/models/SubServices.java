package com.example.sivvar.models;

import com.example.sivvar.observables.favouritesSubServices.favourites.FavouritesSubServicesSubServicesObservers;
import com.example.sivvar.observables.favouritesSubServices.favouritesStepDown.FavouritesSubServicesSubServicesObserversDown;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SubServices extends SugarRecord {

    /**
     * service_dial_code : **0209
     * service_name : Speak to a Qualified Doctor - After Office Hours
     * has_sub_service : false
     * is_audio_service : false
     * service_status : Active
     * service_logo : http://sivvar-services-prod.sivvar.services:8896/sivvar_logos/services/speak2doctor.png
     * service_id : 88
     * is_video_service : false
     * service_description : NONE
     * category_name : Insurance
     */

    private Long favouriteId;
    private String service_dial_code;
    private String service_name;
    private String has_sub_service;
    private String is_audio_service;
    private String service_status;
    private String service_logo;
    private int service_id;
    private String is_video_service;
    private String service_description;
    private String category_name;

    public SubServices(){
        //Default constructor is necessary
    }

    public static List<SubServices> getSubServices(){
        return SubServices.listAll(SubServices.class);
    }

    public SubServices(Long favouriteId, String service_dial_code, String service_name, String has_sub_service,
                       String is_audio_service, String service_status, String service_logo, int service_id,
                       String is_video_service, String service_description, String category_name){
        this.favouriteId = favouriteId;
        this.service_dial_code = service_dial_code;
        this.service_name = service_name;
        this.has_sub_service = has_sub_service;
        this.is_audio_service = is_audio_service;
        this.service_status = service_status;
        this.service_logo = service_logo;
        this.service_id = service_id;
        this.is_video_service = is_video_service;
        this.service_description = service_description;
        this.category_name = category_name;
    }

    public static SubServices objectFromData(String str) {

        return new Gson().fromJson(str, SubServices.class);
    }

    public static List<SubServices> arraySubServicesFromData(String str) {

        Type listType = new TypeToken<ArrayList<SubServices>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public static void getSubservices(Long id) {
        List<SubServices> subServicesList = SubServices.find(SubServices.class, "FAVOURITE_ID =?", String.valueOf(id));
        FavouritesSubServicesSubServicesObservers.getInstance().populateList(subServicesList);
    }
    public static void getSubservicesDown(Long id) {
        List<SubServices> subServicesList = SubServices.find(SubServices.class, "FAVOURITE_ID =?", String.valueOf(id));
        FavouritesSubServicesSubServicesObserversDown.getInstance().populateList(subServicesList);
    }

    public static List<SubServices> getSubservicesList(Long id) {
        return SubServices.find(SubServices.class, "FAVOURITE_ID =?", String.valueOf(id));
    }

    public static List<SubServices> getSubservicesListByServiceId(int service_id) {
        return SubServices.find(SubServices.class, "SERVICEID =?", String.valueOf(service_id));
    }

    public String getService_dial_code() {
        return service_dial_code;
    }

    public void setService_dial_code(String service_dial_code) {
        this.service_dial_code = service_dial_code;
    }

    public Long getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(Long favouriteId) {
        this.favouriteId = favouriteId;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getHas_sub_service() {
        return has_sub_service;
    }

    public void setHas_sub_service(String has_sub_service) {
        this.has_sub_service = has_sub_service;
    }

    public String getIs_audio_service() {
        return is_audio_service;
    }

    public void setIs_audio_service(String is_audio_service) {
        this.is_audio_service = is_audio_service;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getService_logo() {
        return service_logo;
    }

    public void setService_logo(String service_logo) {
        this.service_logo = service_logo;
    }

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public String getIs_video_service() {
        return is_video_service;
    }

    public void setIs_video_service(String is_video_service) {
        this.is_video_service = is_video_service;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
