package com.example.sivvar.models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MyFavourites extends SugarRecord {

    /**
     * service_dial_code : **88125
     * service_name : Meet SIVVAR - The IVR VoIP App
     * favourite_status : Active
     * has_sub_service : true
     * is_audio_service : false
     * service_logo : http://sivvar-services-prod.sivvar.services:8896/sivvar_logos/services/sivvar.png
     * service_id : 1L
     * is_video_service : false
     * service_description : SIVVAR is the Mobile App that helps you Call and Speak with representatives of Businesses or Services using your Mobile Phone Data. Services such as Do-It-Yourself IVR System, Contact Centres, Telephone Banking, Voice related Value Added Services (VAS), e
     * category_name : Information & Communication Technology
     * usage : 4
     */

    private String service_dial_code;
    private String service_name;
    private String favourite_status;
    private String has_sub_service;
    private String is_audio_service;
    private String service_logo;
    private String service_id;
    private String is_video_service;
    private String service_description;
    private String category_name;
    private int usage;

    public MyFavourites(){
        //Default constructor is necessary
    }

    public MyFavourites(String service_dial_code, String service_name,
                        String favourite_status, String has_sub_service,
                        String is_audio_service, String service_logo,
                        String service_id, String is_video_service,
                        String service_description, String category_name, int usage){
        this.service_dial_code = service_dial_code;
        this.service_name = service_name;
        this.favourite_status = favourite_status;
        this.has_sub_service = has_sub_service;
        this.is_audio_service = is_audio_service;
        this.service_logo = service_logo;
        this.service_id = service_id;
        this.is_video_service = is_video_service;
        this.service_description = service_description;
        this.category_name = category_name;
        this.usage = usage;
    }

    public static List<MyFavourites> getMyFavoutires(){
        return MyFavourites.listAll(MyFavourites.class, "USAGE desc");
    }

    public static MyFavourites objectFromData(String str) {

        return new Gson().fromJson(str, MyFavourites.class);
    }

    public static List<MyFavourites> arrayFavouritesFromData(String str) {

        Type listType = new TypeToken<ArrayList<MyFavourites>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public String getService_dial_code() {
        return service_dial_code;
    }

    public void setService_dial_code(String service_dial_code) {
        this.service_dial_code = service_dial_code;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getFavourite_status() {
        return favourite_status;
    }

    public void setFavourite_status(String favourite_status) {
        this.favourite_status = favourite_status;
    }

    public String getHas_sub_service() {
        return has_sub_service;
    }

    public void setHas_sub_service(String has_sub_service) {
        this.has_sub_service = has_sub_service;
    }

    public String getIs_audio_service() {
        return is_audio_service;
    }

    public void setIs_audio_service(String is_audio_service) {
        this.is_audio_service = is_audio_service;
    }

    public String getService_logo() {
        return service_logo;
    }

    public void setService_logo(String service_logo) {
        this.service_logo = service_logo;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getIs_video_service() {
        return is_video_service;
    }

    public void setIs_video_service(String is_video_service) {
        this.is_video_service = is_video_service;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getUsage() {
        return usage;
    }

    public void setUsage(int usage) {
        this.usage = usage;
    }
}
