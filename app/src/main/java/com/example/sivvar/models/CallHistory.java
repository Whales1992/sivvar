package com.example.sivvar.models;

import com.orm.SugarRecord;

import java.util.Date;
import java.util.List;

public class CallHistory extends SugarRecord {

    public String dialCode;
    public String logo;
    public String serviceName;
    public String serviceId;
    public String speakTime;
    public Date date;

    public CallHistory(){
        //Empty constructor needed by sugar orm
    }

    public CallHistory(String dialCode, String logo, String serviceName, String serviceId, String speakTime){
        this.dialCode = dialCode;
        this.logo = logo;
        this.serviceName = serviceName;
        this.serviceId = serviceId;
        this.speakTime = speakTime;
        this.date = new Date();
    }

    public static List<CallHistory> getCallHistory(){
        return CallHistory.listAll(CallHistory.class);
    }

    public static List<CallHistory> getCallHistoryByServiceId(String id){
        return CallHistory.find(CallHistory.class, "SERVICE_ID =?", id);
    }
}