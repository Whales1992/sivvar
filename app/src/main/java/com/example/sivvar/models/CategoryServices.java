package com.example.sivvar.models;

import com.example.sivvar.observables.categoriesSubServices.categoriesStepDown.CategorySubServiceObserversDown;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryServices extends SugarRecord {

    /**
     * service_dial_code : ***014041216
     * service_name : FirstMonie IVR
     * has_sub_service : false
     * is_audio_service : false
     * service_status : Active
     * service_logo : http://sivvar-services-prod.sivvar.services:9448/sivvar_logos/services/firstmonie.png
     * service_id : 2L
     * is_video_service : false
     * service_description : NONE
     * category_name : Banking
     */

    public String service_dial_code;
    public String category_id;
    public String service_name;
    public String has_sub_service;
    public String is_audio_service;
    public String service_status;
    public String service_logo;
    public String service_id;
    public String is_video_service;
    public String service_description;
    public String category_name;

    public CategoryServices(String service_dial_code, String category_id, String service_name, String has_sub_service,
                             String is_audio_service, String service_status, String service_logo,
                             String service_id, String is_video_service, String service_description,
                             String category_name){
        this.service_dial_code = service_dial_code;
        this.category_id = category_id;
        this.service_name = service_name;
        this.has_sub_service = has_sub_service;
        this.is_audio_service = is_audio_service;
        this.service_status = service_status;
        this.service_logo = service_logo;
        this.service_id = service_id;
        this.is_video_service = is_video_service;
        this.service_description = service_description;
        this.category_name = category_name;
    }

    public CategoryServices(){
        //Default constructor is necessary
    }

    public static List<CategoryServices> getCategoryServicesById(String category_id){
        return CategoryServices.find(CategoryServices.class, "CATEGORYID = ?" + " order by id asc", category_id);
    }

    public static CategoryServices objectFromData(String str) {

        return new Gson().fromJson(str, CategoryServices.class);
    }

    public static List<CategoryServices> arrayCategoryServicesFromData(String str) {

        Type listType = new TypeToken<ArrayList<CategoryServices>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public static void getSubservicesDown(String id) {
        List<CategoryServices> list = CategoryServices.find(CategoryServices.class, "SERVICEID =?", id);
        CategorySubServiceObserversDown.getInstance().populateList(list);
    }

    public String getService_dial_code() {
        return service_dial_code;
    }

    public void setService_dial_code(String service_dial_code) {
        this.service_dial_code = service_dial_code;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getHas_sub_service() {
        return has_sub_service;
    }

    public void setHas_sub_service(String has_sub_service) {
        this.has_sub_service = has_sub_service;
    }

    public String getIs_audio_service() {
        return is_audio_service;
    }

    public void setIs_audio_service(String is_audio_service) {
        this.is_audio_service = is_audio_service;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getService_logo() {
        return service_logo;
    }

    public void setService_logo(String service_logo) {
        this.service_logo = service_logo;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getIs_video_service() {
        return is_video_service;
    }

    public void setIs_video_service(String is_video_service) {
        this.is_video_service = is_video_service;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
