package com.example.sivvar.landing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.sivvar.R
import com.example.sivvar.activities.SetUp01Activity
import com.example.sivvar.landing.fragments.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_landing.*

class LandingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        initViewPager()

        continueBtn.setOnClickListener {
            val i= Intent(this, SetUp01Activity().javaClass)
            startActivity(i)
            finish();
        }
    }

    private fun initViewPager() {
        viewpager.adapter = ViewPagerAdapter(supportFragmentManager, 3)
        viewpager.setCurrentItem(0, true)
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
                if(position == 0)
                {
                    titleTxt.text = resources.getString(R.string.title01)
                    description.text = resources.getString(R.string.desc01)
                }

                if(position == 1)
                {
                    titleTxt.text = resources.getString(R.string.title02)
                    description.text = resources.getString(R.string.desc02)
                }


                if(position == 2)
                {
                    titleTxt.text = resources.getString(R.string.title03)
                    description.text = resources.getString(R.string.desc03)
                }
            }

            override fun onPageSelected(position: Int) {

            }
        })

        viewpager.offscreenPageLimit = 3
        dots_indicator.setViewPager(viewpager)
    }
}