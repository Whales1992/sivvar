package com.example.sivvar.landing.fragments

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(
        fm: androidx.fragment.app.FragmentManager,
        private val mNumOfTabs: Int
) : androidx.fragment.app.FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return when (position) {

            0 -> {
                FirstFragment()
            }

            1 -> {
                SecondFragment()
            }

             2 -> {
                ThirdFragment()
            }

            else -> {
                FirstFragment()
            }
        }
    }

    override fun getCount(): Int {
        return mNumOfTabs
    }
}